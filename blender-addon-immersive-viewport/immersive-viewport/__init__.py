__copyright__ = """
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either version 3 of the license, or (at your option) any later
version (http://www.gnu.org/licenses/).
"""
__license__ = "GPLv3"


bl_info = {
    "name": "Immersive viewport",
    "author": "Emmanuel Durand, Francois Ubald Brien",
    "version": (0, 0, 1),
    "blender": (2, 78, 0),
    "description": "Allows for 3D viewport to output spherical or equirectangular projection. See https://www.blender.org/manual/game_engine/camera/dome.html for precisions on video modes",
    "category": "3D View"
}


if "bpy" in locals():
    import imp
    imp.reload(ui)
    imp.reload(operators)
else:
    import bpy
    from bpy.props import (EnumProperty,
                           BoolProperty,
                           IntProperty,
                           FloatProperty,
                           FloatVectorProperty,
                           StringProperty,
                           PointerProperty
                           )
    from bpy.types import (Operator,
                           PropertyGroup,
                           AddonPreferences
                           )
    from bpy.app.handlers import persistent
    from . import operators
    from . import ui


_projectionTypes = [
    ("Spherical", "Spherical", "", 0),
    ("Equirectangular", "Equirectangular", "", 1),
    ("Cubemap", "Cubemap", "", 2)
    ]


class ImmersiveScreenSettings(PropertyGroup):
    enabled = BoolProperty(
        default=False
        )
    active = BoolProperty(
        default=False
        )


class ImmersiveViewportAddonPreferences(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__

    subrenderResolution = IntProperty(
        name="Sub-render resolution",
        description="Resolution of the intermediate renders",
        default=512,
        min=128, max=4096
    )
    anchorObject = StringProperty(
        name="Anchor object",
        description="Anchor object for the rendered view",
        default=""
    )
    fieldOfView = FloatProperty(
        name="Field of view",
        description="Field of view for the spherical rendering",
        default=180.0,
        min=90.0, max=360.0
    )
    projectionType = EnumProperty(
        name="Projection type",
        description="Camera projection type",
        items=_projectionTypes
    )
    domeRadius = FloatProperty(
        name="Dome radius",
        description="Dome radius",
        default=18.0,
        min=1.0
    )
    shiftFromCenter = FloatVectorProperty(
        name="View position shift",
        description="Position shift from the center of the dome",
        size=3,
        default=[0.0, 0.0, 0.0]
    )
    
    def draw(self, context):
        layout = self.layout

        col = layout.column(align=True)
        row = col.row(align=True)
        row.separator()
        row = col.row(align=True)
        row.label("Projection parameters:")
        row = col.row(align=True)
        row.prop(self, "anchorObject")
        row = col.row(align=True)
        row.prop(self, "projectionType", text="")

        if self.projectionType == "Spherical":
            row.prop(self, "fieldOfView", text="FOV")
            row = col.row(align=True)
            row.separator()
            row = col.row(align=True)
            row.label("Dome parameters")
            row = col.row(align=True)
            row.prop(self, "shiftFromCenter", text="")
            row = col.row(align=True)
            row.prop(self, "domeRadius", text="Dome radius")
        
        
classes = (
    ui.RenderPanel,
    ui.ImmersiveViewport,

    operators.ImmersiveView3D,
    operators.ImmersiveView3DToggle,

    ImmersiveScreenSettings,
    ImmersiveViewportAddonPreferences
)


@persistent
def auto_immerse(context):
    bpy.ops.immersive.toggle(no_window=True)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Screen.immersive_viewport = PointerProperty(type=ImmersiveScreenSettings)

    bpy.app.handlers.load_post.append(auto_immerse)


def unregister():
    bpy.app.handlers.load_post.remove(auto_immerse)

    for cls in classes:
        bpy.utils.unregister_class(cls)

    del bpy.types.Screen.immersive_viewport
