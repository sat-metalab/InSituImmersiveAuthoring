from math import pi
from mathutils import Matrix, Vector
from ..gallery import Viewer
from ..utils import BlenderUtils

from enum import Enum
import bpy


class GalleryMode(Enum):
    MATERIAL = 1,
    COPY_OBJECTS = 2,
    IMAGES = 3,
    SOUNDS = 4,


class SlideshowPattern(Enum):
    CIRCLE = 1,
    WALL = 2


def enter_gallery(path,
                  gallery_mode=GalleryMode.COPY_OBJECTS,
                  scale=1.0,
                  modification_target=None):
    """
    Creates a scene and instantiates a gallery inside.
    :param path: Path of the folder containing the assets
    :param gallery_mode: Reason to enter the gallery, can be MATERIAL, IMAGES, COPY_OBJECTS or SOUNDS
    :param scale: Scale of gallery objects
    :param modification_target: Reference object to show materials or onto which to apply a material
    :param The width of the gallery 
    :return The coordinates of the center of the gallery in the Oxy plane.
    """

    # We clear the selected objects from a possible previous gallery
    world = bpy.context.scene.world
    origin_scene = bpy.context.scene
    new_scene = bpy.data.scenes.new('gallery')
    new_scene.world = world
    bpy.context.screen.scene = new_scene
    show_3dmodels = True
    show_materials = False
    show_sounds = False
    if gallery_mode == GalleryMode.MATERIAL:
        show_materials = True
        show_3dmodels = False
    elif gallery_mode == GalleryMode.IMAGES:
        show_3dmodels = False
        show_materials = False
    elif gallery_mode == GalleryMode.SOUNDS:
        show_sounds = True

    width = Viewer.create_gallery(
        scale=scale,
        modification_target=modification_target,
        path=path,
        show_3dmodels=show_3dmodels,
        show_materials=show_materials,
        show_sounds=show_sounds)

    for o in new_scene.objects:
        o.select = False

    return Vector((width / 2, -width / 2, - 2 * scale))


def exit_gallery(gallery_mode,
                 origin_scene,
                 translation_matrix=Matrix.Identity(4),
                 modification_target=None,
                 join_models=False,
                 pattern=SlideshowPattern.WALL,
                 horizontal_axis='X',
                 vertical_axis='Z',
                 grid_size=2.0,
                 nb_columns=6):
    """
    Exits the gallery and process the selected object(s) accordingly to the specified gallery mode.
    :param gallery_mode: Mode of the gallery determining the action to be taken when leaving the gallery
    :param origin_scene: Scene that called the gallery, to which we'll come back.
    :param translation_matrix: Translation matrix used to put the slideshow at the reference point of the chosen 
    pattern (e.g: center of circle, top left of image wall,...)
    :param modification_target: Reference object to show materials or onto which to apply a material
    :param join_models: If True, imported 3D models will be joined in one object
    :param pattern: Can be CIRCLE or WALL
    :param horizontal_axis: Used only by pattern WALL, direction of the slideshow
    :param vertical_axis: Used only by pattern CIRCLE, normal to the circle
    :param grid_size: Used only by pattern WALL, width of each grid square
    :param nb_columns: Used only by pattern WALL, number of columns of the grid.
    """

    ret = None
    if gallery_mode == GalleryMode.COPY_OBJECTS:
        ret = fetch_copy_assets(scene=origin_scene, join_models=join_models)
    elif gallery_mode == GalleryMode.MATERIAL:
        apply_material(scene=origin_scene, modification_target=modification_target)
    elif gallery_mode == GalleryMode.IMAGES:
        image_slideshow(scene=origin_scene,
                        translation_matrix=translation_matrix,
                        pattern=pattern,
                        horizontal_axis=horizontal_axis,
                        vertical_axis=vertical_axis,
                        grid_size=grid_size,
                        nb_columns=nb_columns)
    elif gallery_mode == GalleryMode.SOUNDS:
        attach_sound(scene=origin_scene, modification_target=modification_target)
    return ret


def fetch_copy_assets(scene, join_models=False):
    """
    Get all selected assets and either copy them and put their names in a list for later processing or copy them in 
    their place in the origin scene.
    :param scene: Scene in which the selected objects will be copied
    :param join_models: If True, imported 3D models will be joined in one object
    """
    copy_objects = list()

    # Link the selected items to the calling scene or put them in a list
    # For 3D models, only process the parent
    selected_objects = [o for o in bpy.context.scene.objects if o.select and not o.get('model_object')]

    # Deselect all objects first
    for o in bpy.data.objects:
        o.select = False

    for obj in selected_objects:
        model_objects = Viewer.get_model_objects(obj)

        copy_obj = obj.copy()
        if obj.data:
            copy_obj.data = obj.data.copy()

        if not len(model_objects) or not join_models:
            scene.objects.link(copy_obj)

        if not len(model_objects):
            copy_objects.append(copy_obj)
            continue
        # If the object is a parent, copy and link its children or the joined children
        model_objects = BlenderUtils.get_children(obj)
        for mo in model_objects:
            copy_mo = mo.copy()
            if mo.data:
                copy_mo.data = mo.data.copy()
            copy_mo.parent = copy_obj
            if not join_models:
                scene.objects.link(copy_mo)
            else:
                bpy.context.scene.objects.link(copy_mo)

        if not join_models:
            copy_objects.append(copy_obj)
            continue

        # Select all the parts of the 3D model to join them
        for mo in copy_obj.children:
            mo.select = True
        copy_obj.select = False

        # Join all the parts of the 3D model on the first child of the parent
        joiner = copy_obj.children[0]
        bpy.context.scene.objects.active = joiner
        bpy.ops.object.join()
        joiner.name = joiner.parent.name
        joiner.parent = None
        bpy.context.scene.objects.unlink(joiner)
        scene.objects.link(joiner)
        copy_objects.append(joiner)

    return copy_objects


def apply_material(scene, modification_target):
    """
    Finds the first selected object containing a material and apply it to the active object in the origin scene.
    :param scene: Scene containing the object to texture
    :param modification_target: Object to which the material will be applied
    """
    if not modification_target:
        return

    # Look for the first object with a material and apply the material to the model.
    for ref in [o for o in bpy.context.scene.objects if o.select]:
        material_name = ref.get('material')
        if not material_name:
            continue
        mesh = modification_target.data
        for face in mesh.polygons:
            face.use_smooth = True
        if not mesh.uv_layers:
            mesh.uv_textures.new()
        if len(modification_target.material_slots) < 1:
            mesh.materials.append(None)
        modification_target.material_slots[0].link = 'DATA'
        modification_target.material_slots[0].material = bpy.data.materials[material_name]
        break


def image_slideshow(scene,
                    translation_matrix=Matrix.Identity(4),
                    objects=None,
                    pattern=SlideshowPattern.WALL,
                    vertical_axis='Z',
                    horizontal_axis='X',
                    grid_size=2.0,
                    nb_columns=6):
    """
    Show selected images according to the defined pattern
    :param scene: Scene in which the selected objects will be copied
    :param translation_matrix: Translation matrix used to put the slideshow at the reference point of the chosen 
    pattern (e.g: center of circle, top left of image wall,...)
    :param objects: List of objects to show, do not copy/link these if they are provided
    :param pattern: Can be CIRCLE or WALL
    :param vertical_axis: Used only by CIRCLE, normal to the circle
    :param horizontal_axis: Used only by WALL, direction of the slideshow
    :param grid_size: Used only by WALL, width of each grid square
    :param nb_columns: Used only by WALL, number of columns of the grid.
    """
    vertical_axis = vertical_axis.upper()
    if vertical_axis not in ('X', 'Y', 'Z'):
        vertical_axis = 'Z'
        print('Specified top axis {} is not valid, only options are x, y, or z.')

    total_image_width = 0
    # If a list of objects is passed, just position them, do not copy/link them
    if objects and len(objects):
        objects_list = objects
    else:
        objects_list = list()
        for image in [o for o in bpy.context.scene.objects if o.select]:
            copy_image = image.copy()
            if copy_image.data:
                copy_image.data = image.data.copy()
                objects_list.append(copy_image)

            scene.objects.link(copy_image)

    if not len(objects_list):
        return

    for o in objects_list:
        total_image_width += o.dimensions[1]

    circle_radius = total_image_width * 1.5 / (2 * pi)  # 1.5 factor is here for better visibility

    if pattern == SlideshowPattern.CIRCLE:
        pattern_circle(translation_matrix, circle_radius, vertical_axis, objects_list)
    elif pattern == SlideshowPattern.WALL:
        pattern_wall(translation_matrix, horizontal_axis, nb_columns, grid_size, objects_list)

    bpy.context.scene.update()


def pattern_circle(translation_matrix, circle_radius, vertical_axis, objects):
    current_angle = 0
    angle_incr = 2 * pi / len(objects)
    translate_to_circle_matrix = Matrix.Translation(Vector((0, circle_radius, 0)))

    if vertical_axis == 'Z':
        axis_rotation_matrix = Matrix.Identity(4)
    else:
        axis_rotation_matrix = Matrix.Rotation(pi / 2, 4, 'X' if vertical_axis == 'Y' else 'Y')

    for o in objects:
        scale_matrix = Matrix.Identity(4)
        scale = o.matrix_world.to_scale()
        scale_matrix[0][0] = scale.x
        scale_matrix[1][1] = scale.y
        scale_matrix[2][2] = scale.z
        rotation_matrix = Matrix.Rotation(current_angle, 4, 'Z')
        o.matrix_world = \
            translation_matrix * \
            axis_rotation_matrix * \
            rotation_matrix * \
            translate_to_circle_matrix * \
            Matrix.Rotation(pi/2, 4, 'X') * \
            scale_matrix
        current_angle += angle_incr


def pattern_wall(translation_matrix, horizontal_axis, nb_columns, grid_size, objects):
    x = 0
    z = 0
    cur_column = 0

    rotation_matrix = Matrix.Identity(4)
    if horizontal_axis == 'Y':
        rotation_matrix = Matrix.Rotation(pi / 2, 4, 'Z')
    elif horizontal_axis == 'Z':
        rotation_matrix = Matrix.Rotation(-pi / 2, 4, 'Y')

    for o in objects:
        scale_matrix = Matrix.Identity(4)
        scale = o.matrix_world.to_scale()
        scale_matrix[0][0] = scale.x
        scale_matrix[1][1] = scale.y
        scale_matrix[2][2] = scale.z
        grid_place_matrix = Matrix.Translation(Vector((x, 0, z)))
        o.matrix_world = translation_matrix * \
                         rotation_matrix * \
                         grid_place_matrix * \
                         Matrix.Rotation(pi / 2, 4, 'X') * \
                         scale_matrix
        cur_column += 1
        if cur_column == nb_columns:
            x = 0
            z -= grid_size
            cur_column = 0
        else:
            x += grid_size


def attach_sound(scene, modification_target):
    """
    Finds the first selected object with a sound attached and apply it to the active object in the origin scene.
    :param scene: Scene containing the object to which we will attach the sound.
    :param modification_target: Object to which the sound will be attached.
    """
    if not modification_target:
        return

    selected_sound_object = None
    # Find first selected SATIE object (there should be only one anyway)
    for o in bpy.context.scene.objects:
        if o.select and o.get('satie_object'):
            selected_sound_object = o
            break

    if not selected_sound_object:
        print('No object selected, not attaching sound.')
        return

    if not selected_sound_object.get('satie_object'):
        print('Not a SATIE object, no sound attached.')
        return

    modification_target.satie.plugin_family = selected_sound_object.satie.plugin_family
    modification_target.satie.synth = selected_sound_object.satie.synth
    modification_target.satie.enabled = True

    # Turn off all objects to be safe
    for o in bpy.context.scene.objects:
        if o.get('satie_object'):
            o.satie.enabled = False

    return
