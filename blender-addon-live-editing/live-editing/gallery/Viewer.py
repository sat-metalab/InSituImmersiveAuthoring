from enum import Enum
from math import *
from mathutils import Matrix, Vector
import bmesh
import bpy
from .indexing.AssetIndexer import AssetIndexer
from ..utils import BlenderUtils
from ..engine import TextField

model_element_separator = '####'
max_length = 8  # In scale unity


class ShelfType(Enum):
    IMAGES = 1,
    MATERIALS = 2,
    MODELS_3D = 3


def create_gallery(path, scale, modification_target=None, show_images=True, show_3dmodels=True,
                   show_materials=True, show_sounds=False):
    """
    Parses a folder for displayable assets and presents them in a new scene and make it active.
    :param path: Path of the folder containing the assets
    :param scale: Scale of the gallery objects
    :param modification_target: Reference object to show materials
    :param show_images: Show assets with mime_type image/***
    :param show_3dmodels: Show 3dmodels specified in custom objects json file
    :param show_materials: Show materials found in .blend files (on a sphere by default)
    :param show_sounds: Attach available SATIE sound assets to objects to preview them  
    :param The width of the gallery.
    """

    # Sounds depend exclusively on SATIE so they get an independent processing
    if not show_sounds:
        indexer = AssetIndexer()
        indexer.get_files(path)
        indexer.parse_files()

    # Unlink all objects in case of .blend file importing, let the present_* methods decide who is linked to the scene
    for o in bpy.context.scene.objects:
        bpy.context.scene.objects.unlink(o)

    lamp_data = bpy.data.lamps.new(name='Illuminati', type='SUN')
    lamp_object = bpy.data.objects.new(name='Illuminati', object_data=lamp_data)
    bpy.context.scene.objects.link(lamp_object)

    def get_object_min_max_coords(obj):
        bbox = [(obj.matrix_world * Vector(vert)).to_tuple() for vert in obj.bound_box]
        z = [i for i in zip(*bbox)]
        return [i for i in map(min, z)], [i for i in map(max, z)]

    def create_material_assets(name, asset):
        mat = bpy.data.materials.new(name + '_mat')
        mat.use_shadeless = True

        if asset.get_value('mime_type').startswith('image'):
            tex = bpy.data.textures.new(name + '_tex', 'IMAGE')
            img = bpy.data.images.load(asset.path)
            tex.image = img
        else:
            return None

        mat.texture_slots.add()
        slot = mat.texture_slots[0]
        slot.texture = tex
        slot.texture_coords = 'UV'

        return mat

    def setup_plane_mesh_uv(mesh):
        bm = bmesh.new()
        bm.from_mesh(mesh)
        bm.faces.ensure_lookup_table()
        uv_layer = bm.loops.layers.uv[0]
        bm.faces[0].loops[0][uv_layer].uv = (0, 0)
        bm.faces[0].loops[1][uv_layer].uv = (1, 0)
        bm.faces[0].loops[2][uv_layer].uv = (1, 1)
        bm.faces[0].loops[3][uv_layer].uv = (0, 1)
        bm.to_mesh(mesh)

    def present_images():
        x = 0
        z = 0
        images = indexer.filter_assets_by_key('mime_type', 'image', lambda x, y: x.startswith(y))
        chrono_sorted_images_index = indexer.sort_assets_by_key('creation_date', images)
        for i in chrono_sorted_images_index:
            image = images[i]

            # Check unicity of objects, if it already exists, do not recreate it
            if link_if_object_exists(image.path):
                continue

            bpy.ops.mesh.primitive_plane_add(radius=scale/2, rotation=(pi / 2, 0, 0), location=(x, 0, z))

            obj = bpy.context.scene.objects.active
            obj['metadata'] = image.metadata
            bpy.context.scene.update()
            if not obj:
                continue
            obj['full_path'] = image.path
            mesh = obj.data
            select_shelf(obj, ShelfType.IMAGES)
            x, z = calculate_image_position(image, obj, scale, x, z)
            mat = create_material_assets(obj.name, image)
            if not mat:
                continue
            obj['material'] = obj.name + '_mat'
            mesh.uv_textures.new()
            if len(mesh.materials) < 1:
                mesh.materials.append(mat)
            else:
                mesh.materials[0] = mat
            setup_plane_mesh_uv(mesh)

    def present_3dmodels():
        x = 0
        z = 0

        blend_objects = list(indexer.custom_objects)

        # Objects found in .blend files
        for o in indexer.objects:
            current_object = bpy.data.objects[o['element_name']]
            if current_object.name not in bpy.context.scene.objects:
                bpy.context.scene.objects.link(current_object)
            # Either individual objects or parent objects only
            if current_object and (not current_object.parent or current_object.parent.name == current_object.get('model_parent')):
                blend_objects.append(o)

        for o in blend_objects:
            path = o.get('path')
            if not path:
                continue

            # Check unicity of objects, if it already exists, do not recreate it
            link_if_object_exists(path, True)

            from_blend = o.get('from_blend')
            if from_blend:
                current_object = bpy.data.objects[o['element_name']]
                model_objects = [current_object]
                current_object.location = (0, 0, 0)
                model_objects.extend(BlenderUtils.get_children(current_object))
                for o in bpy.context.scene.objects:
                    o.select = False
            else:
                lower_path = path.lower()
                if lower_path.endswith('.obj'):
                    bpy.ops.import_scene.obj(filepath=path)
                elif lower_path.endswith('.3ds'):
                    bpy.ops.import_scene.autodesk_3ds(filepath=path)
                elif lower_path.endswith('.fbx'):
                    bpy.ops.import_scene.fbx(filepath=path)
                else:
                    continue
                model_objects = [o for o in bpy.context.scene.objects if o.select]

            if not len(model_objects):
                continue

            bpy.context.scene.update()

            # We use an empty object to parent all the parts of the model
            bpy.ops.object.empty_add()
            empty_obj = bpy.context.scene.objects.active
            empty_obj['full_path'] = path
            empty_obj.name = model_objects[0].name + '_parent'

            # We make it so the models fit in a bounding box of dimension 2 * self.scale
            # We also use the loop to set the full path to the object
            max_coords = list()
            min_coords = list()
            for obj in model_objects:
                obj['model_object'] = True  # Needed if the linking to the origin scene is deferred to the caller
                obj_min_coords, obj_max_coords = get_object_min_max_coords(obj)
                max_coords.append(obj_max_coords)
                min_coords.append(obj_min_coords)
                obj.select = True

            try:
                max_coords = [i for i in map(max, zip(*max_coords))]
                min_coords = [i for i in map(min, zip(*min_coords))]
            except:
                print('Cannot detect bounding box for model {}, probably empty'.format(path))
                return

            # Save the individual elements names in a string
            empty_obj['model_objects'] = str()
            for obj in model_objects:
                empty_obj['model_objects'] += model_element_separator + obj.name
                if not obj.parent:
                    obj.parent = empty_obj
                obj['model_parent'] = empty_obj.name
            empty_obj['model_objects'] = empty_obj['model_objects'].strip(model_element_separator)

            empty_obj.select = True
            diff = max([abs(cmax - cmin) for cmax, cmin in zip(max_coords, min_coords)])
            ratio = scale / diff if diff != 0 else 1.0
            empty_obj.matrix_world = Matrix.Translation(Vector((x, 0, z))) * empty_obj.matrix_world
            empty_obj.scale *= ratio
            select_shelf(empty_obj, ShelfType.MODELS_3D)
            empty_obj.select = False

            x, z = calculate_object_position(scale, x, z)

    def present_materials():
        if not modification_target:
            return

        materials = list(indexer.materials)

        # We append the textures to the list like materials but we create the appropriate material
        for tex in indexer.textures:
            mat = bpy.data.materials.new(tex['element_name'])
            mat.use_shadeless = True
            mat.texture_slots.add()
            slot = mat.texture_slots[0]
            slot.texture = bpy.data.textures[tex['element_name']]
            slot.texture_coords = 'UV'
            tex['element_name'] = mat.name  # In case the name is already taken and suffixed by blender
            materials.append(tex)

        x = 0
        z = 0
        for mat in materials:
            # In this case we always recreate the object in case the model changed.
            # It is a simple copy so it is fast anyway.
            link_if_object_exists(mat.get('path'), True)

            obj_location = (x, 0, z)
            obj = modification_target.copy()
            if modification_target.data:
                obj.data = modification_target.data.copy()

            obj.parent = None
            obj.location = (0, 0, 0)
            obj.rotation_euler = (0, 0, 0)
            obj['model_object'] = False  # Hackish but solves potential picking issues
            bpy.context.scene.objects.link(obj)
            parent = obj.get('model_parent')
            if parent:
                obj.scale = bpy.data.objects[parent].scale
            ratio = scale / max(obj.dimensions)
            obj.scale *= ratio

            bpy.context.scene.update()
            obj.matrix_world = Matrix.Translation(obj_location) * obj.matrix_world
            select_shelf(obj, ShelfType.MATERIALS)
            x, z = calculate_object_position(scale, x, z)
            obj['full_path'] = mat['path']
            obj['material'] = mat['element_name']

            # Enables smooth shading for the active object
            obj.select = True
            mesh = obj.data
            for face in mesh.polygons:
                face.use_smooth = True
            if len(mesh.materials) < 1:
                mesh.materials.append(bpy.data.materials[mat['element_name']])
            else:
                mesh.materials[0] = bpy.data.materials[mat['element_name']]

    def select_shelf(obj, shelf_type):
        """
        :param obj: Object to put on the right shelf 
        :param shelf_type: Can be IMAGES, MATERIALS or MODELS_3D 
        """
        bpy.context.scene.update()
        offset = 2 * scale
        if shelf_type == ShelfType.IMAGES:
            translate_to_shelf_matrix = Matrix.Identity(4)
            rotate_to_shelf_matrix = Matrix.Identity(4)
        elif shelf_type == ShelfType.MATERIALS:
            translate_to_shelf_matrix = Matrix.Translation(Vector((-offset, -(max_length * scale + offset), 0)))
            rotate_to_shelf_matrix = Matrix.Rotation(pi / 2, 4, 'Z')
        elif shelf_type == ShelfType.MODELS_3D:
            translate_to_shelf_matrix = Matrix.Translation(Vector((max_length * scale + offset, -offset, 0)))
            rotate_to_shelf_matrix = Matrix.Rotation(-pi / 2, 4, 'Z')
        else:
            return
        obj.matrix_world = translate_to_shelf_matrix * rotate_to_shelf_matrix * obj.matrix_world

    def link_if_object_exists(full_path, remove_if_exists=False):
        # Check unicity of objects, if it already exists, do not recreate it
        object_found = False
        for o in bpy.data.objects:
            if o.get('full_path') == full_path:
                object_found = True
                if remove_if_exists:
                    bpy.data.objects.remove(o, True)
                    break
                if o.name not in bpy.context.scene.objects:
                    bpy.context.scene.objects.link(o)
                # If it is a 3D model, link all the components of the models too
                model_objects = get_model_objects(o)
                if model_objects:
                    for mo in model_objects:
                        if mo.name not in bpy.context.scene.objects:
                            bpy.context.scene.objects.link(mo)
                break
        return object_found

    if not show_sounds:
        if show_images:
            present_images()
        if show_3dmodels:
            present_3dmodels()
        if show_materials:
            present_materials()

    return max_length * scale


def get_model_objects(obj):
    """
    Returns a list of valid objects if the parameter contains a `model_objects` property or None otherwise
    """
    models_list = list()
    if not obj:
        return models_list
    model_objects_str = obj.get('model_objects')
    if model_objects_str:
        model_objects = model_objects_str.split(model_element_separator)
        if model_objects:
            models_list = list()
            for mo in model_objects:
                sub_obj = bpy.data.objects[mo]
                if sub_obj:
                    models_list.append(sub_obj)
                    models_list.extend(BlenderUtils.get_children(sub_obj))
    return models_list


def calculate_image_position(asset, obj, scale, x, z):
    width = int(asset.get_value('width'))
    height = int(asset.get_value('height'))

    ratio = 1
    if width and height:
        ratio = width / height

    if ratio != 1:
        if ratio < 1:
            obj.scale[0] *= ratio
        else:
            obj.scale[1] /= ratio

    return calculate_object_position(scale, x, z)


def calculate_object_position(scale, x, z):
    x += scale * 1.5
    if x > max_length * scale:
        x = 0
        z -= scale * 1.5
    return x, z


def instantiate_sound_objects(scale, layer):
    x = 0
    z = 0
    for plugin in bpy.satie.plugins:
        found = False
        # Reuse the same object if it already exists
        for o in bpy.data.objects:
            if o.get('satie_object') and o.satie.plugin_family == plugin.type and o.satie.synth == plugin.id:
                found = True
                obj = o
                if o.name not in bpy.context.scene.objects:
                    bpy.context.scene.objects.link(o)
        if not found:
            bpy.ops.mesh.primitive_cube_add(radius=scale/2, location=(x, 0, z))
            obj = bpy.context.scene.objects.active
            obj['satie_object'] = True
            obj.satie.plugin_family = plugin.type
            obj.satie.synth = plugin.id
            obj.satie.enabled = False
            obj.select = False
            x, z = calculate_object_position(scale, x, z)

        # We always recreate the texts because they are disposed when leaving the state.
        text_satie = TextField(axis='XZ', size=0.2*scale, text=obj.satie.synth, color=(0, 0, 0))
        text_satie.matrix_world = Matrix.Translation(Vector((0, -(scale / 2 + 0.001), 0))) * obj.matrix_world
        layer.add_child(text_satie)
