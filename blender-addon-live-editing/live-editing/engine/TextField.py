import math
import os
from PIL import Image, ImageFont, ImageDraw
from . import Object3D
from .geometry import Plane
from .textures import PILTexture
from .materials import MeshBasicMaterial
from ..Constants import FONT_BASE_PATH, FONT_PPM


class TextField(Object3D):

    fonts = {}

    @staticmethod
    def get_font_instance(font, size):
        family_dic = TextField.fonts.get(font)
        if family_dic is None:
            family_dic = {}
            TextField.fonts[font] = family_dic
        font_obj = family_dic.get(size)
        if font_obj is None:
            font_obj = ImageFont.truetype(font=font, size=size)
            family_dic[size] = font_obj
        return font_obj

    def __init__(self, text="Label", font=None, size=.30, color=(1.0, 1.0, 1.0), shadow=None, axis='XY'):
        super().__init__(
            geometry=Plane(axis=axis),
            material=MeshBasicMaterial(diffuse_map=PILTexture())
        )

        self._text = text
        self._font = font if font is not None else "SourceSansPro-Light.ttf"
        self._font_obj = None
        self._size = size
        self._color = color
        self._shadow = shadow

        self._text_width = 0
        self._text_height = 0

    # region TextField Properties

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        if self._text != value:
            self._text = value
            self._dirty_attributes = True

    @property
    def font(self):
        return self._font

    @font.setter
    def font(self, value):
        if self._font != value:
            self._font = value
            self._dirty_attributes = True

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, value):
        if self._size != value:
            self._size = value
            self._dirty_attributes = True

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        if self._color != value:
            self._color = value
            self._dirty_attributes = True

    @property
    def shadow(self):
        return self._shadow

    @shadow.setter
    def shadow(self, value):
        if self._shadow != value:
            self._shadow = value
            self._dirty_attributes = True

    # endregion

    # region Computed Dimensions

    @property
    def text_width(self):
        return self._text_width

    @property
    def text_height(self):
        return self._text_height

    # endregion

    def update_attributes(self):
        super().update_attributes()

        if not self._visible:
            return

        self._font_obj = TextField.get_font_instance(
            font=FONT_BASE_PATH + self._font,
            size=round(self._size * FONT_PPM)
        )
        text_texture_width, text_texture_height = self._font_obj.getsize(self._text)

        if text_texture_width <= 0 or text_texture_height <= 0:
            self._material.diffuse_map.set_image(None)
            self._bypass_render = True
        else:
            self._bypass_render = False
            text_offset = self._font_obj.getoffset(self._text)
            shadow_offset = 0
            if self._shadow is not None:
                shadow_offset = math.ceil(self._shadow * FONT_PPM)
                text_texture_width += shadow_offset
                text_texture_height += shadow_offset
            text_texture_width -= text_offset[0]
            text_texture_height -= text_offset[1]
            ratio = text_texture_width / text_texture_height

            self._geometry.width = self._text_width = text_texture_width / FONT_PPM
            self._geometry.height = self._text_height = text_texture_height / FONT_PPM

            int_color = int(self._color[0] * 255), int(self._color[1] * 255), int(self._color[2] * 255)

            # Blend on same color as text otherwise we get background color bleeding around the transparent edges
            image = Image.new('RGBA', (text_texture_width, text_texture_height), (*int_color, 0) if self._shadow is None else (0, 0, 0, 0))

            if self._shadow is not None:
                shadow_image = Image.new('RGBA', (text_texture_width, text_texture_height), (0, 0, 0, 0))
                shadow_draw = ImageDraw.Draw(shadow_image)
                shadow_draw.text(
                    (-text_offset[0] + shadow_offset, -text_offset[1] + shadow_offset),
                    self._text,
                    fill=(0, 0, 0, 255),
                    font=self._font_obj
                )
                image = Image.alpha_composite(image, shadow_image)

            draw = ImageDraw.Draw(image)
            draw.text(
                (-text_offset[0], -text_offset[1]),
                self._text,
                fill=(*int_color, 255),
                font=self._font_obj
            )

            self._material.diffuse_map.set_image(image)
