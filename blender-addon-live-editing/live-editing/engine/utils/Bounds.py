from mathutils import Vector
from collections import namedtuple


class Bounds:
    def __init__(self, min=(0.0, 0.0, 0.0), max=(0.0, 0.0, 0.0)):
        self._min = Vector(min)
        self._max = Vector(max)

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def width(self):
        return self._max.x - self._min.x

    @property
    def height(self):
        return self._max.z - self._min.z

    @property
    def depth(self):
        return self._max.y - self._min.y
