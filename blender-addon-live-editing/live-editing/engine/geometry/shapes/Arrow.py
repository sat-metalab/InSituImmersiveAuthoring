from .. import Shape


class Arrow(Shape):

    def __init__(self, length=1, width=1, head_length_ratio=0.5, head_width_ratio=0.5, line_width=1):
        super().__init__(line_width=line_width)

        self._length = length
        self._width = width
        self._head_length_ratio = head_length_ratio
        self._head_width_ratio = head_width_ratio
        self._line_width = line_width

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, value):
        if self._length != value:
            self._length = value
            self._dirty_geometry = True

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        if self._width != value:
            self._width = value
            self._dirty_geometry = True

    @property
    def head_length_ratio(self):
        return self._head_length_ratio

    @head_length_ratio.setter
    def head_length_ratio(self, value):
        if self._head_length_ratio != value:
            self._head_length_ratio = value
            self._dirty_geometry = True

    @property
    def head_width_ratio(self):
        return self._head_width_ratio

    @head_width_ratio.setter
    def head_width_ratio(self, value):
        if self._head_width_ratio != value:
            self._head_width_ratio = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        hl = self._length / 2
        hw = self._width / 2
        head_bottom = hl - (self._length * self._head_length_ratio)
        bar_hw = (self._width * self._head_width_ratio) / 2

        self._vertices = [
            0, hl, 0,
            hw, head_bottom, 0,
            bar_hw, head_bottom, 0,
            bar_hw, -hl, 0,
            -bar_hw, -hl, 0,
            -bar_hw, head_bottom, 0,
            -hw, head_bottom, 0,
            0, hl, 0
        ]
