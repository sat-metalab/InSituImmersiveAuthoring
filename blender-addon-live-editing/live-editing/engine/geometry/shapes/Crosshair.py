from .. import Shape


class Crosshair(Shape):
    def __init__(self, size=1, line_width=1):
        super().__init__(line_width=line_width)
        self._size = size

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, value):
        if self._size != value:
            self._size = value
            self._dirty_geometry = False

    def update_geometry(self):
        super().update_geometry()

        hs = self._size / 2

        # Because we don't have a curves array on our shape object
        # here we simply just pass by the center between the two lines
        self._vertices = [
            0, hs, 0,
            0, -hs, 0,
            0, 0, 0,
            -hs, 0, 0,
            hs, 0, 0
        ]
