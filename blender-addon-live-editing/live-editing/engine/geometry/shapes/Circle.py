import math
from .. import Shape


class Circle(Shape):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/CircleGeometry.js
    """

    def __init__(self, radius=0.5, segments=32, theta_start=0, theta_length=math.pi * 2, line_width=1):
        super().__init__(line_width=line_width)

        self._radius = radius
        self._segments = segments
        self._theta_start = theta_start
        self._theta_length = theta_length

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, value):
        if self._radius != value:
            self._radius = value
            self._dirty_geometry = True

    @property
    def segments(self):
        return self._segments

    @segments.setter
    def segments(self, value):
        segments = max(3.00, math.floor(value))
        if self._segments != segments:
            self._segments = segments
            self._dirty_geometry = True

    @property
    def theta_start(self):
        return self._theta_start

    @theta_start.setter
    def theta_start(self, value):
        if self._theta_start != value:
            self._theta_start = value
            self._dirty_geometry = True

    @property
    def theta_length(self):
        return self._theta_length

    @theta_length.setter
    def theta_length(self, value):
        if self._theta_length != value:
            self._theta_length = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        self._vertices = []

        # helper variables
        vertex = [0.00, 0.00, 0.00]

        i = 0
        for s in range(0, self._segments + 1):
            i += 3
            segment = self._theta_start + s / self._segments * self._theta_length

            # vertex
            vertex[0] = self._radius * math.cos(segment)
            vertex[1] = self._radius * math.sin(segment)
            self._vertices.extend(vertex)
