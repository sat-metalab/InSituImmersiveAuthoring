import math
from .. import Geometry


class Plane(Geometry):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/PlaneGeometry.js
    """

    def __init__(self, width=1, height=1, width_segments=1, height_segments=1, axis='XY'):
        super().__init__()

        self._width = width
        self._height = height
        self._width_segments = width_segments
        self._height_segments = height_segments
        self._axis = axis

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        if self._width != value:
            self._width = value
            self._dirty_geometry = True

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        if self._height != value:
            self._height = value
            self._dirty_geometry = True

    @property
    def width_segments(self):
        return self._width_segments

    @width_segments.setter
    def width_segments(self, value):
        width_segments = max(1.0, math.floor(value))
        if self._width_segments != width_segments:
            self._width_segments = width_segments
            self._dirty_geometry = True

    @property
    def height_segments(self):
        return self._height_segments

    @height_segments.setter
    def height_segments(self, value):
        height_segments = max(1.0, math.floor(value))
        if self._height_segments != height_segments:
            self._height_segments = height_segments
            self._dirty_geometry = True

    @property
    def axis(self):
        return self._axis

    @axis.setter
    def axis(self, value):
        if self._axis != value:
            self._axis = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        self._indices = []
        self._vertices = []
        self._normals = []
        self._uvs = []

        half_width = self._width / 2
        half_height = self._height / 2

        grid_x = self._width_segments
        grid_y = self._height_segments

        grid_x1 = grid_x + 1
        grid_y1 = grid_y + 1

        segment_width = self._width / grid_x
        segment_height = self._height / grid_y

        # generate vertices, normals and uvs
        for iy in range(0, grid_y1):
            y = iy * segment_height - half_height
            for ix in range(0, grid_x1):
                x = ix * segment_width - half_width
                if self._axis == 'XY':
                    self._vertices.extend([x, -y, 0])
                    self._normals.extend([0, 0, 1])
                elif self._axis == 'XZ':
                    self._vertices.extend([x, 0, -y])
                    self._normals.extend([0, 1, 0])
                elif self._axis == 'YZ':
                    self._vertices.extend([0, x, -y])
                    self._normals.extend([1, 0, 0])

                self._uvs.extend([ix / grid_x, 1 - (iy / grid_y)])

        # indices
        for iy in range(0, grid_y):
            for ix in range(0, grid_x):
                a = ix + grid_x1 * iy
                b = ix + grid_x1 * (iy + 1)
                c = (ix + 1) + grid_x1 * (iy + 1)
                d = (ix + 1) + grid_x1 * iy

                # faces
                self._indices.extend([a, b, d, b, c, d])
