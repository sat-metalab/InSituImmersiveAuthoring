from OpenGL.GL import *
from .. import Geometry


class Shape(Geometry):

    def __init__(self, vertices=None, line_width=1, mode=GL_LINE_STRIP):
        super().__init__()
        self._vertices = vertices if vertices is not None else []
        self._line_width = line_width
        self._mode = mode
        self._vertex_count = 0

    @Geometry.vertices.setter
    def vertices(self, value):
        if self._vertices != value:
            self._vertices = value
            self._dirty_geometry = True

    @property
    def line_width(self):
        return self._line_width

    @line_width.setter
    def line_width(self, value):
        if self._line_width != value:
            self._line_width = value

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if self._mode != value:
            self._mode = value

    def _update_geometry(self, time, delta, object_3d):
        super()._update_geometry(time, delta, object_3d)
        self._vertex_count = int(self._vertices_len / 3)

    def draw(self, object_3d):
        previous_line_width = glGetFloat(GL_LINE_WIDTH)
        if self._line_width != previous_line_width:
            glLineWidth(self._line_width)

        glDrawArrays(self._mode, 0, self._vertex_count)

        if self._line_width != previous_line_width:
            glLineWidth(previous_line_width)
