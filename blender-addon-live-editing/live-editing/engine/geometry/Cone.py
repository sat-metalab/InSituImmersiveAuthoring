import math
from . import Cylinder


class Cone(Cylinder):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/ConeGeometry.js
    """

    def __init__(self, radius=0.5, height=1, radial_segments=32, height_segments=1, open_ended=False, theta_start=0, theta_length=2.0*math.pi):
        super().__init__(0, radius, height, radial_segments, height_segments, open_ended, theta_start, theta_length)
