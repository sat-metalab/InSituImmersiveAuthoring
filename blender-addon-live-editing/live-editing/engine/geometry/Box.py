import math
from .. import Geometry


class Box(Geometry):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/BoxGeometry.js
    """

    def __init__(self, width=1, height=1, depth=1, width_segments=1, height_segments=1, depth_segments=1):
        super().__init__()

        self._width = width
        self._height = height
        self._depth = depth
        self._width_segments = width_segments
        self._height_segments = height_segments
        self._depth_segments = depth_segments

        self._vertex_count = 0

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        if self._width != value:
            self._width = value
            self._dirty_geometry = True

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        if self._height != value:
            self._height = value
            self._dirty_geometry = True

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        if self._depth != value:
            self._depth = value
            self._dirty_geometry = True

    @property
    def width_segments(self):
        return self._width_segments

    @width_segments.setter
    def width_segments(self, value):
        width_segments = max(1.0, math.floor(value))
        if self._width_segments != width_segments:
            self._width_segments = width_segments
            self._dirty_geometry = True

    @property
    def height_segments(self):
        return self._height_segments

    @height_segments.setter
    def height_segments(self, value):
        height_segments = max(1.0, math.floor(value))
        if self._height_segments != height_segments:
            self._height_segments = height_segments
            self._dirty_geometry = True

    @property
    def depth_segments(self):
        return self._depth_segments

    @depth_segments.setter
    def depth_segments(self, value):
        depth_segments = max(1.0, math.floor(value))
        if self._depth_segments != depth_segments:
            self._depth_segments = depth_segments
            self._dirty_geometry = True

    def build_plane(self, u, v, w, udir, vdir, width, height, depth, gridX, gridY, materialIndex):
        segment_width = width / gridX
        segment_height = height / gridY

        width_half = width / 2
        height_half = height / 2
        depth_half = depth / 2

        grid_x1 = gridX + 1
        grid_y1 = gridY + 1

        vertex_counter = 0

        # generate vertices, normals and uvs

        vector = [0.00, 0.00, 0.00]

        for iy in range(0, grid_y1):
            y = iy * segment_height - height_half
            for ix in range(0, grid_x1):
                x = ix * segment_width - width_half

                # set values to correct vector component
                vector[u] = x * udir
                vector[v] = y * vdir
                vector[w] = depth_half

                # now apply vector to vertex buffer
                self._vertices.extend(vector)

                # set values to correct vector component
                vector[u] = 0
                vector[v] = 0
                vector[w] = 1 if depth > 0 else - 1

                # now apply vector to normal buffer
                self._normals.extend(vector)

                # uvs
                self._uvs.append(ix / gridX)
                self._uvs.append(1 - (iy / gridY))

                # counters
                vertex_counter += 1

        # indices

        # 1. you need three indices to draw a single face
        # 2. a single segment consists of two faces
        # 3. so we need to generate six (2*3) indices per segment
        for iy in range(0, gridY):
            for ix in range(0, gridX):
                a = self._vertex_count + ix + grid_x1 * iy
                b = self._vertex_count + ix + grid_x1 * (iy + 1)
                c = self._vertex_count + (ix + 1) + grid_x1 * (iy + 1)
                d = self._vertex_count + (ix + 1) + grid_x1 * iy

                # faces
                self._indices.extend([a, b, d, b, c, d])

        # update total number of vertices
        self._vertex_count += vertex_counter

    def update_geometry(self):
        super().update_geometry()

        self._indices = []
        self._vertices = []
        self._normals = []
        self._uvs = []

        # build each side of the box geometry
        X = 0
        Y = 1
        Z = 2
        self.build_plane(Z, Y, X, - 1, - 1, self._depth, self._height,   self._width,  self._depth_segments, self._height_segments, 0)  # px
        self.build_plane(Z, Y, X,   1, - 1, self._depth, self._height, - self._width,  self._depth_segments, self._height_segments, 1)  # nx
        self.build_plane(X, Z, Y,   1,   1, self._width, self._depth,    self._height, self._width_segments, self._depth_segments,  2)  # py
        self.build_plane(X, Z, Y,   1, - 1, self._width, self._depth,  - self._height, self._width_segments, self._depth_segments,  3)  # ny
        self.build_plane(X, Y, Z,   1, - 1, self._width, self._height,   self._depth,  self._width_segments, self._height_segments, 4)  # pz
        self.build_plane(X, Y, Z, - 1, - 1, self._width, self._height, - self._depth,  self._width_segments, self._height_segments, 5)  # nz
