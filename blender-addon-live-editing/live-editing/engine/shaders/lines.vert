#version 330 compatibility

uniform mat4 proj_matrix;
uniform mat4 view_matrix;
uniform mat4 obj_matrix;

in vec3 vertex_position;

void main()
{
    mat4 mvp = proj_matrix * obj_matrix;
    gl_Position = mvp * vec4(vertex_position, 1.0);
}
