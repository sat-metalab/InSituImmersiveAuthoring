import weakref
from mathutils import Vector, Euler, Quaternion, Matrix
from OpenGL.GL import *
from .utils import Bounds
from ..utils import MatrixUtils, Logger
from ..utils.GLUtils import GLMat4x4
from . import Geometry
from .materials import Material
from ..Constants import DEBUG_ENGINE

IDENTITY = Matrix.Identity(4)


class Object3D:
    """
    A base 3D object
    """

    # Quick and dirty bag of objects to dispose
    # We can't access engine when disposing so using a static variable for now
    trash = []

    @staticmethod
    def empty_trash():
        if len(Object3D.trash) > 0:
            for obj in Object3D.trash:
                obj._dispose()
            Object3D.trash.clear()

    def __init__(self, geometry=None, material=None):
        if DEBUG_ENGINE:
            self._logger = Logger(self)

        self._engine = None
        self._scene = None
        self._parent = None
        self._children = []

        # Geometry
        if geometry is not None and not isinstance(geometry, Geometry):
            raise Exception("Geometry is not a Geometry subclass", geometry)
        self._geometry = geometry

        # Material
        if material is not None and not isinstance(material, Material):
            raise Exception("Material is not a Material subclass")
        self._material = material

        # Appearance
        self._visible = True
        self._bypass_render = False

        self._include_in_bounds = True
        self._ignore_depth = False
        self._skip_render = False

        # Interactive
        self._interactive = False
        self._block_interaction = False
        self._cursor_location = Vector()
        self._cursor_down = False
        self._cursor_entered = False
        self._cursor_over = False
        self._cursor_exited = False
        self._cursor_out = True

        self._dirty_attributes = True
        self._dirty_size = True
        self._dirty_matrix = True
        self._dirty_matrix_world = True
        self._dirty_gl_matrix = True

        self._location = Vector()
        self._rotation = Euler()  # Quaternion((1.00, 0.00, 0.00, 0.00))
        self._scale = Vector((1.00, 1.00, 1.00))

        # matrices
        self._matrix = Matrix.Identity(4)
        self._matrix_world = Matrix.Identity(4)

        # Internal use matrices
        self._offset_matrix = Matrix.Identity(4)
        self._translation_matrix = Matrix.Identity(4)
        self._rotation_matrix = Matrix.Identity(4)
        self._scale_matrix = Matrix.Identity(4)
        self._matrix_world_gl = GLMat4x4()  # MatrixUtils.flatten(self._matrix_world)

        self._bounds = Bounds()

        self._depth_enabled_before_draw = True
        self._instance = False

    def dispose(self):
        Object3D.trash.append(self)

    def _dispose(self):
        if DEBUG_ENGINE:
            self._logger.open("dispose")

        if not self._instance:
            if self._geometry is not None:
                self._geometry.dispose()
                self._geometry = None

            if self._material is not None:
                self._material.dispose()
                self._material = None

        # list() is used because we can't remove items from a list while we're
        # iterating over it
        if self._children is not None:
            for child in list(self._children):
                self.remove_child(child)
                child._dispose()  # Directly _dispose(), they wont be reachable with update() after we are disposed
            self._children = None

        if DEBUG_ENGINE:
            self._logger.close()

    # region Hierarchy

    @property
    def engine(self):
        return self._engine() if self._engine is not None else None  # weakref

    @property
    def scene(self):
        return self._scene() if self._scene is not None else None  # weakref

    @scene.setter
    def scene(self, value):
        self._scene = weakref.ref(value) if value is not None else None
        self._engine = weakref.ref(self.scene.engine) if self._scene is not None else None
        for child in self._children:
            child.scene = self.scene

    @property
    def parent(self):
        return self._parent() if self._parent is not None else None  # weakref
        
    # @parent.setter
    # def parent(self, value):
    #     self._parent = value

    @property
    def hierarchical_depth(self):
        return self.parent.hierarchical_depth + 1 if self.parent is not None else 0

    @property
    def children(self):
        return self._children

    @property
    def num_children(self):
        return len(self._children)

    def get_child_at(self, index):
        return self._children[index]

    def add_child(self, child):
        """
        Add child to the object
        :param child:
        :return:
        """
        self.add_child_at(child, self.num_children)

    def _add_child(self, child):
        """
        Minimal version, used by Flex Layout
        :param child:
        :return:
        """
        Object3D._add_child_at(self, child, self.num_children)

    def add_child_at(self, child, index):
        if not isinstance(child, Object3D):
            raise Exception("Trying to add an invalid child to the object")

        if child is self:
            raise Exception("Trying to add itself as a child")

        if child.parent is not None:
            child.parent.remove_child(child)

        child.scene = self.scene
        child._parent = weakref.ref(self)
        self._children.insert(index, child)

    def _add_child_at(self, child, index):
        # TODO: We don't really care about position for now but this is needed for the layout manager to be happy
        # We don't want control to reach the overridden add child of a sub class
        child._parent = weakref.ref(self)
        child.scene = self.scene
        self._children.insert(index, child)

    def remove_child(self, child):
        """
        Remove child from the object
        :param child: Object
        :return:
        """

        if not isinstance(child, Object3D):
            raise Exception("Trying to remove an invalid child from the group")

        try:
            self._children.remove(child)
        except:
            pass
        child._parent = None
        child._scene = None
        child._engine = None

    def remove_all_children(self):
        # list() is used because we can't iterate over a list and remove items from it at the same time
        for child in list(self._children):
            Object3D.remove_child(self, child)

    def traverse(self, callback):
        callback(self)
        for child in self._children:
            child.traverse(callback)

    def traverse_shallow(self, callback):
        if callback(self):
            for child in self._children:
                child.traverse_shallow(callback)

    # endregion

    # region Coordinates

    @property
    def x(self):
        return self._location.x

    @x.setter
    def x(self, value):
        if self._location.x != value:
            self._location.x = value
            self._dirty_matrix = True

    @property
    def y(self):
        return self._location.y

    @y.setter
    def y(self, value):
        if self._location.y != value:
            self._location.y = value
            self._dirty_matrix = True

    @property
    def z(self):
        return self._location.z

    @z.setter
    def z(self, value):
        if self._location.z != value:
            self._location.z = value
            self._dirty_matrix = True

    @property
    def location(self):
        return self._location
        
    @location.setter
    def location(self, value):
        if self._location != value:
            self._location = value
            self._dirty_matrix = True

    @property
    def rotation_x(self):
        return self._rotation.x

    @rotation_x.setter
    def rotation_x(self, value):
        if self._rotation.x != value:
            self._rotation.x = value
            self._dirty_matrix = True

    @property
    def rotation_y(self):
        return self._rotation.y

    @rotation_y.setter
    def rotation_y(self, value):
        if self._rotation.y != value:
            self._rotation.y = value
            self._dirty_matrix = True

    @property
    def rotation_z(self):
        return self._rotation.z

    @rotation_z.setter
    def rotation_z(self, value):
        if self._rotation.z != value:
            self._rotation.z = value
            self._dirty_matrix = True

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        if self._rotation != value:
            self._rotation = value
            self._dirty_matrix = True
        
    @property
    def scale_x(self):
        return self._scale.x

    @scale_x.setter
    def scale_x(self, value):
        if self._scale.x != value:
            self._scale.x = value
            self._dirty_matrix = True

    @property
    def scale_y(self):
        return self._scale.y

    @scale_y.setter
    def scale_y(self, value):
        if self._scale.y != value:
            self._scale.y = value
            self._dirty_matrix = True

    @property
    def scale_z(self):
        return self._scale.z

    @scale_z.setter
    def scale_z(self, value):
        if self._scale.z != value:
            self._scale.z = value
            self._dirty_matrix = True

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        if self._scale != value:
            self._scale = value
            self._dirty_matrix = True

    @property
    def matrix(self):
        return self._matrix

    @matrix.setter
    def matrix(self, value):
        if self._matrix != value:
            self._matrix = value
            # Update local coordinates from matrix
            location, rotation, scale = self._matrix.decompose()
            self._location = location
            self._rotation = rotation.to_euler()
            self._scale = scale
            self._dirty_matrix_world = True

    @property
    def matrix_world(self):
        return self._matrix_world

    @matrix_world.setter
    def matrix_world(self, value):
        if self._matrix_world != value:
            self._matrix_world = value
            # Update local matrix through the setter so that
            # the invalidation is triggered and propagated
            self.matrix = self.matrix_parent.inverted() * self._matrix_world
            self._dirty_matrix_world = True

    @property
    def matrix_parent(self):
        if self.parent is not None:
            return self.parent.matrix_world
        else:
            return IDENTITY

    # endregion

    # region Dimensions

    @property
    def bounds(self):
        return self._bounds

    # endregion

    # region Appearance

    @property
    def geometry(self):
        return self._geometry

    @geometry.setter
    def geometry(self, value):
        if self._geometry != value:
            if value is not None and not isinstance(value, Geometry):
                raise Exception("Geometry is not a Geometry subclass")
            self._geometry = value

    @property
    def material(self):
        return self._material

    @material.setter
    def material(self, value):
        if self._material != value:
            if value is not None and not isinstance(value, Material):
                raise Exception("Material is not a Material subclass")
            self._material = value

    @property
    def self_or_inherited_material(self):
        """
        Objects can inherit their parent's material if they have none
        :return: 
        """
        return self._material if self._material is not None else self.parent.material if self.parent is not None else None

    # endregion

    # region Interaction

    @property
    def interactive(self):
        return self._interactive

    @interactive.setter
    def interactive(self, value):
        if self._interactive != value:
            self._interactive = value
            # self._dirty_properties = True

    @property
    def block_interaction(self):
        """
        Do not descend into children to find interactive objects
        :return: 
        """
        return self._block_interaction

    @property
    def interactive_ancestry(self):
        if self._interactive:
            return True
        elif self.parent is not None:
            return self.parent.interactive_ancestry
        else:
            return False

    @property
    def cursor_location(self):
        return self._cursor_location

    @property
    def cursor_down(self):
        return self._cursor_down

    def on_cursor_pressed(self):
        pass

    def on_cursor_released(self):
        pass

    @property
    def cursor_entered(self):
        return self._cursor_entered

    def on_cursor_entered(self):
        pass

    @property
    def cursor_over(self):
        return self._cursor_over

    def on_cursor_over(self):
        pass

    @property
    def cursor_exited(self):
        return self._cursor_exited

    def on_cursor_exited(self):
        pass

    @property
    def cursor_out(self):
        return self._cursor_out

    def reset_hover(self):
        self.set_hover(False, Vector(), False)

    def set_hover(self, hovered, location, down):
        hover_changed = False

        self._cursor_location = self.matrix_world.inverted_safe() * location

        entered = hovered and not self._cursor_over
        if self._cursor_entered != entered:
            self._cursor_entered = entered
            if self._cursor_entered:
                self.on_cursor_entered()
            hover_changed = True

        exited = not hovered and self._cursor_over
        if self._cursor_exited != exited:
            self._cursor_exited = exited
            if self._cursor_exited:
                self.on_cursor_exited()
            hover_changed = True

        if self._cursor_over != hovered:
            self._cursor_over = hovered
            hover_changed = True
        if self._cursor_over:
            self.on_cursor_over()

        if self._cursor_out == hovered:
            self._cursor_out = not hovered
            hover_changed = True
        # Having an on_cursor out would be silly, wouldn't it? ;)

        if self._cursor_down != down:
            was_down = self._cursor_down
            if hovered:
                self._cursor_down = down
            if hovered and not was_down and self._cursor_down:
                self.on_cursor_pressed()
            elif was_down and not self._cursor_down:
                # Release even when outside
                self.on_cursor_released()

        if hover_changed:
            self._dirty_attributes = True

        return hover_changed

    # endregion

    # region Visibility

    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, value):
        if self._visible != value:
            self._visible = value
            self._dirty_attributes = True

    @property
    def include_in_bounds(self):
        return self._include_in_bounds

    @include_in_bounds.setter
    def include_in_bounds(self, value):
        if self._include_in_bounds != value:
            self._include_in_bounds = value
            self._dirty_size = True

    @property
    def ignore_depth(self):
        return self._ignore_depth

    @ignore_depth.setter
    def ignore_depth(self, value):
        self._ignore_depth = value

    # endregion

    # region Updates

    def update_attributes(self):
        """
        Update the object's properties
        :return:
        """
        self._dirty_attributes = False
        self._bypass_render = not self._visible

    def update_size(self):
        """
        Update the object's size
        :return:
        """
        self.update_bounds()
        self._dirty_size = False

    def update_bounds(self):
        first = self._geometry is None  # We can skip the "first" initialization pass when we have geometry
        bounds_min = self._geometry.bounds.min.copy() if not first else Vector()
        bounds_max = self._geometry.bounds.max.copy() if not first else Vector()
        for child in self._children:
            if not child.include_in_bounds:
                continue

            child.update_matrix()  # We need an updated matrix before using it
            child_a = child.matrix * child.bounds.min  # They are not min.max anymore after the transformation
            child_b = child.matrix * child.bounds.max  # They are not min.max anymore after the transformation
            if first:
                # print("    " * self.hierarchical_depth, "!!!", child.__class__.__name__, "!!!", child_a, child_b)
                bounds_min.x = min(child_a.x, child_b.x)
                bounds_min.y = min(child_a.y, child_b.y)
                bounds_min.z = min(child_a.z, child_b.z)
                bounds_max.x = max(child_a.x, child_b.x)
                bounds_max.y = max(child_a.y, child_b.y)
                bounds_max.z = max(child_a.z, child_b.z)
                first = False
            else:
                # print("    " * self.hierarchical_depth, ">>>", child.__class__.__name__, ">>>", bounds_min, bounds_max, child_a, child_b)
                bounds_min.x = min(bounds_min.x, child_a.x, child_b.x)
                bounds_min.y = min(bounds_min.y, child_a.y, child_b.y)
                bounds_min.z = min(bounds_min.z, child_a.z, child_b.z)
                bounds_max.x = max(bounds_max.x, child_a.x, child_b.x)
                bounds_max.y = max(bounds_max.y, child_a.y, child_b.y)
                bounds_max.z = max(bounds_max.z, child_a.z, child_b.z)
        # print("    " * self.hierarchical_depth, "===", self.__class__.__name__, "===", bounds_min, bounds_max)
        self._bounds = Bounds(bounds_min, bounds_max)
        if self.parent is not None:
            # Size validation happens after children, so we won't get another cycle just for this
            self.parent._dirty_size = True

    def update_matrix(self):
        """
        Update the object's matrix
        :return:
        """
        self._translation_matrix[0][3] = self._location.x
        self._translation_matrix[1][3] = self._location.y
        self._translation_matrix[2][3] = self._location.z
        self._rotation_matrix = self._rotation.to_matrix().to_4x4()
        self._scale_matrix[0][0] = self._scale.x
        self._scale_matrix[1][1] = self._scale.y
        self._scale_matrix[2][2] = self._scale.z
        mx = self._translation_matrix * self._rotation_matrix * self._scale_matrix * self._offset_matrix
        if mx != self._matrix:
            self._dirty_matrix_world = True
        self._matrix = mx
        self._dirty_matrix = False

    def update_matrix_world(self):
        """
        Update the object's world matrix
        :return:
        """
        self._matrix_world = self.matrix_parent * self._matrix
        self._dirty_matrix_world = False
        self._dirty_gl_matrix = True
        for child in self._children:
            child._dirty_matrix_world = True

    def update_gl_matrix(self):
        # self._matrix_world_gl = GLMat4x4(*MatrixUtils.flatten(self._matrix_world)) # Optimization, instead of doing it on every draw
        MatrixUtils.flatten_in(self._matrix_world, self._matrix_world_gl)  # Optimization, instead of doing it on every draw
        self._dirty_gl_matrix = False

    def update_input(self):
        """
        Update the objects's input
        This runs in the state machine's loop, not the renderer's
        :return:
        """
        for child in self._children:
            if not child.block_interaction:
                """ Explicitely non-interactive objects don't need to be updated """
                child.update_input()

    # endregion

    def update(self, time, delta):
        """
        Update the object
        :return:
        """

        # Properties influence all the rest so are updated first
        if self._dirty_attributes:
            self.update_attributes()

        # Matrices are required by self and children so do it before
        if self._dirty_matrix:
            self.update_matrix()

        # Matrices are required by self and children so do it before
        if self._dirty_matrix_world:
            self.update_matrix_world()

        if self._dirty_gl_matrix:
            self.update_gl_matrix()

        # Update children first, it will save us some invalidation cycles
        for child in self._children:
            child.update(time, delta)

        # When using components and containers, children can invalidate the parent's
        # geometry and/or size so update afterwards
        if self._geometry:
            self._geometry.update(time, delta, self)

        # Geometry can invalidate size, so update after geometry
        if self._dirty_size:
            self.update_size()

        # Do material last
        if self._material is not None:
            self._material.update(time, delta)

    # region Rendering

    def render(self):
        if DEBUG_ENGINE:
            self._logger.open("render")

        if self._bypass_render:
            if DEBUG_ENGINE:
                self._logger.close("bypassed")
            return

        self.pre_draw()
        self.draw()
        for child in self._children:
            child.render()
        self.post_draw()

        if DEBUG_ENGINE:
            self._logger.close()

    def pre_draw(self):
        if DEBUG_ENGINE:
            self._logger.open("pre_draw")

        if self._ignore_depth:
            self._depth_enabled_before_draw = glGetBoolean(GL_DEPTH_TEST)
            glDisable(GL_DEPTH_TEST)

        if self._geometry:
            if not self._instance:
                if self._material is not None:
                    self._material.bind(self)
                self._geometry.pre_draw(self)

            # Set object's world matrix in program
            glUniformMatrix4fv(self.engine.program.object_matrix_uniform, 1, GL_FALSE, self._matrix_world_gl)

    def draw(self):
        if DEBUG_ENGINE:
            self._logger.open("draw")

        if self._geometry:
            self._geometry.draw(self)
            if not self._instance:
                self._geometry.post_draw(self)
                if self._material is not None:
                    self._material.unbind(self)

        if DEBUG_ENGINE:
            self._logger.close()

    def post_draw(self):
        if self._ignore_depth and self._depth_enabled_before_draw:
            glEnable(GL_DEPTH_TEST)

        if DEBUG_ENGINE:
            self._logger.close("post_draw")

    # endregion
