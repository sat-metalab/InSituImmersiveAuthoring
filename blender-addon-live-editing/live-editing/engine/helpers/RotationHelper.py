import math
from OpenGL.GL import *
from .. import Object3D
from ..geometry import Torus
from ..materials import MeshBasicMaterial


class RotationHelper(Object3D):

    tube_radius = 0.025
    tubular_segments = 96

    def __init__(self, dimension=1):
        super().__init__()

        self._dimension = dimension

        self._circle_x_geometry = Torus(
            radius=self._dimension / 2,
            tube=RotationHelper.tube_radius,
            tubular_segments=RotationHelper.tubular_segments
        )
        self._circle_x = Object3D(geometry=self._circle_x_geometry, material=MeshBasicMaterial())
        self._circle_x.material.color = (1.00, 0.00, 0.00)
        self._circle_x.rotation_y = - math.pi / 2
        self.add_child(self._circle_x)

        self._circle_y_geometry = Torus(
            radius=self._dimension / 2,
            tube=RotationHelper.tube_radius,
            tubular_segments=RotationHelper.tubular_segments
        )
        self._circle_y = Object3D(geometry=self._circle_y_geometry, material=MeshBasicMaterial())
        self._circle_y.material.color = (0.00, 1.00, 0.00)
        self._circle_y.rotation_x = math.pi / 2
        self.add_child(self._circle_y)

        self._circle_z_geometry = Torus(
            radius=self._dimension / 2,
            tube=RotationHelper.tube_radius,
            tubular_segments=RotationHelper.tubular_segments
        )
        self._circle_z = Object3D(geometry=self._circle_z_geometry, material=MeshBasicMaterial())
        self._circle_z.material.color = (0.00, 0.00, 1.00)
        self.add_child(self._circle_z)

    @property
    def dimension(self):
        return self._dimension

    @dimension.setter
    def dimension(self, value):
        if self._dimension != value:
            self._dimension = value
            self._dirty_attributes = True

    def update_attributes(self):
        super().update_attributes()

        self._circle_x_geometry.radius = self._dimension
        self._circle_y_geometry.radius = self._dimension
        self._circle_z_geometry.radius = self._dimension
