import math
from OpenGL.GL import *
from .. import Object3D
from . import ArrowHelper


class TranslationHelper(Object3D):

    arrow_radius = 0.025
    arrow_head_radius = 0.1
    arrow_head_length = 0.125

    def __init__(self, dimension=1):
        super().__init__()

        self._dimension = dimension

        self._arrow_x = ArrowHelper(
            color=(1.00, 0.00, 0.00),
            length=self._dimension,
            radius=TranslationHelper.arrow_radius,
            head_radius=TranslationHelper.arrow_head_radius,
            head_length=TranslationHelper.arrow_head_length
        )
        self._arrow_x.rotation_z = - math.pi / 2
        self.add_child(self._arrow_x)

        self._arrow_y = ArrowHelper(
            color=(0.00, 1.00, 0.00),
            length=self._dimension,
            radius=TranslationHelper.arrow_radius,
            head_radius=TranslationHelper.arrow_head_radius,
            head_length=TranslationHelper.arrow_head_length
        )
        self.add_child(self._arrow_y)

        self._arrow_z = ArrowHelper(
            color=(0.00, 0.00, 1.00),
            length=self._dimension,
            radius=TranslationHelper.arrow_radius,
            head_radius=TranslationHelper.arrow_head_radius,
            head_length=TranslationHelper.arrow_head_length
        )
        self._arrow_z.rotation_x = math.pi / 2
        self.add_child(self._arrow_z)

    @property
    def dimension(self):
        return self._dimension

    @dimension.setter
    def dimension(self, value):
        if self._dimension != value:
            self._dimension = value
            self._dirty_attributes = True

    def update_attributes(self):
        super().update_attributes()

        self._arrow_x.length = self._dimension
        self._arrow_y.length = self._dimension
        self._arrow_z.length = self._dimension
