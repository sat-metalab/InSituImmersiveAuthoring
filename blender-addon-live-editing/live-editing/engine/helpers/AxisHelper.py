from OpenGL.GL import *
from .. import Object3D
from ..geometry import Shape
from ..materials import LineBasicMaterial


class AxisHelper(Object3D):

    length = 1000  # meters

    def __init__(self, dimension=1000):
        super().__init__()

        self._inner = 0.00
        self._dimension = dimension
        self._dimension_changed = True

        self._x_axis = Object3D(
            material=LineBasicMaterial(color=(1.00, 0.00, 0.00), opacity=0.5),
            geometry=Shape(mode=GL_LINES)
        )
        self.add_child(self._x_axis)

        self._y_axis = Object3D(
            material=LineBasicMaterial(color=(0.00, 1.00, 0.00), opacity=0.5),
            geometry=Shape(mode=GL_LINES)
        )
        self.add_child(self._y_axis)

        self._z_axis = Object3D(
            material=LineBasicMaterial(color=(0.00, 0.00, 1.00), opacity=0.5),
            geometry=Shape(mode=GL_LINES)
        )
        self.add_child(self._z_axis)

    @property
    def dimension(self):
        return self._dimension

    @dimension.setter
    def dimension(self, value):
        if self._dimension != value:
            self._dimension = value
            self._dimension_changed = True
            self._dirty_attributes = True
            
    @property
    def inner(self):
        return self._inner

    @inner.setter
    def inner(self, value):
        if self._inner != value:
            self._inner = value
            self._dimension_changed = True
            self._dirty_attributes = True

    def update_attributes(self):
        super().update_attributes()

        if self._dimension_changed:
            self._dimension_changed = False

            self._x_axis.geometry.vertices = [
                -self._dimension, 0.00, 0.00, -self._inner, 0.00, 0.00,
                self._inner, 0.00, 0.00, self._dimension, 0.00, 0.00
            ]
            self._y_axis.geometry.vertices = [
                0.00, -self._dimension, 0.00, 0.00, -self._inner, 0.00,
                0.00, self._inner, 0.00, 0.00, self._dimension, 0.00
            ]
            self._z_axis.geometry.vertices = [
                0.00, 0.00, -self._dimension, 0.00, 0.00, -self._inner,
                0.00, 0.00, self._inner, 0.00, 0.00, self._dimension
            ]
