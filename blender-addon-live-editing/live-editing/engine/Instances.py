from . import Object3D
from ..Constants import DEBUG_ENGINE


class Pool:
    """
    Object pool
    Instances are created when needed and released to an available pool
    There is no min/max instance count, it will grow indefinitely if not properly managed 
    """

    def __init__(self, type):
        self._type = type
        self._available = []
        self._used = []
        self._users = 0

    @property
    def users(self):
        return self._users

    def add_user(self):
        self._users += 1

    def remove_user(self):
        self._users -= 1

    def get(self, *args, **kwargs):
        if len(self._available) > 0:
            obj = self._available.pop()
            if hasattr(obj, "init_instance") and callable(obj.init_instance):
                obj.init_instance(*args, **kwargs)
        else:
            obj = self._type(*args, **kwargs)
        self._used.append(obj)
        return obj

    def release(self, obj):
        self._used.remove(obj)
        self._available.append(obj)

    def release_all(self):
        freed = self._used
        self._used = []
        self._available = self._available + freed

    def dispose(self):
        self.release_all()  # Just a safety
        for obj in self._available:
            obj.dispose()
        self._used = None
        self._available = None


class Pools:
    """
    Manages a list of pools and its users, 
    deletes the pool when no users remain
    """

    def __init__(self):
        self._pools = {}

    def get(self, key):
        pool = self._pools.get(key)
        if pool is None:
            pool = Pool(key)
            self._pools[key] = pool
        pool.add_user()
        return pool

    def release(self, key):
        pool = self._pools.get(key)
        if pool is not None:
            pool.remove_user()
            if pool.users == 0:
                pool.dispose()
                self._pools.pop(key)


class Instances(Object3D):

    pools = Pools()

    def __init__(self, *args, of, **kwargs):
        super().__init__(*args, **kwargs)
        self._instance_type = of
        self._pool = Instances.pools.get(self._instance_type)
        self._instances = []

    def _dispose(self):
        for instance in self._instances:
            self._pool.release(instance)
        self._instances = None
        Instances.pools.release(self._instance_type)
        super()._dispose()

    def get(self, index, *args, **kwargs):
        if index < len(self._instances):
            """
            We already have an instance at this index,
            initialize it and return it
            """
            instance = self._instances[index]
            if hasattr(instance, "init_instance") and callable(instance.init_instance):
                instance.init_instance(*args, **kwargs)
            return instance

        while index >= len(self._instances):
            """
            We don't have an instance at this index
            fill the instances pool up to this point
            and return the requested index
            TODO: Maybe use a dict in order to not fill useless values
            """
            instance = self._pool.get(*args, **kwargs)
            instance._geometry = self._geometry
            instance._instance = True
            self._instances.append(instance)
            self.add_child(instance)
        return self._instances[index]

    def clean(self, count):
        if DEBUG_ENGINE:
            self._logger.open("clean", count)

        while len(self._instances) > count:
            instance = self._instances.pop()
            self.remove_child(instance)
            self._pool.release(instance)

        if DEBUG_ENGINE:
            self._logger.close()

    def draw(self):
        pass  # No draw for Instances

    def post_draw(self):
        if self._geometry and not self._instance:
            self._geometry.post_draw(self)
            if self._material is not None:
                self._material.unbind(self)
        super().post_draw()