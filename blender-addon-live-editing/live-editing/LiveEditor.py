import os
import bpy
import logging
import time
from bpy.types import SpaceView3D
from mathutils import Matrix
from transitions import logger
from transitions.extensions import HierarchicalMachine as Machine

"""
Bootstrap is here because it can override constants ;)
It has to be imported before everything else
"""
bootstrap = None
try:
    from .bootstrap import bootstrap
except ImportError as err:
    print("No bootstrap found or error in bootstrap. (" + str(err) + ")")

from . import vrpn_devices
from .editor import Controller, Picker, MenuManager, CursorManager
from .Constants import DEBUG_MACHINE, DEBUG_CONTROLLERS, DEBUG_FPS, SHOW_WAND, PROFILE, PROFILE_SAMPLES, PROFILE_ENGINE, PROFILE_MACHINE, EXIT_ON_LOOP_CONFLICT, LOG_STATES
from .engine import Engine, Object3D
from .engine.geometry import Shape
from .engine.materials import LineBasicMaterial
from .states import ImportingState, PickingState, EditingState, NavigatingState, CalibratingState, AnimatingState, PresentationState, CyclesRenderingState
from .utils import Logger

# Performance debugging
import cProfile, pstats, io

# Enable logging for transitions
if LOG_STATES:
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)


class LiveEditor(Machine):

    shelf_group = "[Shelf]"

    def __init__(self):

        # State Machine
        super().__init__(
            states=['idle'],
            initial='idle',
            send_event=True,  # Send event instead of arguments to callbacks
            auto_transitions=False,  # Does not create `to_state()` automatic transition methods
            # ignore_invalid_triggers=True
        )

        if DEBUG_MACHINE or DEBUG_CONTROLLERS:
            self._logger = Logger(self)

        self._locked = False
        self._rendering = False
        self._current_state = None
        self._last_state = None

        """ DEBUG """
        if DEBUG_FPS:
            self._last_run_end = None
            self._last_run_frame_times = []
            self._run_time_average = 0
            self._last_render_end = None
            self._last_render_frame_times = []
            self._render_time_average = 0

        """ States """
        self.add_states([
            CalibratingState(machine=self),
            NavigatingState(machine=self, on_exit=self.back_to_default_state),
            AnimatingState(machine=self, on_exit=self.back_to_default_state),
            PresentationState(machine=self, on_exit=self.back_to_default_state),
            CyclesRenderingState(machine=self, on_exit=self.back_to_default_state),
            PickingState(machine=self),
            EditingState(machine=self),
            ImportingState(machine=self, on_exit=self.back_to_default_state)
        ])

        """ Transitions """
        self.add_transition(trigger='connect', source='idle', dest='picking', before=self.before_connect, after=self.bootstrap, conditions=self.can_connect)
        self.add_transition(trigger='connect', source='*', dest='picking', before=self.before_connect, conditions=self.can_connect)
        self.add_transition(trigger='disconnect', source='*', dest='idle', before=self.before_disconnect)
        self.add_transition(trigger='reset', source='*', dest='idle')

        self.add_transition(trigger='select', source='picking', dest='editing', conditions='has_focused_object')
        self.add_transition(trigger='edit', source='picking', dest='editing', conditions='has_focused_object')
        self.add_transition(trigger='deselect', source='editing', dest='picking')
        self.add_transition(trigger='navigate', source='*', dest='navigating')
        self.add_transition(trigger='animate', source='*', dest='animating')
        self.add_transition(trigger='present', source='*', dest='presenting')
        self.add_transition(trigger='pick', source='*', dest='picking')
        self.add_transition(trigger='cycles_render', source='*', dest='cycles_rendering')
        self.add_transition(trigger='calibrate', source='*', dest='calibrating')

        self.add_transition(trigger='browse', source='*', dest='importing')
        self.add_transition(trigger='import_asset', source='importing', dest='picking')

        """ Controllers """
        self._controller1 = Controller()
        self._controller2 = Controller()

        self._controllers = [
            self._controller1,
            self._controller2
        ]

        """ Picker """
        self._picker = Picker(self)

        """ Defaults """
        self._default_camera_matrix = Matrix.Identity(4)

        """ Variables """
        self._preferences = bpy.context.user_preferences.addons[__package__.split('.')[0]].preferences
        self._state_object = None
        self._current_scene = None
        self._camera_matrix = self._default_camera_matrix
        self.selected_object = None
        self._scenes_to_remove = []

        """ 3D Engine """
        self._first_run = True
        self._engine = Engine(editor=self)
        SpaceView3D.draw_handler_add(self.render, (), 'WINDOW', 'POST_VIEW')
        SpaceView3D.draw_handler_add(self.render_offscreen, (), 'WINDOW', 'OFFSCREEN_POST_VIEW')

        """ HUD """
        self._hud_layer = Object3D()
        self._engine.scene.add_child(self._hud_layer)

        """ Menu Manager """
        self._menu_layer = Object3D()
        self._engine.scene.add_child(self._menu_layer)
        self._menu_manager = MenuManager(self, self._menu_layer)

        """ Cursor Manager """
        self._cursor_layer = Object3D()
        self._engine.scene.add_child(self._cursor_layer)
        self._cursor_manager = CursorManager(self, self._cursor_layer)

        """ WAND """
        if SHOW_WAND:
            self._wand_geometry = Shape([
                0.00, 0.00, 0.00,
                0.00, 1.00, 0.00
            ])
            self._wand = Object3D(
                geometry=self._wand_geometry,
                material=LineBasicMaterial(color=(0.5, 0.5, 1.0))
            )
            self._engine.scene.add_child(self._wand)

        """ PROFILER """
        if PROFILE:
            self._pr = cProfile.Profile()
            self._pr.enable()
            self._samples = 0

    # region Properties

    @property
    def controller1(self):
        return self._controller1

    @property
    def controller2(self):
        return self._controller2

    @property
    def picker(self):
        return self._picker

    @property
    def engine(self):
        return self._engine

    @property
    def ui_scale(self):
        return self.preferences.ui.ui_scale

    @property
    def assets_path(self):
        return self.preferences.gallery.assets_path

    @property
    def show_axis_ref(self):
        return bpy.context.scene.state_settings.show_axis_ref

    @show_axis_ref.setter
    def show_axis_ref(self, value):
        bpy.context.scene.state_settings.show_axis_ref = value

    @property
    def show_axis(self):
        return bpy.context.scene.state_settings.show_axis

    @show_axis.setter
    def show_axis(self, value):
        bpy.context.scene.state_settings.show_axis = value

    @property
    def lock_x_axis(self):
        return bpy.context.scene.state_settings.lock_x_axis

    @lock_x_axis.setter
    def lock_x_axis(self, value):
        bpy.context.scene.state_settings.lock_x_axis = value

    @property
    def lock_y_axis(self):
        return bpy.context.scene.state_settings.lock_y_axis

    @lock_y_axis.setter
    def lock_y_axis(self, value):
        bpy.context.scene.state_settings.lock_y_axis = value

    @property
    def lock_z_axis(self):
        return bpy.context.scene.state_settings.lock_z_axis

    @lock_z_axis.setter
    def lock_z_axis(self, value):
        bpy.context.scene.state_settings.lock_z_axis = value

    @property
    def hud(self):
        return self._hud_layer

    @property
    def cursor_layer(self):
        return self._cursor_layer

    @property
    def state_object(self):
        return self._state_object

    @property
    def camera_matrix(self):
        return self._camera_matrix

    @property
    def menu_manager(self):
        return self._menu_manager

    @property
    def cursor_manager(self):
        return self._cursor_manager

    @property
    def preferences(self):
        return self._preferences

    @property
    def projection_mesh(self):
        mesh_name = self.preferences.projection.mesh
        return bpy.data.objects.get(mesh_name) if mesh_name is not None else None

    @property
    def pod_camera(self):
        return bpy.data.objects[bpy.context.scene.projection_settings.pod_camera]

    @property
    def pod_camera(self):
        return bpy.data.objects[self.preferences.projection.pod_camera]

    @property
    def camera(self):
        return bpy.data.objects[self.preferences.projection.camera]

    @property
    def shelf(self):
        return bpy.data.objects.get(LiveEditor.shelf_group)

    @property
    def pod(self):
        return bpy.data.objects[self.preferences.projection.pod]

    # endregion

    def initialize(self):
        pass
        # import bpy
        # bpy.ops.debug.connect_debugger_pycharm()

    # region State Callbacks

    def bootstrap(self, event):
        """
        Utility bootstrap to start the editor in a specific state
        Implement a `bootstrap(editor)` in a git-ignored bootstrap.py file
        :param event: 
        :return: 
        """
        if bootstrap is not None:
            bootstrap(self)

    def can_connect(self, event):
        """
        Check is a connection is possible
        :param event:
        :return:
        """

        if bpy.context.scene is None:
            return False

        if vrpn_devices.vrpn_enabled and not self.preferences.vrpn.host:
            return False

        # Either VRPN is not supported or it is configured correctly, so connect
        # We support not having VRPN for debugging purposes
        return True

    def before_connect(self, event):
        """
        Connect transition, actually does the connection
        :param event:
        :return:
        """
        host = self.preferences.vrpn.host
        self.controller1.connect(host, self.preferences.vrpn.controller_1)
        self.controller2.connect(host, self.preferences.vrpn.controller_2)
        self.preferences.vrpn.connected = True

    def before_disconnect(self, event):
        """
        Disconnect transition, actually does the disconnection
        :param event:
        :return:
        """
        for controller in self._controllers:
            controller.disconnect()
        self.preferences.vrpn.connected = False

    def has_focused_object(self, event):
        """
        Check if an object was provided for the edit transition
        or if we previously had a selected object (in order to restore selection when recalling a state)
        :param event:
        :return:
        """
        return event.kwargs.get('object') is not None or self.selected_object is not None

    # endregion

    # region State Navigation Methods

    def back_to_default_state(self):
        self.pick()

    def recall(self):
        if self._last_state is None:
            return False

        return self.trigger_state(self._last_state, default_trigger="pick")

    def trigger_state(self, state_name, default_trigger=None):
        state = self.get_state(state_name)
        if state is None:
            trigger_name = default_trigger
        elif state.trigger is not None:
            trigger_name = state.trigger
        elif state.verb is not None:
            trigger_name = state.verb
        else:
            trigger_name = default_trigger
        if trigger_name is None:
            print("Engine: trigger_state has not found a trigger for", state_name, default_trigger)

        try:
            trigger = getattr(self, trigger_name)
        except AttributeError:
            raise NotImplementedError("Class `{}` does not implement `{}`".format(self.__class__.__name__, trigger_name))

        if trigger is not None:
            trigger()
            return True
        else:
            return False

    # endregion

    def remove_scene(self, scene):
        """
        Flag a blender scene for removal
        
        Scenes can't be removed synchronously because they are used in the loop 
        so we have to wait the end of the loop to remove blender scenes.
        :param scene: 
        :return: 
        """
        self._scenes_to_remove.append(scene)

    def run(self, scene):
        """
        Run the state machine
        
        This is run once per "frame" per scene
        Anything changing the state of the editor should ben inside the _run method
        
        :param scene:
        :return:
        """

        # region Debug & Safety

        if DEBUG_MACHINE:
            self._logger.open("Run Requested")

        # Prevent recursive runs when we trigger an update
        if self._locked:
            if DEBUG_MACHINE:
                self._logger.close("Locked!")
            return

        if self._rendering:
            print("SKIPPING RUN WHILE RENDERING")
            if EXIT_ON_LOOP_CONFLICT:
                exit()
            return

        if DEBUG_FPS:
            start = time.time()
            if self._last_run_end is None:
                self._last_run_end = start

        # endregion

        self._locked = True

        self._run(scene)

        # region Debug

        if DEBUG_FPS:
            end = time.time()

            end_to_end = end - self._last_run_end
            self._last_run_end = end
            self._last_run_frame_times.append(end_to_end)
            self._run_time_average += end_to_end

            buff_len = len(self._last_run_frame_times)
            if buff_len >= 120:
                self._run_time_average -= self._last_run_frame_times.pop(0)

            print(
                "RUN >>>", round(1.00 / (self._run_time_average / buff_len), 3),
                "RENDER >>>", round(1.00 / (self._render_time_average / len(self._last_render_frame_times)), 3)
            )

        if DEBUG_MACHINE:
            self._logger.close()

        # endregion

        self._locked = False

    def _run(self, scene):
        """
        Run the state machine (internal)
        :param scene:
        :return:
        """

        # region Debug & Profiling

        if DEBUG_MACHINE:
            self._logger.open("Run")

        if PROFILE_MACHINE:
            pr = cProfile.Profile()
            pr.enable()

        # endregion

        if self._first_run:
            if DEBUG_MACHINE:
                self._logger.log("First run!")

            self._first_run = False
            self.initialize()

        # If scene changes, sync all windows on the same scene
        # This will reconnect controllers according the the new scene's config
        if scene != self._current_scene:
            if DEBUG_MACHINE:
                self._logger.log("Scene has changed, syncing windows to current scene")

            # Sync all windows to the new scene
            for window in bpy.context.window_manager.windows:
                window.screen.scene = scene

            self._current_scene = scene

        # Scene removal garbage collection
        for scene_to_remove in self._scenes_to_remove:
            bpy.data.scenes.remove(scene_to_remove, do_unlink=True)
        self._scenes_to_remove.clear()

        if not scene.live_editing.setup:
            if DEBUG_MACHINE:
                self._logger.close("Scene is not set up")
            return

        # If we are in the idle state, either attempt to connect if we were previously connected
        # or stop here, nothing concerns an unconnected machine from here
        if self.state == "idle":
            if DEBUG_MACHINE:
                self._logger.log("Idle")

            if bpy.context.scene.world is not None and self.preferences.vrpn.connected and self.can_connect(None):
                if DEBUG_MACHINE:
                    self._logger.log("Automatically connecting...")

                self.connect()
            else:
                if DEBUG_MACHINE:
                    self._logger.close("Idle")
                return

        # Update controller/input values
        if DEBUG_CONTROLLERS:
            self._logger.open("Updating controllers")

        for controller in self._controllers:
            controller.before_update()
        for controller in self._controllers:
            controller.update()

        if SHOW_WAND:
            self._wand_geometry.vertices = [*self.controller1.location, *self.picker.cursor_matrix.translation]

        if DEBUG_CONTROLLERS:
            self._logger.close()

        # Update camera matrix
        # Either use the configured camera or assume a (0.00, 0.00, 0.00) location
        if self.pod_camera is not None:
            # Rotate according to calibration
            self.pod_camera.rotation_euler.z = self.preferences.projection.pod_camera_rotation.z
            self._camera_matrix = self.pod_camera.matrix_world
        else:
            self._camera_matrix = self._default_camera_matrix

        # Update picker rays first
        # We need the picking vector for both the 3d engine and the states
        self.picker.update_rays()

        # CURSOR Camera Update
        cursor_camera = bpy.data.objects.get(self.preferences.projection.cursor_camera)
        if cursor_camera is not None:
            # cursor_camera.location = self._camera_matrix.translation
            if cursor_camera.rotation_mode != 'QUATERNION':
                cursor_camera.rotation_mode = 'QUATERNION'
            # This happens locally inside the in situ pod
            im = self.pod.matrix_world.inverted()
            cursor_camera.rotation_quaternion = (
                (im * self.picker.cursor_matrix).translation - (im * self._camera_matrix).translation
            ).to_track_quat('-Z', 'Y')

        # Check if menu was opened when starting this update round
        # If it was opened and closed in this cycle, we don't want to
        # fully run the state just now as when state changes occur
        # the next state catches the menu click and tries to handle it
        menu_was_opened = self._menu_manager.opened

        # Update the 3d engine (inputs, etc.)
        self.engine.input.ray_origin = self._camera_matrix.translation
        self.engine.input.ray_direction = self.picker.ray_vector_normalized
        self.engine.update_input()  # Input only for now... will trigger button actions

        # Get the current state, the machine doesn't provide the object, only the string
        # so we have to get the current state manually
        # Get it here because the menu can change the state
        # Also, only allow recall between top-level states in here, container states (like EditingState) manage
        # their own recall memory.
        self._state_object = self.get_state(self.state)
        if self.state != self._current_state and self._state_object.recallable and self._state_object.parent is None:
            # State memory for recall method
            self._last_state = self._current_state
            self._current_state = self.state

        # Update menu, needs to happen after engine to be current
        self._menu_manager.update()

        # Update Cursor
        self._cursor_manager.update()

        # region Debug & Profiling

        if PROFILE_MACHINE:
            pr.disable()
            s = io.StringIO()
            ps = pstats.Stats(pr, stream=s).sort_stats("cumtime")
            ps.print_stats()
            print(s.getvalue())

        # endregion

        # Run States
        if DEBUG_MACHINE:
            self._logger.open("Running state", self._state_object, self.state)

        self._state_object.execute(menu_was_opened or self._menu_manager.opened)

        # UI
        # NOTE: This was done before running the state until we realized that
        #       when animating the camera the HUD was jumping because the state
        #       is updating the pod's matrix_world but the engine was always using
        #       the old value. The cursor will still jump around though because it
        #       uses old values that were generated in the picker update.
        self._hud_layer.matrix_world = self.pod.matrix_world.copy()
        self._menu_layer.matrix_world = self.pod.matrix_world.copy()

        if DEBUG_MACHINE:
            self._logger.close()
            self._logger.close()

    def render(self):
        """
        Render loop for the engine 
        :return: 
        """

        # region Safety

        if self._locked:
            print("SKIPPING RENDER BY SAFETY")
            if EXIT_ON_LOOP_CONFLICT:
                exit()
            return

        if self._rendering:
            print("SKIPPING SIMULTANEOUS RENDER")
            if EXIT_ON_LOOP_CONFLICT:
                exit()
            return

        # endregion

        self._rendering = True

        # region Debug & Profiling

        if PROFILE_ENGINE:
            pr = cProfile.Profile()
            pr.enable()

        if DEBUG_FPS:
            start = time.time()
            if self._last_render_end is None:
                self._last_render_end = start

        # endregion

        self.engine.render()

        # region Debug & Profiling

        if DEBUG_FPS:
            end = time.time()

            end_to_end = end - self._last_render_end
            self._last_render_end = end
            self._last_render_frame_times.append(end_to_end)
            self._render_time_average += end_to_end

            buff_len = len(self._last_render_frame_times)
            if buff_len >= 120:
                self._render_time_average -= self._last_render_frame_times.pop(0)

        if PROFILE_ENGINE:
            pr.disable()
            s = io.StringIO()
            ps = pstats.Stats(pr, stream=s).sort_stats("cumtime")
            ps.print_stats()
            print(s.getvalue())

        if PROFILE:
            self._samples += 1
            if self._samples % int(PROFILE_SAMPLES/100) == 0:
                print("Samples: " + str(self._samples) + "/" + str(PROFILE_SAMPLES))
            if self._samples >= PROFILE_SAMPLES:
                self._pr.disable()
                s = io.StringIO()
                ps = pstats.Stats(self._pr, stream=s).sort_stats("cumtime")
                ps.print_stats()
                print(s.getvalue())
                exit()

        # endregion

        self._rendering = False

    def render_offscreen(self):
        """
        Offscreen render loop for the engine
        In case we need to do something differently when offscreen
        :return: 
        """
        self.render()


# "Global" Instance, just because I didn't know where to put it really
instance = None


def register():
    global instance
    instance = LiveEditor()


def unregister():
    global instance
    # TODO: instance.dispose()
    instance = None
