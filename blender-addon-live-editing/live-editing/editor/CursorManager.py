class CursorManager:

    def __init__(self, editor, container):
        self._editor = editor
        self._container = container
        self._container.ignore_depth = True
        self._cursor = None

    @property
    def editor(self):
        return self._editor

    @property
    def cursor(self):
        return self._cursor

    @cursor.setter
    def cursor(self, value):
        if self._cursor != value:
            if self._cursor is not None:
                self._container.remove_child(self._cursor)
            self._cursor = value
            if self._cursor is not None:
                self._cursor._editor = self._editor
                self._container.add_child(self._cursor)
            else:
                pass  # TODO: DEFAULT CURSOR

    def on_menu_opened(self):
        if self._cursor:
            self._cursor.on_menu_opened()

    def on_menu_closed(self):
        self._cursor.on_menu_closed()

    def update(self):
        """
        Update the cursor manager
        :return: 
        """
        if self._cursor:
            self._container.location = self.editor.picker.cursor_matrix.to_translation()
            self._container.rotation = self.editor.picker.cursor_matrix.to_quaternion()

            self._container.scale.x = self.editor.ui_scale
            self._container.scale_y = self.editor.ui_scale
            self._container.scale_z = self.editor.ui_scale

            self._cursor.update_cursor()
