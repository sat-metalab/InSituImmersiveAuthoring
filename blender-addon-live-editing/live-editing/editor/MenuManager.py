from mathutils import Matrix


class MenuManager:

    def __init__(self, editor, container):
        self._editor = editor
        self._container = container
        self._next_menu = None
        self._menu = None
        self._opened = False
        self._just_opened = False
        self._close_callback = None
        self.cursor_matrix_before_menu = Matrix.Identity(4)

    @property
    def editor(self):
        return self._editor

    @property
    def menu(self):
        return self._menu

    @property
    def opened(self):
        return self._opened

    def open_menu(self, menu):
        self._next_menu = menu
        if self._opened:
            self.close_menu(self._open_menu)
        else:
            self._open_menu()

    def _open_menu(self):
        if self._next_menu is None:
            return

        self.cursor_matrix_before_menu = self._editor.picker.cursor_matrix.copy()
        self._menu = self._next_menu
        self._menu._manager = self
        self._menu._state = self._editor.state_object
        self._menu.on_opening()
        self._menu.matrix_world = self.editor.pod.matrix_world.inverted() * self.cursor_matrix_before_menu
        self._container.add_child(self._menu)
        self._opened = True
        self._menu.on_opened()

        # XXX: Not ideal to reference the state from here...
        if self._editor.state_object is not None:
            self._editor.state_object.on_menu_opened()
        self._editor.cursor_manager.on_menu_opened()

    def close_menu(self, callback=None):
        self._close_callback = callback
        if not self._opened:
            if self._close_callback is not None:
                self._close_callback()
                self._close_callback = None
            return

        self._menu.on_closing(self._close_menu)

    def _close_menu(self):
        self._container.remove_child(self._menu)
        self._opened = False
        self._menu.on_closed()
        self._menu.dispose()
        self._menu = None

        # XXX: Not ideal to reference the state from here...
        if self._editor.state_object is not None:
            self._editor.state_object.on_menu_closed()
        self._editor.cursor_manager.on_menu_closed()

        if self._close_callback is not None:
            self._close_callback()
            self._close_callback = None

    def update(self):
        """
        Update the menu manager
        :return: 
        """

        # Open Menu
        state = self._editor.state_object
        if state is not None and state.menu is not None and not self._opened and self._editor.controller1.menu_button.pressed:
            # Open on press
            # Some actions will need to know where we clicked to open the menu
            self.open_menu(state.menu())
            self._just_opened = True

        # Update Menu
        if self._opened and self._menu is not None:
            self._menu.update_menu()

        # Close Menu
        if self._opened and self._editor.controller1.menu_button.released:
            # Close on release
            if self._just_opened:
                # We just opened the menu, so allow releasing the button without closing it right away
                self._just_opened = False
            else:
                self.close_menu()
