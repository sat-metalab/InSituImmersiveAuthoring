import bpy
import math
from mathutils import Vector, Quaternion, Matrix, Euler


class Picker:

    CURSOR_ANGLE = 0.1

    def __init__(self, editor):
        self.editor = editor
        self.reset(True)

        # Cursor
        self.cursor_matrix = Matrix.Identity(4)
        self.cursor_normal = Vector((0.00, 0.00, 0.00))

        # Ray
        self.ray_orientation = Quaternion((1.00, 0.00, 0.00, 0.00))
        self.ray_vector = Vector((0.00, 0.00, 0.00))
        self.ray_vector_normalized = Vector((0.00, 0.00, 0.00))

        # Result
        self.object = None
        self.location = Vector((0.00, 0.00, 0.00))
        self.normal = Vector((0.00, 0.00, 0.00))
        self.location_matrix = Matrix.Identity(4)

    def update_rays(self):
        """
        Update the rays used to pick objects
        This is split from the picking as it is also required when editing
        :return:
        """

        room_mesh = self.editor.projection_mesh

        # ROOM MESH INTERSECTION
        room_mesh_inverted_matrix = room_mesh.matrix_world.inverted()
        room_mesh_ray_matrix = room_mesh_inverted_matrix * self.editor.controller1.matrix
        room_mesh_ray_direction = Vector((0.0, 1.0, 0.0))  # Point with Y
        room_mesh_ray_direction.rotate(room_mesh_ray_matrix.to_quaternion())

        result = None
        # First try, assume we are inside the mesh
        try:
            result, loc, norm, index = room_mesh.ray_cast(room_mesh_ray_matrix.translation, room_mesh_ray_direction)
        except:
            pass

        if result and room_mesh_ray_direction.dot(norm) > 0:
            # We were outside the mesh, recast from the inside
            try:
                # Add a little bit of epsilon in the direction of the ray
                # so that we don't end up casting from inside the face
                e = room_mesh_ray_direction.copy()
                # Tried using sys.float_info.epsilon here but it would not work
                # Anyway, are we really going to map sub-millimeter objects? ;)
                e.length = 0.001
                result, loc, norm, index = room_mesh.ray_cast(loc + e, room_mesh_ray_direction)
            except:
                pass

        if not result:
            # print("Controller ray does not intersect with room mesh")
            self.reset(True)
            return

        # Hit Location Output
        mesh_intersection = room_mesh.matrix_world * loc
        self.cursor_normal = norm

        # CAMERA -> ROOM MESH INTERSECTION RAYCAST
        self.ray_vector = mesh_intersection - self.editor.camera_matrix.to_translation()
        self.ray_vector_normalized = self.ray_vector.normalized()

        # Align the track quat to the in situ pod orientation instead of the world
        self.ray_orientation = room_mesh.matrix_world.to_quaternion() * (room_mesh_inverted_matrix.to_quaternion() * self.ray_vector).to_track_quat('Y', 'Z')

        # Picker/Cursor matrix
        picker_rotation_matrix = self.ray_orientation.to_matrix().to_4x4()
        self.cursor_matrix = Matrix.Translation(mesh_intersection) * picker_rotation_matrix * Euler((math.pi/2, 0.00, 0.00)).to_matrix().to_4x4()

    def _raycast(self, obj):
        obj_inv_mat = obj.matrix_world.inverted_safe()
        obj_ray_loc = obj_inv_mat * self.editor.camera_matrix.translation
        obj_ray_vec = ((obj_inv_mat * self.cursor_matrix.translation) - obj_ray_loc).normalized()
        try:
            return obj.ray_cast(obj_ray_loc, obj_ray_vec)
        except:
            return None, None, None, None

    def pick(self):
        """
        Find the object being pointed at
        :return:
        """

        # Update the rays used to pick objects
        # NOTE: Now done manually in LiveEditor
        # self.update_rays()

        # Reset before picking
        self.object = None
        distance = math.inf

        for obj in bpy.context.scene.objects:
            # Only pick unlocked visible mesh objects
            if obj is None or obj.live_editing.lock or obj.type != 'MESH' or not obj.is_visible(bpy.context.scene):
                continue

            res, loc, norm, index = self._raycast(obj)
            if not res:
                continue

            dist = math.fabs(((obj.matrix_world * loc) - self.editor.camera_matrix.translation).length)
            if dist > distance:
                continue

            distance = dist

            self.object = obj
            self.location = loc
            self.normal = norm

        # Last resort, find the closest in the cursor's cone
        if not self.object:
            for obj in bpy.context.scene.objects:
                # Only pick unlocked visible mesh objects
                if obj is None or obj.live_editing.lock or obj.type != 'MESH' or not obj.is_visible(bpy.context.scene):
                    continue

                target = obj.matrix_world.to_translation() - self.editor.camera_matrix.translation
                angle = self.ray_vector.angle(target) if target.length > 0 and self.ray_vector.length > 0 else 0.00
                if angle >= Picker.CURSOR_ANGLE:  # Approx. fits the size of our cursor
                    continue

                # We pick by the closest from the center,
                # if this ends up being weird, the following will sort by distance instead
                # dist = math.fabs((obj.matrix_world.translation - self.editor.camera_matrix.translation).length)
                if angle > distance:
                    continue

                distance = angle

                self.object = obj
                self.location = Vector()  # We don't have that info, assume object's origin
                self.normal = Vector()  # We don't have that info

        if not self.object:
            self.reset(False)
            return False
        else:
            self.update_location_matrix()
            return True

    def pick_all(self):
        """
        Find all objects being pointed at
        :return:
        """

        objects = []
        for obj in bpy.context.scene.objects:
            # Only pick unlocked visible mesh objects
            if obj.live_editing.lock or obj.type != 'MESH' or not obj.is_visible(bpy.context.scene):
                continue
            res, loc, norm, index = self._raycast(obj)
            if not res:
                continue
            objects.append(obj)

        # Last resort, find objects inside the cursor's cone
        if len(objects) == 0:
            for obj in bpy.context.scene.objects:
                # Only pick unlocked visible mesh objects
                if obj is None or obj.live_editing.lock or obj.type != 'MESH' or not obj.is_visible(
                        bpy.context.scene):
                    continue
                target = obj.matrix_world.to_translation() - self.editor.camera_matrix.translation
                angle = self.ray_vector.angle(target) if target.length > 0 and self.ray_vector.length > 0 else 0.00
                if angle >= Picker.CURSOR_ANGLE:  # Approx. fits the size of our cursor
                    continue
                objects.append(obj)

        objects.sort(key=lambda obj: math.fabs((obj.matrix_world.translation - self.editor.camera_matrix.translation).length))

        return objects

    def pick_obj(self, obj):
        """
        Update the values for the picked object
        :return:
        """

        if obj is None:
            return

        # Update the rays used to pick objects
        # NOTE: Now done manually in LiveEditor
        # self.update_rays()

        res, loc, norm, index = self._raycast(obj)

        # Last resort, guestimate where we clicked inside the cursor's cone
        if not res:
            target = obj.matrix_world.to_translation() - self.editor.camera_matrix.translation
            angle = self.ray_vector.angle(target) if target.length > 0 and self.ray_vector.length > 0 else 0.00
            if angle < Picker.CURSOR_ANGLE:  # Approx. fits the size of our cursor
                res = True
                loc = Vector()  # We don't have that info, assume object's origin
                norm = Vector()  # We don't have that info

        self.object = obj if res else None
        self.location = loc
        self.normal = norm

        if not self.object:
            self.reset(False)
            return False
        else:
            self.update_location_matrix()
            return True

    def update_location_matrix(self):
        translation = Matrix.Translation(self.location)
        rotation = self.normal.to_track_quat('Y', 'Z').to_matrix().to_4x4()
        self.location_matrix = self.object.matrix_world * translation * rotation
        # Forget about scale
        self.location_matrix = Matrix.Translation(self.location_matrix.translation) * self.location_matrix.to_quaternion().to_matrix().to_4x4()

    def reset(self, reset_mesh_values):
        """
        Reset picking values
        :param reset_mesh_values:
        :return:
        """
        if reset_mesh_values:
            self.cursor_matrix = Matrix.Identity(4)
            self.cursor_normal = Vector((0.00, 0.00, 0.00))
            self.ray_orientation = Quaternion((1.00, 0.00, 0.00, 0.00))
            self.ray_vector = Vector((0.00, 0.00, 0.00))
            self.ray_vector_normalized = Vector((0.00, 0.00, 0.00))
        self.object = None
        self.location = Vector((0.00, 0.00, 0.00))
        self.normal = Vector((0.00, 0.00, 0.00))
        self.location_matrix = Matrix.Identity(4)
