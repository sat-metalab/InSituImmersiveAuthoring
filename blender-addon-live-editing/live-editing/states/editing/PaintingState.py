import bpy
import sys
import math
from time import time
from .. import BaseEditorState
from ...utils import BrushUtils
from ...components import Cursor
from ...engine import Object3D
from ...engine.geometry import Ring
from ...engine.geometry.shapes import Circle
from ...engine.materials import LineBasicMaterial, MeshBasicMaterial


class PaintingState(BaseEditorState):
    def __init__(self, machine, parent=None):
        super().__init__(
            machine=machine,
            parent=parent,
            name='painting',
            verb='paint'
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'trigger': "Paint",
                'trackpad': "Set Hue & Saturation (HSV)|Set Value (HSV)",
                'grab': "Recall last state"
            },
            'secondary': {
                'trigger': "Select Brush",
                'trackpad': "(x) Radius",
                'grab': "Modifier",
                'menu': "Deselect/Exit"
            }
        }

        # Control parameters
        self.trackpad_epsilon = 0.01 # trackpad update not used if smaller than this

        # Variables
        self.start_time = None
        self.area_center = (0.0, 0.0)
        self.previous_cont2_trackpad_x = 0.0
        self.previous_cont2_trackpad_y = 0.0

        self._overrider = None

        # UI
        self._cursor = None
        self._cursor_helper = None
        self._color_indicator = None
        self._surface_cursor = None
        self._surface_cursor_mesh = None

    def get_overrider(self):
        selected_object = self.editor.selected_object
        overrider = bpy.context.copy()
        w = 0
        for window in bpy.context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    if area.width > w:
                        w = area.width
                        for region in area.regions:
                            if region.type == 'WINDOW':
                                overrider['window'] = window
                                overrider['screen'] = screen
                                overrider['area'] = area
                                overrider['region'] = region
                                overrider['object'] = selected_object
                                overrider['active_object'] = selected_object
                                overrider['selected_objects'] = [selected_object]
                                overrider['sculpt_object'] = selected_object
                                break
            # Priority is given to the immersive viewport
            if window.screen.immersive_viewport.enabled:
                break

        return overrider

    def set_camera_view(self):
        for window in bpy.context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D' and area.spaces[0].camera == bpy.context.scene.camera:
                    if area.spaces[0].region_3d.view_perspective != 'CAMERA':
                        bpy.ops.view3d.viewnumpad(self._overrider, type='CAMERA')

                    # Fit the camera view to the 3d view
                    v3d = area.regions[4]
                    r3d = area.spaces[0].region_3d

                    r3d.view_camera_offset[0] = 0.0
                    r3d.view_camera_offset[1] = 0.0
                    a1 = v3d.width / v3d.height
                    a2 = bpy.context.scene.render.resolution_x / bpy.context.scene.render.resolution_y
                    if a2 < a1:
                        r3d.view_camera_zoom = 50 * (2 * math.sqrt(a2 / a1) - math.sqrt(2))
                    else:
                        r3d.view_camera_zoom = 29.29

                    # Store the area center
                    self.area_center = (v3d.width / 2.0, v3d.height / 2.0)

    def should_enter(self, event):
        should = super().should_enter(event)
        can = not self.editor.selected_object.live_editing.non_editable
        return should and can

    def enter(self, event):
        super().enter(event)

        # Here, we have to be sure that an object is selected
        self.editor.selected_object.select = True
        selected_object = self.editor.selected_object
        bpy.context.scene.objects.active = selected_object

        # Brush Setup
        bpy.ops.object.mode_set(mode='VERTEX_PAINT')
        bpy.context.scene.tool_settings.vertex_paint.brush = bpy.data.brushes['Draw']
        self._overrider = self.get_overrider()

        # Set the material to Mat_VertexPaint, so that color is visible in Material viewport shading mode
        if len(selected_object.material_slots.items()) == 0:
            bpy.ops.object.material_slot_add(self._overrider)
        selected_object.material_slots[0].material = bpy.data.materials['Mat_VertexPaint2']

        # Override the brush size with the one saved for the state
        bpy.context.scene.tool_settings.unified_paint_settings.size = bpy.context.scene.state_settings.paint_brush_size

        # Cursor
        self._cursor = Cursor(
            label=self.editor.selected_object.name,
            detail="Paint",
            color=(0.15, 0.65, 0.95),
            selection_chevron=True
        )
        self.editor.cursor_manager.cursor = self._cursor

        self._surface_cursor = Object3D()
        self._surface_cursor.visible = False
        self._surface_cursor_mesh = Object3D(
            geometry=Circle(
                segments=64,
                radius=0.30,
                line_width=2
            ),
            material=LineBasicMaterial(color=(1.0, 0.34, 0.13))
        )
        self._surface_cursor_mesh.y = 0.001
        self._surface_cursor_mesh.rotation_x = -math.pi / 2
        self._surface_cursor.add_child(self._surface_cursor_mesh)
        self._cursor.add_child(self._surface_cursor)

        # UI
        self._color_indicator = Object3D(
            geometry=Ring(inner_radius=0.0, outer_radius=0.15),
            material=MeshBasicMaterial(color=bpy.context.scene.tool_settings.vertex_paint.brush.color.copy())
        )
        self._color_indicator.x = 1.0
        self._color_indicator.y = 1.0
        self._cursor.add_child(self._color_indicator)

    def exit(self, event):
        super().exit(event)
        bpy.ops.object.mode_set(mode='OBJECT')

        self._cursor.dispose()
        self._cursor = None

        self._color_indicator = None
        self._surface_cursor = None
        self._surface_cursor_mesh = None

    def on_menu_opened(self):
        super().on_menu_opened()
        self._surface_cursor.visible = False
        self._color_indicator.visible = False

    def on_menu_closed(self):
        self._color_indicator.visible = True
        super().on_menu_closed()

    def run(self):
        super().run()

        selected_object = self.editor.selected_object
        cont1 = self.editor.controller1
        cont2 = self.editor.controller2

        brush_location = self.editor.picker.location
        stroke = [
            {
                "name": "first",
                "mouse": self.area_center,
                "pen_flip": False,
                "is_start": False,
                "location": brush_location,
                "pressure": 1.0,
                "time": 0.0,
                "size": 1.0
            }
        ]

        if len(selected_object.data.vertex_colors) == 0:
            bpy.ops.mesh.vertex_color_add(self._overrider)

        #
        # Painting option
        #
        # Color
        brush = bpy.context.scene.tool_settings.vertex_paint.brush
        if cont1.trackpad_button.touching and cont2.grab_button.down:
            # Set V (from HSV)
            y = (1.0 + cont1.trackpad_y) / 2.0
            brush.color.v = y
        elif cont1.trackpad_button.touching:
            # Set HS (from HSV)
            x = cont1.trackpad_x
            y = cont1.trackpad_y

            norm = math.sqrt(x*x + y*y) + sys.float_info.epsilon
            angle = math.acos(-(y / norm))
            if x > 0.0:
                angle = 2 * math.pi - angle
            hue = min(1.0, max(0.0, angle / (2 * math.pi)))
            saturation = min(1.0, max(0.0, math.sqrt(x*x + y*y)))
            brush.color.h = hue
            brush.color.s = saturation
        self._color_indicator.material.color = brush.color.copy()

        # Brush size
        if cont2.trackpad_button.touched:
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            self.previous_cont2_trackpad_y = cont2.trackpad_y
        elif cont2.trackpad_button.touching:
            # Set radius
            delta = cont2.trackpad_x - self.previous_cont2_trackpad_x
            if abs(delta) < self.trackpad_epsilon:
                delta = 0.0
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            bpy.context.scene.state_settings.paint_brush_size += round(delta * 10)
            bpy.context.scene.tool_settings.unified_paint_settings.size = bpy.context.scene.state_settings.paint_brush_size

        # Cursor Update
        if self.editor.picker.object is not None:
            self._surface_cursor_mesh.geometry.radius = BrushUtils.brush_radius(
                bpy.context.scene.tool_settings.unified_paint_settings.size,
                (self.editor.picker.location_matrix.to_translation() - self.editor.pod.matrix_world.to_translation()).length
            )
            self._surface_cursor.matrix_world = self.editor.picker.location_matrix.copy()
            self._surface_cursor.visible = True
        else:
            self._surface_cursor.visible = False

        self._cursor.detail = "Brush Size: " + str(
            round(bpy.context.scene.tool_settings.unified_paint_settings.size, 2))

        #
        # Not hovering the object: return
        #
        if not self.editor.picker.object or self.editor.picker.object.name != selected_object.name:
            return

        #
        # Do vertex paint
        #
        if cont1.trigger_button.pressed and self.editor.picker.object.name == selected_object.name:
            self.start_time = time()
            # Make sure the 3d view is rendered with the current bpy.context.scene.camera
            self.set_camera_view()
            bpy.ops.paint.vertex_paint(self._overrider, stroke=stroke, mode='INVERT')
        elif self.start_time is not None and cont1.trigger_button.down and self.editor.picker.object.name == selected_object.name:
            current_time = time() - self.start_time
            stroke[0]['is_start'] = True
            stroke[0]['time'] = current_time
            bpy.ops.paint.vertex_paint(self._overrider, stroke=stroke, mode='INVERT')
        elif self.start_time is not None and cont1.trigger_button.released and self.editor.picker.object.name == selected_object.name:
            self.start_time = None