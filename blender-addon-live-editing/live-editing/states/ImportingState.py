import bpy
from mathutils import Matrix
from . import BaseEditorState, NavigatingState
from ..gallery import Importer, Viewer
from ..components import Cursor
from ..components.menus import EditorRadialMenu
from ..gallery.Importer import GalleryMode, SlideshowPattern
from ..utils import BlenderUtils
from ..engine import Object3D


class ImportingState(BaseEditorState):

    def __init__(self, *args, on_exit, **kwargs):

        super().__init__(
            name='importing',
            **kwargs,
        )

        self._cursor = None
        self._gallery_mode = GalleryMode.COPY_OBJECTS
        self._display_sounds = True
        self._selected_objects = list()
        self._modification_target = None
        self._origin_scene = None
        self._last_focused_object = None
        self._selected_objects = list()
        self._origin_pod_matrix = None
        self._environment_lighting = None
        self._satie_text_layer = None
        self._translation_import_matrix = Matrix.Identity(4)
        self._menu = ImportingMenu
        self._on_exit = on_exit

        def _on_exit_navigating():
            self.editor.browse_stop()
            self.editor.cursor_manager.cursor = self._cursor

        self.machine.add_state(NavigatingState(machine=self.machine, parent=self, on_exit=_on_exit_navigating))
        self.machine.add_transition(trigger='browse_navigate', source='importing', dest='importing_navigating')
        self.machine.add_transition(trigger='browse_stop', source='importing_navigating', dest='importing')

    @property
    def gallery_mode(self):
        return self._gallery_mode

    @property
    def modification_target(self):
        return self._modification_target

    @property
    def origin_scene(self):
        return self._origin_scene

    @property
    def origin_pod_matrix(self):
        return self._origin_pod_matrix

    @property
    def translation_import_matrix(self):
        return self._translation_import_matrix

    def go_back(self):
        self._on_exit()

    @property
    def cursor(self):
        return self._cursor

    def enter(self, event):
        super().enter(event)
        self._cursor = Cursor()
        self._cursor.minimal = True
        self._origin_scene = bpy.context.scene
        self._origin_pod_matrix = self.editor.pod.matrix_world.copy()
        self.editor.cursor_manager.cursor = self._cursor
        self._display_sounds = True

        gallery_mode = event.kwargs.get('gallery_mode')
        if gallery_mode:
            self._gallery_mode = gallery_mode

        self._modification_target = event.kwargs.get('modification_target')

        cursor_matrix = event.kwargs.get('cursor_matrix')
        if cursor_matrix:
            cursor_in_pod_coords = self._origin_pod_matrix.inverted() * cursor_matrix
            cursor_translation = cursor_in_pod_coords.to_translation()
            cursor_translation.length = 4 * self.editor.ui_scale  # 4 is arbitratry but gives an acceptable result
            self._translation_import_matrix = self._origin_pod_matrix * Matrix.Translation(cursor_translation)

        self._environment_lighting = {
            'enabled': bpy.context.scene.world.light_settings.use_environment_light,
        }
        if self._environment_lighting['enabled']:
           self._environment_lighting['energy'] = bpy.context.scene.world.light_settings.environment_energy
           self._environment_lighting['color'] = bpy.context.scene.world.light_settings.environment_color

        scene_center = Importer.enter_gallery(path=self.editor.assets_path,
                                              scale=self.editor.ui_scale,
                                              gallery_mode=self._gallery_mode,
                                              modification_target=self._modification_target)
        bpy.ops.live_editing.setup_scene()

        # Set predefined environment lighting
        bpy.context.scene.world.light_settings.use_environment_light = True
        bpy.context.scene.world.light_settings.environment_energy = 1
        bpy.context.scene.world.light_settings.environment_color = 'PLAIN'

        self.editor.pod.matrix_world = Matrix.Translation(scene_center)

        if self._gallery_mode == GalleryMode.SOUNDS:
            self._modification_target.satie.enabled = False
            self._satie_text_layer = Object3D()
            self.editor.engine.scene.add_child(self._satie_text_layer)

    def exit(self, event):
        super().exit(event)

        current_scene = bpy.context.scene
        # Then go back to the origin scene
        bpy.context.screen.scene = self._origin_scene
        for o in self._origin_scene.objects:
            o.select = False

        # Finally deletes the created scene
        self.editor.remove_scene(current_scene)

        # Smart project if MATERIAL mode was used to texture an object
        if self._modification_target and self._gallery_mode == GalleryMode.MATERIAL:
            self._modification_target.select = True
            bpy.ops.uv.smart_project()

        # Resets the pod to its original position
        self.editor.pod.matrix_world = self._origin_pod_matrix

        # Resets the environment lighting settings
        bpy.context.scene.world.light_settings.use_environment_light = self._environment_lighting['enabled']
        if self._environment_lighting['enabled']:
            bpy.context.scene.world.light_settings.environment_energy = self._environment_lighting['energy']
            bpy.context.scene.world.light_settings.environment_color = self._environment_lighting['color']

        if self._gallery_mode == GalleryMode.SOUNDS:
            self.editor.engine.scene.remove_child(self._satie_text_layer)
            self._satie_text_layer.dispose()
            self._satie_text_layer = None

        if self._cursor:
            self._cursor.dispose()
            self._cursor = None

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

    def run(self):
        super().run()

        if self._gallery_mode == GalleryMode.SOUNDS and self._display_sounds:
            satie_addon = bpy.context.user_preferences.addons.get('satie')
            if satie_addon and satie_addon.preferences.active and len(bpy.satie.plugins):
                Viewer.instantiate_sound_objects(self.editor.ui_scale, self._satie_text_layer)
                self._display_sounds = False

        # Since we have a navigation sub-state in the ImportingState, we need to check this otherwise
        # we could still select objects while navigating.
        if self.machine.state != self.name:
            return

        # Update picking
        self.editor.picker.pick()

        def toggle_selected_state(obj):
            if obj in self._selected_objects:
                self._selected_objects.remove(obj)
            else:
                self._selected_objects.append(obj)

        parent = None
        if self.editor.picker.object:
            if self._gallery_mode == GalleryMode.SOUNDS:
                self.editor.picker.object.satie.enabled = True
            if self.editor.picker.object.get('model_object'):
                parent = bpy.data.objects.get(self.editor.picker.object['model_parent'])
            # Deselect old focused object it the focus changed
            if self._last_focused_object \
                    and self._last_focused_object != self.editor.picker.object \
                    and self._last_focused_object not in self._selected_objects:
                self._last_focused_object.select = False
                if self._gallery_mode == GalleryMode.SOUNDS:
                    self._last_focused_object.satie.enabled = False
            self._last_focused_object = self.editor.picker.object

            # Highlight all elements of a model if an element is in the picker
            if parent:
                parent.select = True
                for child in BlenderUtils.get_children(parent):
                    child.select = True
            else:
                self.editor.picker.object.select = True

            if self.editor.controller1.trigger_button.pressed:
                # Deselect all only if trigger 2 is not pressed or if we are texturing an object
                if self._gallery_mode == GalleryMode.MATERIAL or self._gallery_mode == GalleryMode.SOUNDS or not \
                        self.editor.controller2.trigger_button.down:
                    to_remove = list()
                    for obj in self._selected_objects:
                        # Do not deselect the siblings of the picker object
                        if obj.parent and obj.parent == parent:
                            break
                        elif obj != self.editor.picker.object:
                            obj.select = False
                            to_remove.append(obj)
                    for obj in to_remove:
                        self._selected_objects.remove(obj)
                # Add the picker object or all the objects of the model to the list of selected objects
                if parent:
                    toggle_selected_state(parent)
                    for child in BlenderUtils.get_children(parent):
                        toggle_selected_state(child)
                else:
                    toggle_selected_state(self.editor.picker.object)
        # Unselect the last focused object if it is not under the picker anymore
        elif self._last_focused_object:
            if self._last_focused_object not in self._selected_objects:
                if self._last_focused_object.get('model_object'):
                    parent = bpy.data.objects.get(self._last_focused_object['model_parent'])
                    if parent:
                        parent.select = False
                        for child in BlenderUtils.get_children(parent):
                            child.select = False
                self._last_focused_object.select = False
            if self._gallery_mode == GalleryMode.SOUNDS:
                self._last_focused_object.satie.enabled = False
            self._last_focused_object = None


class ImportingMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        def _exit(target):
            self._state.go_back()
            self.close()
        self.add_item("Exit", _exit)

        def _navigate(target):
            self.editor.browse_navigate()
            self.close()
        self.add_item("Navigate", _navigate)

    def on_opening(self):
        super().on_opening()

        def _texture(target):
            Importer.exit_gallery(gallery_mode=self._state.gallery_mode, origin_scene=self._state.origin_scene,
                                  modification_target=self._state.modification_target)
            self.editor.import_asset()

        def _slideshow(target):
            pattern = target.data
            Importer.exit_gallery(gallery_mode=self._state.gallery_mode, origin_scene=self._state.origin_scene,
                                  pattern=pattern, grid_size=self.editor.ui_scale * 1.5,
                                  translation_matrix=self._state.translation_import_matrix)
            self.editor.import_asset()

        def _import(target):
            objects = Importer.exit_gallery(gallery_mode=self._state.gallery_mode,
                                            origin_scene=self._state.origin_scene,
                                            join_models=self.editor.preferences.gallery.join_models)
            if objects and len(objects):
                Importer.image_slideshow(scene=self._state.origin_scene, pattern=SlideshowPattern.WALL,
                                         objects=objects, grid_size=self.editor.ui_scale * 1.5,
                                         translation_matrix=self._state.translation_import_matrix)
            self.editor.import_asset()

        def _attach_sound(target):
            Importer.exit_gallery(gallery_mode=self._state.gallery_mode,
                                  origin_scene=self._state.origin_scene,
                                  modification_target=self._state.modification_target)
            if self.editor.picker.object:
                self.editor.picker.object.satie.enabled = False
            self.editor.import_asset()

        if self._state.gallery_mode == GalleryMode.MATERIAL:
            self.add_item("Texture object", _texture)
        elif self._state.gallery_mode == GalleryMode.IMAGES:
            self.add_item("Wall", _slideshow, data=SlideshowPattern.WALL)
            self.add_item("Circle", _slideshow, data=SlideshowPattern.CIRCLE)
        elif self._state.gallery_mode == GalleryMode.COPY_OBJECTS:
            self.add_item("Import asset(s)", _import)
        elif self._state.gallery_mode == GalleryMode.SOUNDS:
            if self.editor.picker.object:
                self.editor.picker.object.satie.enabled = False
            self.add_item("Attach sound", _attach_sound)
