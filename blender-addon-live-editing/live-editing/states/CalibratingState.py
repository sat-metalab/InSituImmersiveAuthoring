import bpy
import math
from mathutils import Vector, Matrix, Euler
from . import BaseEditorState
from ..flex.components import Root, Slider, HGroup
from ..components import Cursor, MenuPanel
from ..engine import Object3D, TextField
from ..engine.materials import LineBasicMaterial
from ..engine.geometry.shapes import Crosshair


class CalibrationInterface(Root):
    def __init__(self, state):
        super().__init__()

        self._state = state

        self.ignore_depth = True

        self._container = HGroup(gap=0.05, vertical_align='JUSTIFY')
        self._container.horizontal_center = 0.00
        self._container.vertical_center = 0.00
        self.add_element(self._container)

        # Camera
        self.camera_menu = MenuPanel("Camera", gap=0.125)
        self.camera_menu.width = 2.0
        self._container.add_element(self.camera_menu)

        def _rotate_camera(target, value):
            camera = bpy.data.objects[self._state.editor.preferences.projection.pod_camera]
            if camera is None:
                print("No camera to rotate")
                return
            self._state.editor.preferences.projection.pod_camera_rotation.z = value
            orig_rot_mode = camera.rotation_mode
            camera.rotation_mode = 'XYZ'
            camera.rotation_euler.z = value
            camera.rotation_mode = orig_rot_mode
        self.camera_rotation_slider = Slider(label="Rotation", min=0.00, max=math.pi * 2, on_change=_rotate_camera)
        self.camera_menu.add_item(self.camera_rotation_slider)

        # Tracking
        self.tracking_menu = MenuPanel("Tracking", gap=0.125)
        self.tracking_menu.width = 2.0
        self._container.add_element(self.tracking_menu)

        def _tracking_translate_x(target, value):
            self._state.editor.preferences.tracking.tracking_space_position.x = value
        self.tracking_location_x_slider = Slider(label="Location X", min=-5.00, max=5.00, on_change=_tracking_translate_x)
        self.tracking_menu.add_item(self.tracking_location_x_slider)

        def _tracking_translate_y(target, value):
            self._state.editor.preferences.tracking.tracking_space_position.y = value
        self.tracking_location_y_slider = Slider(label="Location Y", min=-5.00, max=5.00, on_change=_tracking_translate_y)
        self.tracking_menu.add_item(self.tracking_location_y_slider)

        def _tracking_translate_z(target, value):
            self._state.editor.preferences.tracking.tracking_space_position.z = value
        self.tracking_location_z_slider = Slider(label="Location Z", min=-5.00, max=5.00, on_change=_tracking_translate_z)
        self.tracking_menu.add_item(self.tracking_location_z_slider)

        def _tracking_rotate_x(target, value):
            self._state.editor.preferences.tracking.tracking_space_rotation.x = value
        self.tracking_rotation_x_slider = Slider(label="Rotation X", min=0.00, max=math.pi * 2, on_change=_tracking_rotate_x)
        self.tracking_menu.add_item(self.tracking_rotation_x_slider)

        def _tracking_rotate_y(target, value):
            self._state.editor.preferences.tracking.tracking_space_rotation.y = value
        self.tracking_rotation_y_slider = Slider(label="Rotation Y", min=0.00, max=math.pi * 2, on_change=_tracking_rotate_y)
        self.tracking_menu.add_item(self.tracking_rotation_y_slider)

        def _tracking_rotate_z(target, value):
            self._state.editor.preferences.tracking.tracking_space_rotation.z = value
        self.tracking_rotation_z_slider = Slider(label="Rotation Z", min=0.00, max=math.pi * 2, on_change=_tracking_rotate_z)
        self.tracking_menu.add_item(self.tracking_rotation_z_slider)

        # Controller
        self.controller_menu = MenuPanel("Controller", gap=0.125)
        self.controller_menu.width = 2.0
        self._container.add_element(self.controller_menu)

        def _controller_rotate_x(target, value):
            self._state.editor.preferences.tracking.tracker_rotation.x = value
        self.controller_rotation_x_slider = Slider(label="Rotation X", min=0.00, max=math.pi * 2, on_change=_controller_rotate_x)
        self.controller_menu.add_item(self.controller_rotation_x_slider)

        def _controller_rotate_y(target, value):
            self._state.editor.preferences.tracking.tracker_rotation.y = value
        self.controller_rotation_y_slider = Slider(label="Rotation Y", min=0.00, max=math.pi * 2, on_change=_controller_rotate_y)
        self.controller_menu.add_item(self.controller_rotation_y_slider)

        def _controller_rotate_z(target, value):
            self._state.editor.preferences.tracking.tracker_rotation.z = value
        self.controller_rotation_z_slider = Slider(label="Rotation Z", min=0.00, max=math.pi * 2, on_change=_controller_rotate_z)
        self.controller_menu.add_item(self.controller_rotation_z_slider)

    def update_data(self):
        # Camera
        camera = bpy.data.objects[self._state.editor.preferences.projection.pod_camera]
        if camera is None:
            return
        self.camera_rotation_slider.value = self._state.editor.preferences.projection.pod_camera_rotation.z % (math.pi * 2)

        # Tracking
        self.tracking_location_x_slider.value = self._state.editor.preferences.tracking.tracking_space_position.x
        self.tracking_location_y_slider.value = self._state.editor.preferences.tracking.tracking_space_position.y
        self.tracking_location_z_slider.value = self._state.editor.preferences.tracking.tracking_space_position.z
        self.tracking_rotation_x_slider.value = self._state.editor.preferences.tracking.tracking_space_rotation.x % (math.pi * 2)
        self.tracking_rotation_y_slider.value = self._state.editor.preferences.tracking.tracking_space_rotation.y % (math.pi * 2)
        self.tracking_rotation_z_slider.value = self._state.editor.preferences.tracking.tracking_space_rotation.z % (math.pi * 2)
        self.controller_rotation_x_slider.value = self._state.editor.preferences.tracking.tracker_rotation.x % (math.pi * 2)
        self.controller_rotation_y_slider.value = self._state.editor.preferences.tracking.tracker_rotation.y % (math.pi * 2)
        self.controller_rotation_z_slider.value = self._state.editor.preferences.tracking.tracker_rotation.z % (math.pi * 2)


class CalibrationCursor(Cursor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        size = self.radius * 2
        self.custom_cursor = Object3D(
            geometry=Crosshair(
                size=size,
                line_width=2
            ),
            material=LineBasicMaterial()
        )

    @Cursor.radius.setter
    def radius(self, value):
        Cursor.radius.fset(self, value)
        size = value * 2
        self._custom_cursor.geometry.size = size


class CalibratingState(BaseEditorState):

    def __init__(self, machine, parent=None):
        super().__init__(
            name='calibrating',
            verb='calibrate',
            recallable=True,
            machine=machine,
            parent=parent
        )

        self._help_data = {
            'primary': {
                'menu': "Back",
                'trigger': "Change Mode",
                'trackpad': "Adjust Values"
            },
            'secondary': {
                'menu': "Exit",
                'trackpad': "Adjust Values"
            }
        }

        self._modes = ['CAMERA', 'TRACKING', 'CONTROLLER']
        self._mode = -1

        self.previous_controller_trackpad = {}
        self._ui_container = None
        self._interface = None
        self._calibration_ui = None
        self._calibration_text = None
        self._current_mode_label = None
        self._current_mode_detail_1 = None
        self._current_mode_detail_2 = None
        self._camera_ui = None
        self._tracking_ui = None
        self._tracker_ui = None
        self._cursor = None

    def enter(self, event):
        super().enter(event)

        ui_distance = 10  # TODO: Dynamically find out where to put it
        ui_height = 1

        self._mode = -1

        # Cursor
        self._cursor = CalibrationCursor()
        self.editor.cursor_manager.cursor = self._cursor

        # We need all of the ui elements to follow the user in the scene,
        # so group everything inside one container
        self._ui_container = Object3D()
        self.editor.hud.add_child(self._ui_container)

        # UI
        self._interface = CalibrationInterface(self)
        pos = Vector((0.00, ui_distance, 0.00))
        pos.rotate(Euler((math.pi / 4, 0.00, 0.00)))
        self._interface.location = pos.copy()
        self._interface.rotation_x = math.pi * 0.75
        self._ui_container.add_child(self._interface)

        # Helpers
        self._calibration_ui = Object3D()
        self._calibration_ui.ignore_depth = True
        self._ui_container.add_child(self._calibration_ui)

        self._calibration_text = Object3D()
        self._calibration_text.y = ui_distance
        self._calibration_text.z = ui_height + 2.00
        self._calibration_text.rotation_x = math.pi /2
        self._calibration_ui.add_child(self._calibration_text)

        self._current_mode_label = TextField(font="SourceSansPro-Bold.ttf", size=1.00, shadow=0.01)
        self._current_mode_label.y = 0.5
        self._calibration_text.add_child(self._current_mode_label)

        self._current_mode_detail_1 = TextField(font="SourceSansPro-Italic.ttf", size=.50, shadow=0.01)
        self._current_mode_detail_1.y = -0.5
        self._calibration_text.add_child(self._current_mode_detail_1)

        self._current_mode_detail_2 = TextField(font="SourceSansPro-Italic.ttf", size=.50, shadow=0.01)
        self._current_mode_detail_2.y = -1.00
        self._calibration_text.add_child(self._current_mode_detail_2)

        # Camera
        self._camera_ui = Object3D()
        self._camera_ui.visible = False
        self._calibration_ui.add_child(self._camera_ui)

        north = TextField(text="N", color=(0.00, 1.00, 0.00), font="SourceSansPro-Bold.ttf", size=2.00)
        north.y = ui_distance
        north.z = ui_height
        north.rotation_x = math.pi / 2
        self._camera_ui.add_child(north)
        
        south = TextField(text="S", color=(0.00, 1.00, 0.00), font="SourceSansPro-Bold.ttf", size=2.00)
        south.y = -ui_distance
        south.z = ui_height
        south.rotation_x = math.pi / 2
        south.rotation_z = math.pi
        self._camera_ui.add_child(south)
        
        east = TextField(text="E", color=(1.00, 0.00, 0.00), font="SourceSansPro-Bold.ttf", size=2.00)
        east.x = ui_distance
        east.z = ui_height
        east.rotation_x = math.pi / 2
        east.rotation_z = -math.pi / 2
        self._camera_ui.add_child(east)

        west = TextField(text="W", color=(1.00, 0.00, 0.00), font="SourceSansPro-Bold.ttf", size=2.00)
        west.x = -ui_distance
        west.z = ui_height
        west.rotation_x = math.pi / 2
        west.rotation_z = math.pi * 1.5
        self._camera_ui.add_child(west)

        # Tracking
        self._tracking_ui = Object3D()
        self._tracking_ui.visible = False
        self._calibration_ui.add_child(self._tracking_ui)

        north = Object3D(geometry=Crosshair(size=2, line_width=2), material=LineBasicMaterial(color=(0.00, 1.00, 0.00)))
        north.y = ui_distance
        north.rotation_x = math.pi / 2
        self._tracking_ui.add_child(north)

        south = Object3D(geometry=Crosshair(size=2, line_width=2), material=LineBasicMaterial(color=(0.00, 1.00, 0.00)))
        south.y = -ui_distance
        south.rotation_x = math.pi / 2
        south.rotation_z = math.pi
        self._tracking_ui.add_child(south)

        east = Object3D(geometry=Crosshair(size=2, line_width=2), material=LineBasicMaterial(color=(1.00, 0.00, 0.00)))
        east.x = ui_distance
        east.rotation_x = math.pi / 2
        east.rotation_z = -math.pi / 2
        self._tracking_ui.add_child(east)

        west = Object3D(geometry=Crosshair(size=2, line_width=2), material=LineBasicMaterial(color=(1.00, 0.00, 0.00)))
        west.x = -ui_distance
        west.rotation_x = math.pi / 2
        west.rotation_z = math.pi * 1.5
        self._tracking_ui.add_child(west)

        sky = Object3D(geometry=Crosshair(size=2, line_width=2), material=LineBasicMaterial(color=(0.00, 0.00, 1.00)))
        sky.z = ui_distance
        self._tracking_ui.add_child(sky)

        pos = Vector((0.00, ui_distance, 0.00))
        pos.rotate(Euler((math.pi / 4, 0.00, math.pi / 4)))
        for i in range(4):
            angled = Object3D(geometry=Crosshair(size=2, line_width=2), material=LineBasicMaterial(color=(0.5, 0.5, 0.5)))
            angled.location = pos.copy()
            angled.rotation_x = math.pi * 0.75
            angled.rotation_z = (math.pi / 2) * i + (math.pi / 4)
            self._tracking_ui.add_child(angled)
            pos.rotate(Euler((0.00, 0.00, math.pi / 2)))

    def exit(self, event):
        super().exit(event)
        self.previous_controller_trackpad = {}

        self.editor.hud.remove_child(self._ui_container)
        self._ui_container.dispose()
        self._ui_container = None

        self._interface = None
        self._calibration_ui = None

        self._calibration_text = None
        self._current_mode_label = None
        self._current_mode_detail_1 = None
        self._current_mode_detail_2 = None
        self._camera_ui = None
        self._tracking_ui = None
        self._tracker_ui = None

        self._cursor.dispose()
        self._cursor = None

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

    def _get_trackpad_delta(self, controller):
        delta_x = 0.00
        delta_y = 0.00
        previous = self.previous_controller_trackpad.get(controller)
        if previous is not None and not controller.trackpad_button.touched and controller.trackpad_button.touching:
            previous_x, previous_y = previous
            delta_x = (controller.trackpad_x - previous_x)
            delta_y = (controller.trackpad_y - previous_y)

        self.previous_controller_trackpad[controller] = controller.trackpad_x, controller.trackpad_y

        return delta_x, delta_y

    def run(self):
        super().run()

        if self.editor.controller2.menu_button.pressed:
            self.editor.pick()
            return

        elif self._mode != -1 and self.editor.controller1.trigger_button.pressed:
            # mode = -1 is there to protect against changing the mode instantly upon entering the
            # state, since we are pressing the trigger button
            self._mode = (self._mode + 1) % len(self._modes)

        if self._mode == -1:
            self._mode = 0

        mode = self._modes[self._mode]
        if mode == 'CAMERA':
            self._camera_ui.visible = True
            self._tracking_ui.visible = False
            
            self._current_mode_label.text = "Camera"
            self._current_mode_detail_1.text = "Use the trackpad on the secondary controller to rotate and align"
            self._current_mode_detail_2.text = "the scene relative to the projection space"

            delta_x, delta_y = self._get_trackpad_delta(self.editor.controller2)
            camera = self.editor.pod_camera
            if camera is None:
                print("No camera to calibrate")
                return
            self.editor.preferences.projection.pod_camera_rotation.z += delta_x * (self.editor.controller2.trackpad_y + 1.0)

        elif mode == 'TRACKING':
            self._camera_ui.visible = False
            self._tracking_ui.visible = True
            
            self._current_mode_label.text = "Tracking"
            self._current_mode_detail_1.text = "Aim at the targets with the primary controller and adjust alignment"
            self._current_mode_detail_2.text = "using the trackpads, primary = translation, secondary = rotation"

            primary_delta_x, primary_delta_y = self._get_trackpad_delta(self.editor.controller1)
            secondary_delta_x, secondary_delta_y = self._get_trackpad_delta(self.editor.controller2)

            self.editor.preferences.tracking.tracking_space_position += Vector((primary_delta_x, primary_delta_y, 0.00))
            self.editor.preferences.tracking.tracking_space_rotation.z += -secondary_delta_x * (self.editor.controller2.trackpad_y + 1.00)

        elif mode == 'CONTROLLER':
            self._camera_ui.visible = False
            self._tracking_ui.visible = True
            
            self._current_mode_label.text = "Controller"
            self._current_mode_detail_1.text = "Fine-tune the controller orientation"
            self._current_mode_detail_2.text = ""

            secondary_delta_x, secondary_delta_y = self._get_trackpad_delta(self.editor.controller2)
            self.editor.preferences.tracking.tracker_rotation.rotate(Euler((secondary_delta_y * 0.25, 0.00, -secondary_delta_x * 0.25)))

        self._interface.update_data()
