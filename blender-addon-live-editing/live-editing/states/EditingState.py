import bpy
import math
from collections import OrderedDict
from . import BaseEditorState, NavigatingState, AnimatingState
from .editing import PaintingState, SculptingState, TransformingState
from ..components import MenuPanel
from ..components.menus import EditorContextualMenu, EditorRadialMenu
from ..flex.components import Button, HGroup, Label, Slider, Spacer
from ..gallery.Importer import GalleryMode


class EditingMoreMenu(EditorContextualMenu):
    def __init__(self):
        super().__init__()

        # Actions menu
        self.action_menu = MenuPanel("Action")
        self._container.add_element(self.action_menu)
        self.lamp_menu = None

        def _deselect(target):
            self.editor.deselect()
            self.close()

        self.deselect_button = Button(label="Deselect", on_click=_deselect)
        self.action_menu.add_item(self.deselect_button)

        self.action_menu.add_item(Spacer(height=0.125))

        def _undo(target):
            bpy.ops.ed.undo()

        self.undo_button = Button(label="Undo", on_click=_undo)
        self.action_menu.add_item(self.undo_button)

        def _redo(target):
            bpy.ops.ed.redo()

        self.redo_button = Button(label="Redo", on_click=_redo)
        self.action_menu.add_item(self.redo_button)

        self.action_menu.add_item(Spacer(height=0.125))

        def _duplicate(target):
            new_obj = self.editor.selected_object.copy()
            new_obj.data = self.editor.selected_object.data.copy()
            new_obj.animation_data_clear()
            bpy.context.scene.objects.link(new_obj)
            self.editor.deselect()
            self.editor.select(object=new_obj)
            self.close()

        self.duplicate_button = Button(label="Duplicate", on_click=_duplicate)
        self.action_menu.add_item(self.duplicate_button)

        def _delete(target):
            bpy.context.scene.objects.unlink(self.editor.selected_object)
            self.editor.deselect()
            self.close()

        self.delete_button = Button(label="Delete", on_click=_delete)
        self.action_menu.add_item(self.delete_button)

        # endregion

        # region Axis menu

        self.axis_menu = MenuPanel("Axis")
        self._container.add_element(self.axis_menu)

        def _show_axis(target):
            self.editor.show_axis = target.selected
        self._show_axis_button = Button(label="Show Axis", on_click=_show_axis, toggle=True)
        self.axis_menu.add_item(self._show_axis_button)

        self.axis_menu.add_item(Spacer(height=0.125))

        def _change_axis(target):
            self.editor.show_axis_ref = 'LOCAL' if self.editor.show_axis_ref == 'WORLD' else 'WORLD'
            self._change_axis_local_button.selected = self.editor.show_axis_ref == 'LOCAL'
            self._change_axis_world_button.selected = self.editor.show_axis_ref == 'WORLD'
        self._change_axis_world_button = Button(label="World", on_click=_change_axis)
        self.axis_menu.add_item(self._change_axis_world_button)
        self._change_axis_local_button = Button(label="Local", on_click=_change_axis)
        self.axis_menu.add_item(self._change_axis_local_button)

        self.axis_menu.add_item(Spacer(height=0.125))
        self.axis_menu.add_item(Label(text="Edit axis"))
        self.axis_menu.add_item(Spacer(height=0.125))

        self._axis_lock = HGroup(gap=0.0625)
        self.axis_menu.add_item(self._axis_lock)

        def _lock_x_axis(target):
            self.editor.lock_x_axis = not target.selected
        self._lock_x_axis_button = Button(label="X", on_click=_lock_x_axis, toggle=True, horizontal_label_align='CENTER')
        self._lock_x_axis_button.percent_width = 1.0
        self._axis_lock.add_element(self._lock_x_axis_button)

        def _lock_y_axis(target):
            self.editor.lock_y_axis = not target.selected
        self._lock_y_axis_button = Button(label="Y", on_click=_lock_y_axis, toggle=True, horizontal_label_align='CENTER')
        self._lock_y_axis_button.percent_width = 1.0
        self._axis_lock.add_element(self._lock_y_axis_button)

        def _lock_z_axis(target):
            self.editor.lock_z_axis = not target.selected
        self._lock_z_axis_button = Button(label="Z", on_click=_lock_z_axis, toggle=True, horizontal_label_align='CENTER')
        self._lock_z_axis_button.percent_width = 1.0
        self._axis_lock.add_element(self._lock_z_axis_button)

        # endregion

        # region Properties menu
        self.properties_menu = MenuPanel("Properties", gap=0.125)
        self.properties_menu.width = 4.0
        self._container.add_element(self.properties_menu)

        # Reset
        def _reset(target):
            self.editor.selected_object.rotation_euler.x = 0.00
            self.editor.selected_object.rotation_euler.y = 0.00
            self.editor.selected_object.rotation_euler.z = 0.00
            self.editor.selected_object.scale.x = 1.00
            self.editor.selected_object.scale.y = 1.00
            self.editor.selected_object.scale.z = 1.00
        self.properties_menu.add_item(Button(label="Reset All", on_click=_reset))

        # region X Rotation
        self._rotate_x_group = HGroup(gap=0.0625)
        self.properties_menu.add_item(self._rotate_x_group)

        def _rotate_x(target, value):
            self.editor.selected_object.rotation_euler.x = value
        self.rotation_x_slider = Slider(label="Rotation X", min=0.00, max=math.pi * 2, on_change=_rotate_x)
        self.rotation_x_slider.percent_width = 1.00
        self._rotate_x_group.add_element(self.rotation_x_slider)

        def _rotate_x_reset(target):
            self.editor.selected_object.rotation_euler.x = 0.00
        self._rotate_x_group.add_element(Button(label="Reset", on_click=_rotate_x_reset))
        # endregion

        # region Y Rotation
        self._rotate_y_group = HGroup(gap=0.0625)
        self.properties_menu.add_item(self._rotate_y_group)

        def _rotate_y(target, value):
            self.editor.selected_object.rotation_euler.y = value
        self.rotation_y_slider = Slider(label="Rotation Y", min=0.00, max=math.pi * 2, on_change=_rotate_y)
        self.rotation_y_slider.percent_width = 1.00
        self._rotate_y_group.add_element(self.rotation_y_slider)

        def _rotate_y_reset(target):
            self.editor.selected_object.rotation_euler.y = 0.00
        self._rotate_y_group.add_element(Button(label="Reset", on_click=_rotate_y_reset))
        # endregion

        # region Z Rotation
        self._rotate_z_group = HGroup(gap=0.0625)
        self.properties_menu.add_item(self._rotate_z_group)

        def _rotate_z(target, value):
            self.editor.selected_object.rotation_euler.z = value
        self.rotation_z_slider = Slider(label="Rotation Z", min=0.00, max=math.pi * 2, on_change=_rotate_z)
        self.rotation_z_slider.percent_width = 1.00
        self._rotate_z_group.add_element(self.rotation_z_slider)

        def _rotate_z_reset(target):
            self.editor.selected_object.rotation_euler.z = 0.00
        self._rotate_z_group.add_element(Button(label="Reset", on_click=_rotate_z_reset))
        # endregion

        # region X Scale
        self._scale_x_group = HGroup(gap=0.0625)
        self.properties_menu.add_item(self._scale_x_group)

        def _scale_x(target, value):
            self.editor.selected_object.scale.x = value
        self.scale_x_slider = Slider(label="Scale X", min=0.00, max=10.00, on_change=_scale_x)
        self.scale_x_slider.percent_width = 1.00
        self._scale_x_group.add_element(self.scale_x_slider)

        def _scale_x_reset(target):
            self.editor.selected_object.scale.x = 0.00
        self._scale_x_group.add_element(Button(label="Reset", on_click=_scale_x_reset))
        # endregion

        # region Y Scale
        self._scale_y_group = HGroup(gap=0.0625)
        self.properties_menu.add_item(self._scale_y_group)

        def _scale_y(target, value):
            self.editor.selected_object.scale.y = value
        self.scale_y_slider = Slider(label="Scale Y", min=0.00, max=10.00, on_change=_scale_y)
        self.scale_y_slider.percent_width = 1.00
        self._scale_y_group.add_element(self.scale_y_slider)

        def _scale_y_reset(target):
            self.editor.selected_object.scale.y = 0.00
        self._scale_y_group.add_element(Button(label="Reset", on_click=_scale_y_reset))
        # endregion

        # region X Scale
        self._scale_z_group = HGroup(gap=0.0625)
        self.properties_menu.add_item(self._scale_z_group)

        def _scale_z(target, value):
            self.editor.selected_object.scale.z = value
        self.scale_z_slider = Slider(label="Scale Z", min=0.00, max=10.00, on_change=_scale_z)
        self.scale_z_slider.percent_width = 1.00
        self._scale_z_group.add_element(self.scale_z_slider)

        def _scale_z_reset(target):
            self.editor.selected_object.scale.z = 0.00
        self._scale_z_group.add_element(Button(label="Reset", on_click=_scale_z_reset))
        # endregion

        # Lamp menu
        self.lamp_menu = None

    def reset_lamp_menu(self):
        if len(self.editor.selected_object.children) != 0 and self.editor.selected_object.children[0].type == 'LAMP':
            self.lamp_menu = MenuPanel("Lamp", gap=0.125)
            self.lamp_menu.width = 4.0
            self._container.add_element(self.lamp_menu)

            def _lamp_change_type(target):
                lamp = self.editor.selected_object.children[0]
                if lamp.data.type == 'SPOT':
                    lamp.data.type = 'SUN'
                elif lamp.data.type == 'SUN':
                    lamp.data.type = 'SPOT'
                self._container.remove_element(self.lamp_menu)
                self.reset_lamp_menu()

            if self.editor.selected_object.children[0].data.type == 'SPOT':
                self.lamp_change_type_button = Button(label="Change to Sun", on_click=_lamp_change_type)
            elif self.editor.selected_object.children[0].data.type == 'SUN':
                self.lamp_change_type_button = Button(label="Change to Spot", on_click=_lamp_change_type)
            self.lamp_menu.add_item(self.lamp_change_type_button)

            def _lamp_energy(target, value):
                self.editor.selected_object.children[0].data.energy = value

            self.lamp_energy_slider = Slider(label="Energy", min=0.00, max=10.00, on_change=_lamp_energy)
            self.lamp_energy_slider.value = self.editor.selected_object.children[0].data.energy
            self.lamp_menu.add_item(self.lamp_energy_slider)

            if self.editor.selected_object.children[0].data.type == 'SPOT':
                def _lamp_distance(target, value):
                    self.editor.selected_object.children[0].data.distance = value

                self.lamp_distance_slider = Slider(label="Distance", min=5.00, max=1000.00, on_change=_lamp_distance)
                self.lamp_distance_slider.value = self.editor.selected_object.children[0].data.distance
                self.lamp_menu.add_item(self.lamp_distance_slider)

                def _lamp_spot_size(target, value):
                    self.editor.selected_object.children[0].data.spot_size = value / 180.0 * math.pi

                self.lamp_spot_size_slider = Slider(label="Spot size", min=1.00, max=180.00, on_change=_lamp_spot_size)
                self.lamp_spot_size_slider.value = self.editor.selected_object.children[0].data.spot_size
                self.lamp_menu.add_item(self.lamp_spot_size_slider)

                def _lamp_spot_blend(target, value):
                    self.editor.selected_object.children[0].data.spot_blend = value

                self.lamp_spot_blend_slider = Slider(label="Spot blend", min=0.00, max=1.00, on_change=_lamp_spot_blend)
                self.lamp_spot_blend_slider.value = self.editor.selected_object.children[0].data.spot_blend
                self.lamp_menu.add_item(self.lamp_spot_blend_slider)

            def _lamp_color(target, value):
                channel = target.label.text
                color = self.editor.selected_object.children[0].data.color
                if channel == "Hue":
                    color.h = value
                elif channel == "Saturation":
                    color.s = value
                elif channel == "Value":
                    color.v = value
                self.editor.selected_object.children[0].data.color = color

            self.lamp_color_hue_slider = Slider(label="Hue", min=0.00, max=1.00, on_change=_lamp_color)
            self.lamp_color_sat_slider = Slider(label="Saturation", min=0.00, max=1.00, on_change=_lamp_color)
            self.lamp_color_val_slider = Slider(label="Value", min=0.00, max=1.00, on_change=_lamp_color)
            current_lamp_color = self.editor.selected_object.children[0].data.color
            self.lamp_color_hue_slider.value = current_lamp_color.h
            self.lamp_color_sat_slider.value = current_lamp_color.s
            self.lamp_color_val_slider.value = current_lamp_color.v
            self.lamp_menu.add_item(self.lamp_color_hue_slider)
            self.lamp_menu.add_item(self.lamp_color_sat_slider)
            self.lamp_menu.add_item(self.lamp_color_val_slider)

    def on_opening(self):
        super().on_opening()
        self._show_axis_button.selected = self.editor.show_axis
        self._change_axis_local_button.selected = self.editor.show_axis_ref == 'LOCAL'
        self._change_axis_world_button.selected = self.editor.show_axis_ref == 'WORLD'
        self._lock_x_axis_button.selected = not self.editor.lock_x_axis
        self._lock_y_axis_button.selected = not self.editor.lock_y_axis
        self._lock_z_axis_button.selected = not self.editor.lock_z_axis

        self.rotation_x_slider.value = self.editor.selected_object.rotation_euler.x % (math.pi * 2)
        self.rotation_y_slider.value = self.editor.selected_object.rotation_euler.y % (math.pi * 2)
        self.rotation_z_slider.value = self.editor.selected_object.rotation_euler.z % (math.pi * 2)
        self.scale_x_slider.max = self.scale_y_slider.max = self.scale_z_slider.max = max(
            10.0,
            self.editor.selected_object.scale.x,
            self.editor.selected_object.scale.y,
            self.editor.selected_object.scale.z
        )
        self.scale_x_slider.value = self.editor.selected_object.scale.x
        self.scale_y_slider.value = self.editor.selected_object.scale.y
        self.scale_z_slider.value = self.editor.selected_object.scale.z

        # Lamp-specific properties
        self.reset_lamp_menu()

    def on_closed(self):
        # Lamp-specific properties
        if self.lamp_menu is not None and self._container.contains_element(self.lamp_menu):
            self._container.remove_element(self.lamp_menu)

        super().on_closed()


class EditingRadialMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        # Extended Menu
        def _more(target):
            self._manager.open_menu(EditingMoreMenu())
        self.add_item_at(0, "More...", _more)

        # States Menu
        def _transform(target):
            self.editor.transform()
            self.close()
        self.add_item("Transform", _transform)

        def _texture(target):
            self.editor.browse(gallery_mode=GalleryMode.MATERIAL, modification_target=self.editor.selected_object)
            self.close()
        self.add_item("Texture", _texture)

        def _sound(target):
            self.editor.browse(gallery_mode=GalleryMode.SOUNDS, modification_target=self.editor.selected_object)
        self.add_item("Sound", _sound)

        def _sculpt(target):
            self.editor.sculpt()
            self.close()
        self._sculpt_item = self.add_item("Sculpt", _sculpt)

        def _vpaint(target):
            self.editor.paint()
            self.close()
        self._vpaint_item = self.add_item("Vertex Paint", _vpaint)

        def _animate(target):
            self.editor.edit_animate()
            self.close()
        self.add_item("Animate", _animate)

        def _navigate(target):
            self.editor.edit_navigate()
            self.close()
        self.add_item("Navigate", _navigate)

        def _cancel_changes(target):
            to_remove = self.editor.selected_object
            saved_object = self._state.parent.saved_object
            bpy.context.scene.objects.link(saved_object)
            self.editor.selected_object = saved_object
            self._state.parent.saved_object = None
            self.editor.deselect()
            self.close()
            bpy.data.objects.remove(to_remove, True)
        self.add_item("Cancel", _cancel_changes)

        self._companion_item = None

    def on_opening(self):
        super().on_opening()

        self._sculpt_item.enabled = not self.editor.selected_object.live_editing.non_editable
        self._vpaint_item.enabled = not self.editor.selected_object.live_editing.non_editable

        if self.editor.selected_object.parent == self.editor.shelf:
            # Leave here option
            def _leave_here(target):
                m = self.editor.selected_object.matrix_world.copy()
                self.editor.selected_object.parent = None
                self.editor.selected_object.matrix_world = m
                self.close()
            self._companion_item = self.add_item("Leave Here", _leave_here)
        else:
            # Bring along option
            def _bring_along(target):
                m = self.editor.selected_object.matrix_world.copy()
                self.editor.selected_object.parent = self.editor.shelf
                self.editor.selected_object.matrix_world = m
                self.close()
            self._companion_item = self.add_item("Bring along", _bring_along)

    def on_closed(self):
        self.remove_item(self._companion_item)
        super().on_closed()


class EditingState(BaseEditorState):

    def __init__(self, machine, parent=None):
        super().__init__(
            name='editing',
            verb='edit',
            recallable=True,
            machine=machine,
            parent=parent
        )

        # Menu
        self._menu = EditingRadialMenu

        # Sub States
        self._last_state = None
        self._current_state = self.name + '_transforming'

        editing_states = [
            TransformingState(machine=machine, parent=self),
            SculptingState(machine=machine, parent=self),
            PaintingState(machine=machine, parent=self),
            AnimatingState(machine=machine, parent=self, trigger="edit_animate", on_exit=self.recall),
            NavigatingState(machine=machine, parent=self, trigger="edit_navigate", on_exit=self.recall)
        ]
        machine.add_states(editing_states)

        machine_states = OrderedDict()
        for state in editing_states:
            machine_states[state.trigger] = state

        for state_key in machine_states:
            remaining_states = machine_states.copy()
            remaining_states.pop(state_key)
            sources = [self.name]
            for other_state in remaining_states:
                sources.append(self.name + '_' + machine_states[other_state].original_name)
            machine.add_transition(
                trigger=state_key,
                source=sources,
                dest=self.name + '_' + machine_states[state_key].original_name,
                conditions=machine_states[state_key].should_enter
            )

        # TODO: cycling may fail inexplicably if trying to cycle a non_editable object
        keys = [state for state in machine_states]
        count = len(keys)
        for idx in range(count):
            machine.add_transition(
                trigger='edit_next',
                source=self.name + '_' + machine_states[keys[idx]].original_name,
                dest=self.name + '_' + machine_states[keys[(idx+1) % count]].original_name,
                conditions=machine_states[keys[(idx+1) % count]].should_enter
            )

        self._saved_object = None

    @property
    def saved_object(self):
        return self._saved_object

    @saved_object.setter
    def saved_object(self, value):
        self._saved_object = value

    def enter(self, event):
        super().enter(event)

        # Select object
        self.editor.selected_object = event.kwargs.get('object')
        for obj in bpy.context.scene.objects:
            obj.select = False
        self.editor.selected_object.select = True

        # Go back to last state
        # We have to call triggers directly because of how the state self._machine library works.
        # Ideally we would just call a set_state method but it doesn't provide one that
        # also processes callbacks, the current set_state method only changes the state
        # without processing any callbacks
        # NOTE: This is effectively disabled by the reset in `exit()`
        self.editor.trigger_state(self._current_state, default_trigger="transform")

        # Saved state of the object for cancel
        self._saved_object = self.editor.selected_object.copy()
        if self.editor.selected_object.data:
            self._saved_object.data = self.editor.selected_object.data.copy()

    def exit(self, event):
        super().exit(event)
        self.editor.selected_object = None

        # Reset last/current state as this was confusing, we would select an object and not be in transform
        # mode, or worse stuck in a navigate state without recallabe last state.
        self._last_state = None
        self._current_state = self.name + '_transforming'

        if self._saved_object:
            bpy.data.objects.remove(self._saved_object, True)

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

    def recall(self):
        """
        Recall the last state used
        :return: 
        """
        if self._last_state is None:
            return False

        return self.editor.trigger_state(self._last_state, default_trigger="transform")

    def run(self):
        super().run()

        if self.editor.controller2.menu_button.pressed:
            """ Go back (deselect) """
            self.editor.deselect()
            return

        # elif self.editor.controller1.trackpad_button.pressed:
        #     """ Cycle edit modes """
        #     self.editor.edit_next()
        #     return

        # Keep a record of the current state so that we can enter it when re-entering editing mode
        # Also keep the last state to recall the previous state manually, we do it locally
        # instead of the recall() inside the live editor because we want to be able to recall only
        # editing states and never leave the editing state other than explicitly
        current_state = self.machine.state
        if self._current_state != current_state:
            self._last_state = self._current_state
            self._current_state = current_state

        # Only pick selected object
        self.editor.picker.pick_obj(self.editor.selected_object)
