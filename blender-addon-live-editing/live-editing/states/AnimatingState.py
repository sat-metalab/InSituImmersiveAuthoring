import bpy
import math
from mathutils import Matrix
from . import BaseEditorState
from .animating import TimelineSummary
from .animating.components import TransportRoot
from ..components import Cursor
from ..components.menus import EditorRadialMenu
from ..utils.KeyframeUtils import euler_filter


CAMERA_TO_POD_ROTATION_OFFSET = Matrix.Rotation(-math.pi/2, 4, 'X')
POD_TO_CAMERA_ROTATION_OFFSET = Matrix.Rotation(math.pi/2, 4, 'X')


class AnimatingMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        def _exit(target):
            self._state.go_back()
            self.close()
        self.add_item("Exit", _exit)

        def _jump_next(target):
            bpy.context.scene.frame_current += 1
            self.close()
        self.add_item("Next Frame", _jump_next)

        def _jump_next_keyframe(target):
            obj = self.editor.selected_object if self.editor.selected_object is not None else self.editor.camera
            if obj.animation_data is not None:
                closest = None
                distance = math.inf
                for fcurve in obj.animation_data.action.fcurves:
                    for keyframe_point in fcurve.keyframe_points:
                        frame = keyframe_point.co[0]
                        current = bpy.context.scene.frame_current
                        dist = abs(current - frame)
                        if frame > current and dist < distance:
                            closest = frame
                            distance = dist
                if closest:
                    bpy.context.scene.frame_current = closest
            self.close()
        self._next_keyframe = self.add_item("Next Keyframe", _jump_next_keyframe)

        def _jump_end(target):
            bpy.context.scene.frame_current = bpy.context.scene.frame_end
            self.close()
        self.add_item("Goto End", _jump_end)

        def _remove_keyframe(target):
            obj = self.editor.selected_object if self.editor.selected_object is not None else self.editor.camera
            try:
                obj.keyframe_delete(data_path='location')
                obj.keyframe_delete(data_path='rotation_euler')
                obj.keyframe_delete(data_path='scale')
            except:
                pass
        self._remove_keyframe_item = self.add_item("Remove Key", _remove_keyframe)

        def _clear_keyframes(target):
            overrider = bpy.context.copy()
            obj = self.editor.selected_object if self.editor.selected_object is not None else self.editor.camera
            for window in bpy.context.window_manager.windows:
                screen = window.screen
                for area in screen.areas:
                    if area.type == 'VIEW_3D':
                        for region in area.regions:
                            if region.type == 'WINDOW':
                                overrider['window'] = window
                                overrider['screen'] = screen
                                overrider['area'] = area
                                overrider['region'] = region
                                overrider['object'] = obj
                                overrider['active_object'] = obj
                                overrider['selected_objects'] = [obj]
                                break
                        break
            bpy.ops.anim.keyframe_clear_v3d(overrider)
            self.close()
        self._clear_keyframes_item = self.add_item("Clear Keys", _clear_keyframes)

        def _auto_keyframes(target):
            # Use the built-in blender setting
            bpy.context.scene.tool_settings.use_keyframe_insert_auto = target.selected
            self.close()
        self._auto_keyframes_item = self.add_item("Auto Key", _auto_keyframes, toggle=True)

        def _jump_start(target):
            bpy.context.scene.frame_current = bpy.context.scene.frame_start
            self.close()
        self.add_item("Goto Start", _jump_start)

        def _jump_previous_keyframe(target):
            obj = self.editor.selected_object if self.editor.selected_object is not None else self.editor.camera
            if obj.animation_data is not None:
                closest = None
                distance = math.inf
                for fcurve in obj.animation_data.action.fcurves:
                    for keyframe_point in fcurve.keyframe_points:
                        frame = keyframe_point.co[0]
                        current = bpy.context.scene.frame_current
                        dist = abs(current - frame)
                        if frame < current and dist < distance:
                            closest = frame
                            distance = dist
                if closest:
                    bpy.context.scene.frame_current = closest
            self.close()
        self._previous_keyframe = self.add_item("Prev Keyframe", _jump_previous_keyframe)

        def _jump_previous(target):
            bpy.context.scene.frame_current -= 1
            self.close()
        self.add_item("Prev Frame", _jump_previous)

    def on_opening(self):
        super().on_opening()
        # Use the built-in blender setting
        self._auto_keyframes_item.selected = bpy.context.scene.tool_settings.use_keyframe_insert_auto

    def update_menu(self):
        super().update_menu()
        # Use the built-in blender setting
        self._auto_keyframes_item.selected = bpy.context.scene.tool_settings.use_keyframe_insert_auto


class AnimatingState(BaseEditorState):

    def __init__(self, *args, on_exit, **kwargs):
        super().__init__(
            name='animating',
            verb='animate',
            recallable=True,
            **kwargs
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'grab': "Recall last state"
            },
            'secondary': {
                'trigger': "Play/Pause",
                'trackpad': "Move Playhead",
                'grab': "Create Keyframe"
            }
        }

        self._on_exit = on_exit

        self._timeline_summary = TimelineSummary()

        # Control parameters
        self.trackpad_epsilon = 0.01  # trackpad update not used if smaller than this

        # Variables
        self.previous_cont2_trackpad_x = 0.0
        self.previous_cont2_trackpad_y = 0.0

        # UI
        self._transport_root = None
        self._cursor = None

        # Menu
        self._menu = AnimatingMenu

    def enter(self, event):
        super().enter(event)

        # Here, we have to be sure that an object is selected
        if self.editor.selected_object is None:
            self.editor.camera.select = True
            bpy.context.scene.objects.active = self.editor.camera
        else:
            self.editor.selected_object.select = True
            bpy.context.scene.objects.active = self.editor.selected_object

        # Calculate paths
        # bpy.ops.object.paths_calculate(
        #     start_frame=bpy.context.scene.frame_start,
        #     end_frame=bpy.context.scene.frame_end
        # )

        # Cursor
        self._cursor = Cursor(selection_chevron=True)
        self._cursor.minimal = True
        self.editor.cursor_manager.cursor = self._cursor

        # UI
        self._transport_root = TransportRoot(self)
        self._transport_root.y = 10  # TODO: Dynamically find out where to put it
        self._transport_root.rotation_x = math.pi / 2
        self.editor.hud.add_child(self._transport_root)

    def exit(self, event):
        super().exit(event)

        # Clear paths
        #bpy.ops.object.paths_clear()

        # UI
        self.editor.hud.remove_child(self._transport_root)
        self._transport_root.dispose()
        self._transport_root = None

        self._cursor.dispose()
        self._cursor = None

    def go_back(self):
        self._on_exit()

    def always_run_before(self):
        super().always_run_before()

        # UI
        self._transport_root.transport.timeline.start = bpy.context.scene.frame_start
        self._transport_root.transport.timeline.end = bpy.context.scene.frame_end
        self._transport_root.transport.timeline.frame = bpy.context.scene.frame_current
        self._transport_root.transport.timeline.tracks = self._timeline_summary.get(self.editor.selected_object if self.editor.selected_object is not None else self.editor.camera)

        # Pod follows camera if we are animating the camera
        if self.editor.selected_object is None:
            self.editor.pod.matrix_world = self.editor.camera.matrix_world * CAMERA_TO_POD_ROTATION_OFFSET

    def run(self):
        super().run()

        # Control

        cont1 = self.editor.controller1
        cont2 = self.editor.controller2

        # Control animation
        if cont2.trackpad_button.touched:
            # Initialize
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            self.previous_cont2_trackpad_y = cont2.trackpad_y

        elif cont2.trackpad_button.touching:
            # Update
            delta = cont2.trackpad_x - self.previous_cont2_trackpad_x
            coeff = (cont2.trackpad_y + 1.0) * 5.0
            if abs(delta) < self.trackpad_epsilon:
                delta = 0.0
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            bpy.context.scene.frame_current += round(delta * 10 * coeff)

        # Add keyframe
        if cont2.grab_button.pressed:
            if self.editor.selected_object is not None:
                target_object = self.editor.selected_object
            else:
                target_object = self.editor.camera
                target_object.matrix_world = self.editor.pod.matrix_world * POD_TO_CAMERA_ROTATION_OFFSET

            target_object.keyframe_insert(data_path='location')
            target_object.keyframe_insert(data_path='rotation_euler')
            target_object.keyframe_insert(data_path='scale')
            # Filter euler rotation keyframes
            euler_filter(target_object)

        # Play/pause
        if cont2.trigger_button.pressed:
            bpy.ops.screen.animation_play()

        # Update paths
        # bpy.ops.object.paths_update()
