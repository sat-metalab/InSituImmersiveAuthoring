import bpy


# class Keyframe:
#     def __init__(self):
#         self.frame = 0.00
#         self.prop = None
#         self.value = 0.00

class TimelineSummary:

    def __init__(self):
        pass

    def get(self, obj):
        curves = []
        if obj is not None and obj.animation_data is not None and obj.animation_data.action is not None:
            for fcurve in obj.animation_data.action.fcurves:
                keyframes = []
                for keyframe_point in fcurve.keyframe_points:
                    keyframes.append(keyframe_point.co)
                curves.append((fcurve.data_path, keyframes))
        return curves
        # return [
        #     (fcurve.data_path, [keyframe_point.co for keyframe_point in fcurve.keyframe_points])
        #     for fcurve in obj.animation_data.action.fcurves
        # ] if obj is not None and obj.animation_data is not None else []
