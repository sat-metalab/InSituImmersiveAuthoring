import bpy
from .timeline import Track
from ....components import Panel
from ....flex.components import Slider, VGroup, HGroup, Spacer, Label


class Timeline(Panel):

    def __init__(self, editor):
        super().__init__()

        self._editor = editor

        self._start = 0
        self._end = 1
        self._frame = 0
        self._zoom = 0.5

        self._tracks = {}
        self._tracks_changed = False
        self._track_components = {}

        # Main Container
        self._vgroup = VGroup(horizontal_align='JUSTIFY', gap=0.125)
        self._vgroup.left = 0.25
        self._vgroup.right = 0.25
        self._vgroup.top = 0.25
        self._vgroup.bottom = 0.25
        # self._vgroup.height = 1.00
        self._vgroup.percent_width = 1.00
        self.add_element(self._vgroup)

        # Timeline Slider
        def _scrub(target, value):
            bpy.context.scene.frame_current = value
        self._bar = Slider(label="Timeline", on_change=_scrub)
        # self.bar.left = 0.5
        # self.bar.right = 0.5
        # self.bar.top = 0.5
        # self.bar.bottom = 0.5
        self._bar.height = 1.00
        # self.bar.percent_width = 1.00
        self._vgroup.add_element(self._bar)

        # Toolbar
        toolbar = HGroup(vertical_align='MIDDLE')
        self._vgroup.add_element(toolbar)
        toolbar.add_element(Label(text="Keyframes"))
        toolbar.add_element(Spacer(percent_width=1.00))
        self._scale_slider = Slider(label="Zoom", min=0, max=10, value=self._zoom * 10, on_change=self._on_zoom)
        self._scale_slider.width = 1.5
        toolbar.add_element(self._scale_slider)

        # Tracks
        self._tracks_container = VGroup(horizontal_align='JUSTIFY', gap=0.0625)
        self._vgroup.add_element(self._tracks_container)

    # region Properties

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        if self._start != value:
            self._start = value
            self._dirty_attributes = True

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if self._end != value:
            self._end = value
            self._dirty_attributes = True

    @property
    def frame(self):
        return self._frame

    @frame.setter
    def frame(self, value):
        if self._frame != value:
            self._frame = value
            self._dirty_attributes = True

    @property
    def zoom(self):
        return self._zoom

    @zoom.setter
    def zoom(self, value):
        if self._zoom != value:
            self._zoom = value
            self._dirty_attributes = True

    @property
    def tracks(self):
        return self._tracks

    @tracks.setter
    def tracks(self, value):
        if self._tracks != value:
            self._tracks = value
            self._tracks_changed = True
            self._dirty_attributes = True

    # endregion

    def _on_zoom(self, target, value):
        self.zoom = (target.value - target.min) / (target.max - target.min)

    def update_attributes(self):
        super().update_attributes()

        self._bar.min = self._start
        self._bar.max = self._end
        self._bar.value = self._frame

        # Create/Update tracks
        if self._tracks_changed:
            self._tracks_changed = False
            active_track_components = []

            # Create/Update Tracks
            for track_data in self._tracks:
                track_name = track_data[0]
                active_track_components.append(track_name)

                track_component = self._track_components.get(track_name)
                if track_component is None:
                    track_component = Track(track_name)
                    self._track_components[track_name] = track_component
                    self._tracks_container.add_element(track_component)
                track_component.keyframes = track_data[1]

            # Remove tracks that are not needed anymore
            for track_name, track_component in list(self._track_components.items()):
                if track_name not in active_track_components:
                    self._tracks_container.remove_element(track_component)
                    self._track_components[track_name].dispose()
                    del self._track_components[track_name]

        # Update track values
        for track_component in self._track_components.values():
            track_component.zoom = self._zoom
            track_component.start = self._start
            track_component.end = self._end
            track_component.frame = self._frame
