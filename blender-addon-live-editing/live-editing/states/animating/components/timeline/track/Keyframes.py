from . import Keyframe
from ......engine import Instances
from ......engine.geometry import Plane
from ......engine.materials import MeshBasicMaterial


class Keyframes(Instances):
    def __init__(self):
        super().__init__(
            of=Keyframe,
            geometry=Plane(width=0.1, height=0.1),  # Shared geometry
            material=MeshBasicMaterial()            # Shared material
        )
        self._block_interaction = True  # Prevent picking from trying to descend into keyframes
        self._material.flat = True

    def pre_draw(self):
        super().pre_draw()
        Keyframe.last_active = None
