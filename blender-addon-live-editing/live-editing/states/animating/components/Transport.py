import math
from ....flex.components import Root, Group, HGroup
from . import Controls, Timeline, Timing


class Transport(Group):

    def __init__(self, editor):
        super().__init__()

        self._editor = editor

        self._container = HGroup(gap=0.125, vertical_align='JUSTIFY')

        # self.controls = Controls()
        # self.controls.z = 2
        # self.controls.rotation_y = math.pi / 4
        # self._container.add_element(self.controls)

        self.timeline = Timeline(self._editor)
        # self.timeline.left = 0.5
        # self.timeline.right = 0.5
        # self.timeline.top = 0.5
        # self.timeline.bottom = 0.5
        self.timeline.width = 10
        self._container.add_element(self.timeline)

        # self.timing = Timing()
        # self.timing.rotation_y = -math.pi / 4
        # self._container.add_element(self.timing)

    def create_children(self):
        super().create_children()
        self.add_element(self._container)


class TransportRoot(Root):
    def __init__(self, editor):
        super().__init__()
        self.ignore_depth = True

        self._editor = editor

        self.transport = Transport(self._editor)
        self.transport.horizontal_center = 0.00
        self.transport.vertical_center = 0.00
        self.add_element(self.transport)
