import bpy
import math
from mathutils import Vector, Euler, Matrix
from . import BaseEditorState
from ..components import Cursor
from ..components.menus import EditorRadialMenu
from ..engine import Object3D
from ..engine.geometry.shapes import Arrow
from ..engine.materials import LineBasicMaterial
from ..utils.KeyframeUtils import euler_filter


CAMERA_ROTATION_OFFSET = Matrix.Rotation(math.pi/2, 4, 'X')


class NavigatingMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        def _exit(target):
            self._state.go_back()
            self.close()
        self.add_item("Exit", _exit)

        def _origin(target):
            self._move(Matrix.Identity(4))
            self.close()
        self.add_item("Origin", _origin)

    def on_opening(self):
        super().on_opening()

    def _move(self, matrix_world):
        self.editor.pod.matrix_world = matrix_world


class NavigatingCursor(Cursor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        diameter = self.radius * 2
        self.custom_cursor = Object3D(
            geometry=Arrow(
                length=diameter,
                width=diameter,
                head_length_ratio=2/3,
                line_width=2
            ),
            material=LineBasicMaterial()
        )
        self.custom_cursor.rotation_x = -math.pi / 4

    @Cursor.radius.setter
    def radius(self, value):
        Cursor.radius.fset(self, value)
        diameter = value * 2
        self._custom_cursor.geometry.length = diameter
        self._custom_cursor.geometry.width = diameter


class NavigatingState(BaseEditorState):
    def __init__(self, *args, on_exit, **kwargs):
        super().__init__(
            name='navigating',
            verb='navigate',
            recallable=True,
            **kwargs,
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'trigger': "Move Backwards",
                'trackpad': "Orbit Camera|Adjust Roll",
                'grab': "Recall last state"
            },
            'secondary': {
                'trigger': "Move Forward",
                'trackpad': "Change Speed, (Click) Reset Pod|Pan",
                'grab': "Modifier"
            }
        }

        self._on_exit = on_exit
        self._wait_for_backwards_movement = False

        # Parameters
        # Orientation mode. 0 is constrainted is fully constrainted, 1 is Blender-like, 2 is free for all
        self.orientation_mode = 2
        self.speed_rotate = 0.5
        self.speed_translate = 2.0

        # Variables
        self.previous_cont1_trackpad_x = 0.0
        self.previous_cont1_trackpad_y = 0.0
        self.previous_cont2_trackpad_x = 0.0
        self.previous_cont2_trackpad_y = 0.0
        self.speed = 0.01
        self.cont1_orbit_location = Vector((0.0, 0.0, 0.0))
        self.cont1_orbit_offset = None
        self.cont2_orbit_location = Vector((0.0, 0.0, 0.0))
        self.cont2_orbit_offset = None
        self._previous_camera_matrix = Matrix.Identity(4)

        # Cursor
        self._cursor = None

        # Menu
        self._menu = NavigatingMenu

    def enter(self, event):
        super().enter(event)

        # Cursor
        self._cursor = NavigatingCursor(label="Navigation", color=(0.55, 0.76, 0.29), selection_chevron=True)
        self.editor.cursor_manager.cursor = self._cursor

        # Delay the moment we can move back because we probably clicked the trigger to get to this state
        # so we would start going backwards immediately if we didn't wait for a release
        self._wait_for_backwards_movement = self.editor.controller1.trigger_button.down

        # Comparison matrix for keyframing
        self._previous_camera_matrix = self.editor.camera.matrix_world.copy()

    def exit(self, event):
        super().exit(event)
        self._cursor.dispose()
        self._cursor = None

    def go_back(self):
        self._on_exit()

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

        # Delay the moment we can move back because we probably clicked the trigger to get to this state
        # so we would start going backwards immediately if we didn't wait for a release
        self._wait_for_backwards_movement = self.editor.controller1.trigger_button.down

    def run(self):
        super().run()

        # Trigger state change
        if self.editor.controller2.menu_button.pressed:
            self._on_exit()
            return

        # Delay the moment we can move back because we probably clicked the trigger to get to this state
        # so we would start going backwards immediately if we didn't wait for a release
        if self._wait_for_backwards_movement and self.editor.controller1.trigger_analog == 0:
            self._wait_for_backwards_movement = False

        # Control
        cont1 = self.editor.controller1
        cont2 = self.editor.controller2

        # INITIALIZATION

        # Initialize primary controller values
        if cont1.trackpad_button.touched:
            # Do picking once
            self.editor.picker.pick()

            self.previous_cont1_trackpad_x = cont1.trackpad_x
            self.previous_cont1_trackpad_y = cont1.trackpad_y

            has_selection = self.editor.selected_object is not None
            has_pick = self.editor.picker.object is not None
            pointing_selected = has_selection and has_pick and self.editor.selected_object == self.editor.picker.object

            if pointing_selected or (not has_selection and has_pick):
                # We are either pointing at the selected object, or pointing at an object without a selection
                # In that case we want to orbit around the pointed location
                self.cont1_orbit_location = self.editor.picker.object.matrix_world * self.editor.picker.location
            elif has_selection:
                # We are not pointing at the selection
                # In that case rotate around the selected object
                self.cont1_orbit_location = self.editor.selected_object.matrix_world.translation
            else:
                # We are pointing nowhere, rotate around the cursor
                self.cont1_orbit_location = self.editor.picker.cursor_matrix.translation

            self.cont1_orbit_offset = self.editor.pod.matrix_world.to_quaternion().rotation_difference((self.editor.pod.matrix_world.translation - self.cont1_orbit_location).to_track_quat('Y', 'Z')).inverted()

        # Initialize secondary controller values
        if cont2.trackpad_button.touched:
            # Do picking once
            self.editor.picker.pick()

            self.previous_cont2_trackpad_x = cont2.trackpad_x
            self.previous_cont2_trackpad_y = cont2.trackpad_y

            has_selection = self.editor.selected_object is not None
            has_pick = self.editor.picker.object is not None
            pointing_selected = has_selection and has_pick and self.editor.selected_object == self.editor.picker.object

            if pointing_selected or (not has_selection and has_pick):
                # We are either pointing at the selected object, or pointing at an object without a selection
                # In that case we want to orbit around the pointed location
                self.cont2_orbit_location = self.editor.picker.object.matrix_world * self.editor.picker.location
            elif has_selection:
                # We are not pointing at the selection
                # In that case rotate around the selected object
                self.cont2_orbit_location = self.editor.selected_object.matrix_world.translation
            else:
                # We are pointing nowhere, rotate around the cursor
                self.cont2_orbit_location = self.editor.picker.cursor_matrix.translation

        # ORBITING

        # Primary Controller
        if cont1.trackpad_button.touching and not cont2.grab_button.down:
            deltaX = cont1.trackpad_x - self.previous_cont1_trackpad_x
            deltaY = cont1.trackpad_y - self.previous_cont1_trackpad_y
            self.previous_cont1_trackpad_x = cont1.trackpad_x
            self.previous_cont1_trackpad_y = cont1.trackpad_y

            deltaX *= self.speed_rotate
            deltaY *= self.speed_rotate

            if self.orientation_mode == 0:
                """Rotate around the object, but do not rotate the pod"""

                pod = self.editor.pod

                vector_direction = pod.matrix_world.translation - self.cont1_orbit_location
                vector_direction.z = 0.0
                vector_direction.normalize()
                vector_Y = Vector((0.0, 1.0, 0.0))
                rotated_basis = vector_Y.rotation_difference(vector_direction).to_matrix().to_4x4()

                orbit_position = pod.matrix_world.translation - self.cont1_orbit_location
                rotation = Euler((-deltaY, 0.0, -deltaX)).to_matrix().to_4x4()
                orbit_position = rotated_basis * rotation * rotated_basis.inverted() * orbit_position

                pod_matrix = pod.matrix_world
                pod_matrix_translation = orbit_position + self.cont1_orbit_location
                pod_matrix_rotation = pod_matrix.to_quaternion()
                pod_matrix = Matrix.Translation(pod_matrix_translation) * pod_matrix_rotation.to_matrix().to_4x4()
                pod.matrix_world = pod_matrix

            elif self.orientation_mode == 1:
                """Rotation around the object and rotate the pod, try to keep the pod up vector vertical"""

                pod = self.editor.pod
                pod_basis = pod.matrix_world.to_quaternion().to_matrix().to_4x4()

                axis_vector = Vector((1.0, 0.0, 0.0))
                axis_vector_rotated = pod_basis * axis_vector
                axis_vector_rotated.z = 0.0
                axis_vector_rotated.normalize()
                horizontal_rotation = axis_vector.rotation_difference(axis_vector_rotated).to_matrix().to_4x4()

                orbit_position = pod.matrix_world.translation - self.cont1_orbit_location
                rotation_Z = Euler((0.0, 0.0, -deltaX)).to_matrix().to_4x4()
                rotation_Y = Euler((deltaY, 0.0, 0.0)).to_matrix().to_4x4()
                orbit_position = horizontal_rotation * rotation_Y * horizontal_rotation.inverted() * rotation_Z * orbit_position

                pod_matrix = pod.matrix_world
                pod_matrix_translation = orbit_position + self.cont1_orbit_location

                pod_matrix_rotation = orbit_position.to_track_quat('Y', 'Z') * self.cont1_orbit_offset

                pod_matrix = Matrix.Translation(pod_matrix_translation) * pod_matrix_rotation.to_matrix().to_4x4()
                pod.matrix_world = pod_matrix

            elif self.orientation_mode == 2:
                """Free flight mode"""

                pod = self.editor.pod
                pod_basis = pod.matrix_world.to_quaternion().to_matrix().to_4x4()

                orbit_position = pod.matrix_world.translation - self.cont1_orbit_location
                rotation = Euler((deltaY, 0.0, -deltaX)).to_matrix().to_4x4()
                orbit_position = pod_basis * rotation * pod_basis.inverted() * orbit_position

                pod_matrix = pod.matrix_world
                pod_matrix_translation = orbit_position + self.cont1_orbit_location

                pod_matrix_rotation = pod_matrix.to_quaternion().to_matrix().to_4x4()
                pod_matrix_rotation = pod_basis * rotation * pod_basis.inverted() * pod_matrix_rotation

                pod_matrix = Matrix.Translation(pod_matrix_translation) * pod_matrix_rotation
                pod.matrix_world = pod_matrix

        elif cont1.trackpad_button.touching and cont2.grab_button.down:
            """Rotate the pod around the horizontal axis defined by the picker's direction"""

            deltaX = cont1.trackpad_x - self.previous_cont1_trackpad_x
            deltaY = cont1.trackpad_y - self.previous_cont1_trackpad_y
            self.previous_cont1_trackpad_x = cont1.trackpad_x
            self.previous_cont1_trackpad_y = cont1.trackpad_y

            deltaX *= self.speed_rotate
            deltaY *= self.speed_rotate

            pod = self.editor.pod
            vector_direction = pod.matrix_world.translation - self.cont2_orbit_location
            vector_direction.z = 0.0
            vector_direction.normalize()
            vector_Y = Vector((0.0, 1.0, 0.0))
            rotated_basis = vector_Y.rotation_difference(vector_direction).to_matrix().to_4x4()
            translated_basis = Matrix.Translation(self.cont2_orbit_location)

            rotation = Euler((0.0, deltaX, 0.0)).to_matrix().to_4x4()
            pod_matrix = pod.matrix_world
            pod_matrix = translated_basis * rotated_basis * rotation * rotated_basis.inverted() * translated_basis.inverted() * pod_matrix
            pod.matrix_world = pod_matrix

        # Secondary Controller
        if cont2.trackpad_button.touching and cont2.grab_button.down:
            """Pan the pod in the camera projection plane"""

            deltaX = cont2.trackpad_x - self.previous_cont2_trackpad_x
            deltaY = cont2.trackpad_y - self.previous_cont2_trackpad_y
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            self.previous_cont2_trackpad_y = cont2.trackpad_y

            pod = self.editor.pod
            distance = (pod.matrix_world.translation - self.cont1_orbit_location).length

            speed = max(1.0, self.speed_translate * distance / 10.0)
            translation = Vector((-deltaX, 0.0, -deltaY)) * speed
            translation = pod.matrix_world.to_quaternion().to_matrix().to_4x4() * translation
            pod.matrix_world = Matrix.Translation(translation) * pod.matrix_world

        elif cont2.trackpad_button.touching:
            # Update speed
            # TODO: Implement delta time for constant speed regardless of framerate
            #       We'll then be able to show speed as m/s or km/h
            delta = (cont2.trackpad_y - self.previous_cont2_trackpad_y) * 0.01
            self.previous_cont2_trackpad_y = cont2.trackpad_y
            self.speed = max(0.001, min(0.1, self.speed+delta))

        if cont2.trackpad_button.pressed:
            """ Reset the pod as horizontal """

            pod_group = self.editor.pod
            translated_basis = Matrix.Translation(self.cont1_orbit_location)
            reset_rotation = pod_group.matrix_world.to_quaternion().inverted().to_euler('XYZ')
            reset_rotation.z = 0.0
            reset_rotation = reset_rotation.to_matrix().to_4x4()
            pod_matrix = pod_group.matrix_world
            pod_matrix = translated_basis * reset_rotation * translated_basis.inverted() * pod_matrix
            pod_group.matrix_world = pod_matrix

        # TRANSLATING

        delta_forward = self.editor.picker.ray_vector * self.speed * cont2.trigger_analog
        delta_reverse = self.editor.picker.ray_vector * self.speed * -cont1.trigger_analog if not self._wait_for_backwards_movement else Vector()
        delta = delta_forward + delta_reverse
        self.editor.pod.location += delta

        # KEYFRAMING
        # TODO: A matrix should be constructed and then applied, so that we don't have to resort on saving the last matrix
        self.editor.camera.matrix_world = self.editor.pod.matrix_world * CAMERA_ROTATION_OFFSET
        if bpy.context.scene.tool_settings.use_keyframe_insert_auto:
            if self.editor.camera.matrix_world != self._previous_camera_matrix:
                # Only record what has changed
                if self.editor.camera.matrix_world.to_translation() != self._previous_camera_matrix.to_translation():
                    self.editor.camera.keyframe_insert(data_path='location')
                if self.editor.camera.matrix_world.to_quaternion() != self._previous_camera_matrix.to_quaternion():
                    self.editor.camera.keyframe_insert(data_path='rotation_euler')
                    # Filter euler rotation keyframes
                    euler_filter(self.editor.camera)

                self._previous_camera_matrix = self.editor.camera.matrix_world.copy()

        # UI - Cursor Update
        self._cursor.detail = "Speed: " + str(round((self.speed / 0.1) * 100, 2)) + "%"
