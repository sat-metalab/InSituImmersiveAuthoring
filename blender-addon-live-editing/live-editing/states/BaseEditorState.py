from ..engine import Object3D
from . import BaseState


class BaseEditorState(BaseState):

    def __init__(self, *args, recallable=False, **kwargs):
        super().__init__(*args, **kwargs)

        self._recallable = recallable

        """
        Default menu for the state
        Shows up when the user presses the menu button on the primary controller
        """
        self._menu = None

        """
        Default Cursor for the state
        """
        self._cursor = None

        """
        Help data for the state
        
            self._help_data = {
                'primary': {
                    'menu': "Open/Close Menu",
                    'trigger': "Select object",
                    'grab': "Recall last state"
                },
                'secondary': {...}
            }
            
        """
        self._help_data = {}

    def enter(self, event):
        super().enter(event)

    def exit(self, event):
        super().exit(event)

        self.on_menu_closed()  # XXX: Fake it...
        self.editor.menu_manager.close_menu()
        self.editor.cursor_manager.cursor = None

    def recall(self):
        """
        Recall the last state used
        :return: 
        """
        return self.editor.recall()

    def always_run_before(self):
        super().always_run_before()

        # Every state can handle its own cursor so call for all levels
        # self._cursor.location = self.editor.picker.cursor_matrix.to_translation()
        # self._cursor.rotation = self.editor.picker.cursor_matrix.to_quaternion()

    def run_nested(self, modal):
        # Only top-level (no parent) states can trigger a top-level recall
        if self.parent is None and self.editor.controller1.grab_button.pressed:
            """ Recall last state"""
            if self.recall():
                return

        super().run_nested(modal)

    @property
    def editor(self):
        """
        Use editor instead of machine to prevent confusion in editor states
        :return: 
        """
        return self._machine

    @property
    def recallable(self):
        return self._recallable

    @property
    def menu(self):
        if self._menu is not None:
            return self._menu
        elif self._parent is not None:
            return self._parent.menu
        else:
            return None

    @property
    def help_data(self):
        return self._help_data

    def on_menu_opened(self):
        if self._parent is not None:
            self._parent.on_menu_opened()

    def on_menu_closed(self):
        if self._parent is not None:
            self._parent.on_menu_closed()
