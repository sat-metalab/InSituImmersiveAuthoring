import bpy
from itertools import chain
import math

from . import BaseEditorState
from ..components import Cursor, MenuPanel
from ..components.menus import EditorRadialMenu, EditorContextualMenu
from ..flex.components import Button
from ..gallery.Importer import GalleryMode


class ImportingConfigMenu(EditorContextualMenu):
    def __init__(self):
        super().__init__()

        self.models_menu = MenuPanel("3D models")
        self._container.add_element(self.models_menu)

        def _join_models(target):
            self.editor.preferences.gallery.join_models = target.selected
            bpy.ops.wm.save_userpref()

        self._join_models_button = Button(label="Join models", on_click=_join_models, toggle=True)
        self.models_menu.add_item(self._join_models_button)

    def on_opening(self):
        super().on_opening()
        self._join_models_button.selected = self.editor.preferences.gallery.join_models

    def on_closed(self):
        super().on_closed()


class ConfigRadialMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        # States Menu
        def _calibrate(target):
            self.editor.calibrate()
            self.close()
        self.add_item("Calibrate", _calibrate)

        def _import(target):
            self._manager.open_menu(ImportingConfigMenu())
        self.add_item("Import config.", _import)


class SwitchSceneMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        def _create_new_scene(target):
            scene = bpy.context.scene
            saved_pod_matrix = self.editor.pod.matrix_world.copy()
            scene.live_editing.saved_pod_matrix = [saved_pod_matrix[j][i] for i in range(4) for j in range(4)]
            bpy.context.screen.scene = bpy.data.scenes.new('EIS_{}'.format(len(bpy.data.scenes)))
            bpy.ops.live_editing.setup_scene()
            self.close()

        def _choose_scene(target):
            scene = bpy.context.scene
            saved_pod_matrix = self.editor.pod.matrix_world.copy()
            scene.live_editing.saved_pod_matrix = [saved_pod_matrix[j][i] for i in range(4) for j in range(4)]
            bpy.context.screen.scene = bpy.data.scenes[target.data]
            bpy.ops.live_editing.setup_scene()
            self.close()

        self.add_item('Add new scene', _create_new_scene)
        for scene in bpy.data.scenes:
            self.add_item(scene.name, _choose_scene, data=scene.name)


class PickingRadialMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        self._select_candidates = []

        # States Menu
        def _navigate(target):
            self.editor.navigate()
            self.close()
        self.add_item("Navigate", _navigate)

        def _animate(target):
            self.editor.animate()
            self.close()
        self.add_item("Animate", _animate)

        def _present(target):
            self.editor.present()
            self.close()
        self.add_item("Presentation", _present)

        def _cycles_render(target):
            self.editor.cycles_render()
            self.close()
        self.add_item("Rendering", _cycles_render)

        def _select(target):
            self._manager.open_menu(SelectMenu(self._select_candidates))
        self._select_button = self.add_item("Select", _select)

        def _create(target):
            self._manager.open_menu(CreateMenu())
        self.add_item("Create", _create)

        def _config(target):
            self._manager.open_menu(ConfigRadialMenu())
        self.add_item("Configure", _config)

        def _switch_scene(target):
            self._manager.open_menu(SwitchSceneMenu())
        self.add_item("Switch scene", _switch_scene)

    def on_opening(self):
        super().on_opening()
        self._select_candidates = self.editor.picker.pick_all()
        self._select_button.enabled = len(self._select_candidates) > 0

    def on_closed(self):
        super().on_closed()


class SelectMenu(EditorRadialMenu):
    def __init__(self, candidates):
        super().__init__()
        self._candidates = candidates
        self._candidates_changed = True

    def _select(self, target):
        self.editor.select(object=target.data)

    # @property
    # def candidates(self):
    #     return self._candidates
    #
    # @candidates.setter
    # def candidates(self, value):
    #     if self._candidates != value:
    #         self._candidates = value
    #         self._candidates_changed = True
    #         self._dirty_attributes = True

    def update_attributes(self):
        super().update_attributes()

        if self._candidates_changed:
            self._candidates_changed = False
            self.remove_all_items()
            for candidate in self._candidates:
                self.add_item(candidate.name, self._select, data=candidate)


class CreateMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        primitives = [
            ("Plane", bpy.ops.mesh.primitive_plane_add, False),
            ("Cube", bpy.ops.mesh.primitive_cube_add, True),
            ("UV Sphere", bpy.ops.mesh.primitive_uv_sphere_add, True),
            ("Ico Sphere", bpy.ops.mesh.primitive_ico_sphere_add, True),
            ("Cylinder", bpy.ops.mesh.primitive_cylinder_add, True),
            ("Cone", bpy.ops.mesh.primitive_cone_add, True),
            ("Torus", bpy.ops.mesh.primitive_torus_add, False),
            ("Monkey", bpy.ops.mesh.primitive_monkey_add, True),
            ("Lamp", bpy.ops.live_editing.lamp_add, True)
        ]

        def _create(target):
            operator = target.data[0]
            if callable(operator):
                rotate = target.data[1]
                rotation = self._manager.cursor_matrix_before_menu.to_euler()
                if rotate:
                    rotation.rotate_axis('X', -math.pi / 2)
                operator(
                    location=self._manager.cursor_matrix_before_menu.translation,
                    rotation=rotation
                )
                self.editor.select(object=bpy.context.object)
        for primitive in primitives:
            self.add_item(primitive[0], _create, data=(primitive[1], primitive[2]))

        def _import(target):
            self.editor.browse(gallery_mode=GalleryMode.COPY_OBJECTS,
                               modification_target=self.editor.selected_object,
                               cursor_matrix=self._manager.cursor_matrix_before_menu)
        self.add_item('Asset', _import)

        def _slideshow(target):
            self.editor.browse(gallery_mode=GalleryMode.IMAGES,
                               modification_target=self.editor.selected_object,
                               cursor_matrix=self._manager.cursor_matrix_before_menu)
        self.add_item('Slideshow', _slideshow)


class PickingState(BaseEditorState):

    def __init__(self, machine, parent=None):
        super().__init__(
            name='picking',
            verb='pick',
            recallable=False,
            machine=machine,
            parent=parent
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'trigger': "Select object",
                'grab': "Recall last state"
            }
        }

        self.has_focused_object = False
        self._cursor = None

        # Menu
        self._menu = PickingRadialMenu

    def enter(self, event):
        super().enter(event)
        # Cursor
        self._cursor = Cursor(label="Select an object", color=(0.15, 0.65, 0.95))
        self._cursor.highlight_on_hover = True
        self.editor.cursor_manager.cursor = self._cursor

    def exit(self, event):
        super().exit(event)
        self._cursor.dispose()
        self._cursor = None

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

    def run(self):
        super().run()

        # Update picking
        self.editor.picker.pick()

        # Select picked object
        if self.editor.picker.object is not None:
            if not self.editor.picker.object.select:
                for obj in bpy.context.scene.objects:
                    obj.select = False
                self.editor.picker.object.select = True
            self.has_focused_object = True
        else:
            if self.has_focused_object:
                for obj in bpy.context.scene.objects:
                    obj.select = False
            self.has_focused_object = False

        # Trigger state change
        if self.editor.controller1.trigger_button.pressed:
            self.editor.select(object=self.editor.picker.object)
            return
