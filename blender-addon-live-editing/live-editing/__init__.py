__copyright__ = """
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either version 3 of the license, or (at your option) any later
version (http://www.gnu.org/licenses/).
"""
__license__ = "GPLv3"


bl_info = {
    "name": "Live Editing",
    "author": "Francois Ubald Brien, Emmanuel Durand",
    "version": (0, 0, 1),
    "blender": (2, 78, 0),
    "description": "Live Editing with VRPN data from Vive controllers",
    "category": "In Situ"
}

# Handle traceback dumps
import faulthandler
faulthandler.enable()

# Performance debugging
from bpy.props import PointerProperty
from bpy.app.handlers import persistent
import cProfile, pstats, io  # For the profiler (when needed), do not remove

# General plugin imports
# We use * because of the auto register that needs to have everything imported
from .Settings import *
from .UI import *
from .operators import *
from . import NodesManager, LiveEditor


class Runner:

    master_window = None

    @staticmethod
    @persistent
    def run(scene):
        # pr = cProfile.Profile()
        # pr.enable()

        # We check which window triggered the scene update and only respond to updates
        # coming from the master window. The master window is decided on a first come
        # first served basis and is reset when the master window disappears from the
        # window managers's window list. This fixes the problem of having the callback
        # run for every opened window in blender. We do this as blender doesn't offer
        # global callbacks for its update loop.
        if Runner.master_window is None:
            # First run, assign first window found
            Runner.master_window = bpy.context.window

        elif Runner.master_window not in bpy.context.window_manager.windows.values():
            # Master window vanished, take the next one we see
            Runner.master_window = bpy.context.window

        elif Runner.master_window != bpy.context.window:
            # Not the master window, get out
            return

        NodesManager.walk_trees(scene)
        LiveEditor.instance.run(scene)

        # pr.disable()
        # s = io.StringIO()
        # ps = pstats.Stats(pr, stream=s).sort_stats("cumtime")
        # ps.print_stats()
        # print(s.getvalue())


def register():
    bpy.utils.register_module(__name__)

    bpy.types.Object.live_editing = PointerProperty(type=LiveEditingObjectSettings)
    bpy.types.Scene.live_editing = PointerProperty(type=LiveEditingSceneSettings)
    bpy.types.Scene.state_settings = PointerProperty(type=StateSettings)

    NodesManager.register()
    LiveEditor.register()

    bpy.app.handlers.scene_update_pre.append(Runner.run)


def unregister():
    bpy.app.handlers.scene_update_pre.remove(Runner.run)

    NodesManager.unregister()
    LiveEditor.unregister()

    del bpy.types.Object.live_editing
    del bpy.types.Scene.live_editing
    del bpy.types.Scene.state_settings

    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
