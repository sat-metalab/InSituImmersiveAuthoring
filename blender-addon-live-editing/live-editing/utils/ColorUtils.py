class ColorUtils:

    @staticmethod
    def fromHex(hexa):
        return ((hexa >> 16) & 0xff) / 255, ((hexa >> 8) & 0xff) / 255, (hexa & 0xff) / 255


class Colors:
    light = ColorUtils.fromHex(0xabb2bf)
    medium = ColorUtils.fromHex(0x828997)
    dark = ColorUtils.fromHex(0x5c6370)

    cyan = ColorUtils.fromHex(0x56b6c2)
    blue = ColorUtils.fromHex(0x61afef)
    purple = ColorUtils.fromHex(0xc678dd)
    green = ColorUtils.fromHex(0x98c379)
    salmon = ColorUtils.fromHex(0xe06c75)
    red = ColorUtils.fromHex(0xbe5046)
    orange = ColorUtils.fromHex(0xd19a66)
    yellow = ColorUtils.fromHex(0xe5c07b)

    foreground = ColorUtils.fromHex(0xabb2bf)
    background = ColorUtils.fromHex(0x282c34)
    gutter = ColorUtils.fromHex(0x636d83)
    blue_accent = ColorUtils.fromHex(0x528bff)

    white = ColorUtils.fromHex(0xffffff)
    black = ColorUtils.fromHex(0x000000)
