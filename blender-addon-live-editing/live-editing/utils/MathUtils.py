class MathUtils:
    @staticmethod
    def clamp(value, min_or_max_a, min_or_max_b):
        """
        clamp a Float to an interval
        interval endpoints are compared to get min and max, so it doesn't matter what order they are passed in
        @param	value value to clamp
        @param	minOrMax1 interval endpoint
        @param	minOrMax2 interval endpoint
        @return 	clamped value to given interval
        """
        min_value = min(min_or_max_a, min_or_max_b)
        max_value = max(min_or_max_a, min_or_max_b)
        return min_value if value < min_value else (max_value if value > max_value else value)
