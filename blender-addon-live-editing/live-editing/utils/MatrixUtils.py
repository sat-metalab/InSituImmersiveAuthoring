class MatrixUtils:
    @staticmethod
    def flatten(mat):
        # dim = len(mat)
        # return [mat[j][i] for i in range(dim) for j in range(dim)]
        return [
            mat[0][0], mat[1][0], mat[2][0], mat[3][0],
            mat[0][1], mat[1][1], mat[2][1], mat[3][1],
            mat[0][2], mat[1][2], mat[2][2], mat[3][2],
            mat[0][3], mat[1][3], mat[2][3], mat[3][3],
        ]

    @staticmethod
    def flatten_in(mat, gl_mat):
        gl_mat[0] = mat[0][0]
        gl_mat[1] = mat[1][0]
        gl_mat[2] = mat[2][0]
        gl_mat[3] = mat[3][0]
        gl_mat[4] = mat[0][1]
        gl_mat[5] = mat[1][1]
        gl_mat[6] = mat[2][1]
        gl_mat[7] = mat[3][1]
        gl_mat[8] = mat[0][2]
        gl_mat[9] = mat[1][2]
        gl_mat[10] = mat[2][2]
        gl_mat[11] = mat[3][2]
        gl_mat[12] = mat[0][3]
        gl_mat[13] = mat[1][3]
        gl_mat[14] = mat[2][3]
        gl_mat[15] = mat[3][3]
