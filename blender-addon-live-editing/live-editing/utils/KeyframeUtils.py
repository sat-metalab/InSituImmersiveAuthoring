from math import pi


def euler_filter(obj):
    """
    This is a python rewrite of the "euler_filter" Blender's operator code in C (graph_edit.c).
    Its purpose it to filter euler rotation keyframes to detect rotations greater thant +/- 180 degrees.
    
    :param obj: Object to filter keyframes for 
    :return: 
    """

    if obj.animation_data is None or obj.animation_data.action is None or obj.animation_data.action.fcurves is None:
        return

    curves = [curve for curve in obj.animation_data.action.fcurves if curve.data_path == "rotation_euler"]
    if len(curves) != 3:
        raise Exception("Euler filter requires 3 euler rotation components.")  # Why?

    for curve in curves:
        prev = None
        for keyframe in curve.keyframe_points:
            if prev is None:
                prev = keyframe
                continue

            sign = 1.0 if prev.co[1] > keyframe.co[1] else -1.0
            if (sign * (prev.co[1] - keyframe.co[1])) >= pi:
                fac = sign * 2.0 * pi
                while (sign * (prev.co[1] - keyframe.co[1])) >= pi:
                    keyframe.handle_left[1] += fac
                    keyframe.co[1] += fac
                    keyframe.handle_right[1] += fac
