import bpy
import math
from mathutils import Vector


class BrushUtils:

    @staticmethod
    def brush_radius(brush_size, z):
        r3d = None
        w = 0
        for window in bpy.data.window_managers[0].windows:
            for area in window.screen.areas:
                if area.type == 'VIEW_3D':
                    if area.width > w:
                        w = area.width
                        for space in area.spaces:
                            if space.type == 'VIEW_3D' and space.region_3d.view_perspective == 'CAMERA':
                                r3d = space.region_3d
                            if r3d is not None:
                                break
            # Priority is given to the immersive viewport
            if window.screen.immersive_viewport.enabled and r3d is not None:
                break

        if r3d:
            mx = r3d.window_matrix
            near = (2.0 * mx[2][3]) / (2.0 * mx[2][2] - 2.0)
            left = (mx.inverted() * Vector((-1, 0, -1, 1)))
            left = left.x / left.w
            right = (mx.inverted() * Vector((1, 0, -1, 1)))
            right = right.x / right.w
            lfov = abs(math.atan(left / near))
            rfov = abs(math.atan(right / near))
            d = math.tan((lfov + rfov) / 2) * near
            p = d * ((brush_size * 2) / w)
            return (z / near) * p
        else:
            return 0.00
