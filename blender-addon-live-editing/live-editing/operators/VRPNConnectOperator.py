import bpy
from bpy.types import Operator
from .. import LiveEditor


class VRPNConnectOperator(Operator):
    """Connect VRPN"""
    bl_idname = "vrpn.connect"
    bl_label = "Connect"

    def execute(self, context):
        LiveEditor.instance.connect()
        return {'FINISHED'}
