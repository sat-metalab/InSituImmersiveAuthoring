import bpy
import os
from math import acos, pi, sqrt
from bpy.types import Operator
from mathutils import Color, Matrix, Euler
from ..LiveEditor import LiveEditor


class SetupLiveEditingSceneOperator(Operator):
    """Setup Live Editing Scene"""
    bl_idname = "live_editing.setup_scene"
    bl_label = "Setup Scene"

    def lock_helper(self, helper, hide=True):
        helper.live_editing.lock = True
        helper.hide = hide
        helper.hide_select = True
        helper.hide_render = True
        helper.lock_location = True, True, True
        helper.lock_rotation = True, True, True
        helper.lock_scale = True, True, True

    def set_camera_view(self):
        for window in bpy.context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D' and area.spaces[0].camera == bpy.context.scene.camera:
                    if area.spaces[0].region_3d.view_perspective != 'CAMERA':
                        area.spaces[0].region_3d.view_perspective = 'CAMERA'

                    # Fit the camera view to the 3d view
                    v3d = area.regions[4]
                    r3d = area.spaces[0].region_3d

                    r3d.view_camera_offset[0] = 0.0
                    r3d.view_camera_offset[1] = 0.0
                    a1 = v3d.width / v3d.height
                    a2 = bpy.context.scene.render.resolution_x / bpy.context.scene.render.resolution_y
                    if a2 < a1:
                        r3d.view_camera_zoom = 50 * (2 * sqrt(a2 / a1) - sqrt(2))
                    else:
                        r3d.view_camera_zoom = 29.29

    def execute(self, context):

        current_file_path = os.path.dirname(os.path.abspath(__file__))
        preferences = bpy.context.user_preferences.addons[__package__.split('.')[0]].preferences

        # region World

        # New scenes don't get World automatically
        world = bpy.context.scene.world
        if world is None:
            if len(bpy.data.worlds) == 0:
                bpy.ops.world.new()
            bpy.context.scene.world = bpy.data.worlds[0]
            # world = bpy.context.scene.world

        # endregion

        # region Pod
        name = preferences.projection.pod
        pod = bpy.data.objects.get(name)
        if pod is None:
            pod = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(pod)
        self.lock_helper(pod)
        pod.matrix_world = Matrix.Identity(4)
        # endregion

        # region Shelf
        name = LiveEditor.shelf_group
        shelf = bpy.data.objects.get(name)
        if shelf is None:
            shelf = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(shelf)
        self.lock_helper(shelf, False)
        shelf.matrix_world = Matrix.Identity(4)
        shelf.parent = pod
        #endregion

        # region Controllers

        # Link the vive controller model
        with bpy.data.libraries.load(
            filepath=current_file_path + "/../assets/controllers/Vive Controller.blend",
            link=False
        ) as (data_from, data_to):
            data_to.objects = [name for name in data_from.objects if name.startswith("Vive")]
        with bpy.data.libraries.load(
            filepath=current_file_path + "/../assets/controllers/Vive Controller.blend",
            link=True
        ) as (data_from, data_to):
            data_to.meshes = [name for name in data_from.meshes if name.startswith("Vive")]
            data_to.materials = [name for name in data_from.materials if name.startswith("Vive")]
            data_to.textures = [name for name in data_from.textures if name.startswith("Vive")]

        name = "[Controllers]"
        controllers = bpy.data.objects.get(name)
        if controllers is None:
            controllers = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(controllers)
        self.lock_helper(controllers)
        controllers.matrix_world = Matrix.Identity(4)
        controllers.parent = pod

        # 3D Controllers
        controller3d1 = bpy.data.objects.get("Vive Controller 1")
        self.lock_helper(controller3d1)
        controller3d1.parent = controllers
        controller3d1_children = [ob for ob in bpy.data.objects if ob.parent == controller3d1]
        if bpy.context.scene.objects.get(controller3d1.name) is None:
            bpy.context.scene.objects.link(controller3d1)
        for child in controller3d1_children:
            self.lock_helper(child)
            if bpy.context.scene.objects.get(child.name) is None:
                bpy.context.scene.objects.link(child)

        controller3d2 = bpy.data.objects.get("Vive Controller 2")
        self.lock_helper(controller3d2)
        controller3d2.parent = controllers
        controller3d2_children = [ob for ob in bpy.data.objects if ob.parent == controller3d2]
        if bpy.context.scene.objects.get(controller3d2.name) is None:
            bpy.context.scene.objects.link(controller3d2)
        for child in controller3d2_children:
            self.lock_helper(child)
            if bpy.context.scene.objects.get(child.name) is None:
                bpy.context.scene.objects.link(child)

        # region Controller 1
        name = preferences.vrpn.controller_1
        controller1 = bpy.data.objects.get(name)
        if controller1 is None:
            controller1 = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(controller1)
        self.lock_helper(controller1)
        controller1.matrix_world = Matrix.Identity(4)
        controller1.parent = controllers
        controller1.show_name = True
        # endregion

        # region Controller 2
        name = preferences.vrpn.controller_2
        controller2 = bpy.data.objects.get(name)
        if controller2 is None:
            controller2 = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(controller2)
        self.lock_helper(controller2)
        controller2.matrix_world = Matrix.Identity(4)
        controller2.parent = controllers
        controller2.show_name = True
        # endregion

        # endregion

        # region Cameras
        name = "[Cameras]"
        cameras = bpy.data.objects.get(name)
        if cameras is None:
            cameras = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(cameras)
        self.lock_helper(cameras)
        cameras.matrix_world = Matrix.Identity(4)
        cameras.parent = pod

        # region Main Camera
        # The main camera can be used for rendering animations but is not used directly in situ.
        # It only serves as an animatable object that the pod follows. The camera visualized
        # in situ is the pod camera.
        name = preferences.projection.camera
        camera_data = bpy.data.cameras.get(name)
        if camera_data is None:
            camera_data = bpy.data.cameras.new(name)
        camera = bpy.data.objects.get(name)
        if camera is None:
            camera = bpy.data.objects.new(name, camera_data)
        else:
            camera.data = camera_data
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(camera)
        self.lock_helper(camera)
        camera.matrix_world = Matrix.Identity(4)
        camera.rotation_euler.x = pi / 2
        # endregion

        # region Pod Camera
        # Actual POV of the insitu editing session. Can move freely (with the pod) in the space
        # or follow the animated main camera
        name = preferences.projection.pod_camera
        camera_data = bpy.data.cameras.get(name)
        if camera_data is None:
            camera_data = bpy.data.cameras.new(name)
        pod_camera = bpy.data.objects.get(name)
        if pod_camera is None:
            pod_camera = bpy.data.objects.new(name, camera_data)
        else:
            pod_camera.data = camera_data

        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(pod_camera)
        self.lock_helper(pod_camera)
        pod_camera.matrix_world = preferences.projection.pod_camera_rotation.to_matrix().to_4x4() * Matrix.Identity(4)
        pod_camera.rotation_euler.x = pi/2
        pod_camera.parent = cameras

        # Set the pod camera as the satie reference if satie is present
        satie_addon = bpy.context.user_preferences.addons.get('satie')
        if satie_addon:
            satie_addon.preferences.listener = name

        # Setup in immersive view addon (if available)
        if bpy.context.user_preferences.addons.get('immersive-viewport') is not None:
            bpy.context.user_preferences.addons['immersive-viewport'].preferences.anchorObject = name

        # endregion

        # region Cursor Camera
        name = preferences.projection.cursor_camera
        camera_data = bpy.data.cameras.get(name)
        if camera_data is None:
            camera_data = bpy.data.cameras.new(name)
        camera_data.lens = 10.0
        cursor_camera = bpy.data.objects.get(name)
        if cursor_camera is None:
            cursor_camera = bpy.data.objects.new(name, camera_data)
        else:
            cursor_camera.data = camera_data
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(cursor_camera)
        self.lock_helper(cursor_camera)
        cursor_camera.matrix_world = Matrix.Identity(4)
        # cursor_camera.rotation_euler.x = pi/2
        cursor_camera.parent = cameras
        # endregion

        # endregion

        # region Mesh

        name = "[Mesh]"
        mesh = bpy.data.objects.get(name)
        if mesh is None:
            mesh = bpy.data.objects.new(name, None)
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(mesh)
        self.lock_helper(mesh)
        mesh.matrix_world = Matrix.Identity(4)
        mesh.parent = pod

        # region Sample Mesh
        name = preferences.projection.mesh
        dome_mesh = bpy.data.objects.get(name)
        if dome_mesh is None:
            bpy.ops.mesh.primitive_ico_sphere_add(location=(0.00, 0.00, 0.00), size=10, subdivisions=4, enter_editmode=True)
            bpy.ops.mesh.flip_normals()
            bpy.ops.object.editmode_toggle()
            dome_mesh = bpy.context.object
            dome_mesh.name = name
        if bpy.context.scene.objects.get(name) is None:
            bpy.context.scene.objects.link(dome_mesh)
        self.lock_helper(dome_mesh)
        dome_mesh.draw_type = 'WIRE'
        dome_mesh.matrix_world = Matrix.Identity(4)
        dome_mesh.parent = mesh
        # endregion

        # endregion

        # region Libraries

        # Link the material library
        with bpy.data.libraries.load(filepath=current_file_path+"/../assets/materials.blend", link=True) as (data_from, data_to):
            data_to.materials = [name for name in data_from.materials if name.startswith("Mat_")]

        # Link the models library
        with bpy.data.libraries.load(filepath=current_file_path+"/../assets/models.blend", link=True) as (data_from, data_to):
            data_to.meshes = [name for name in data_from.meshes if name.startswith("Model_")]
            data_to.materials = [name for name in data_from.materials if name.startswith("Mat_")]

        # endregion

        # region Rendering

        # Activate GLSL rendering
        bpy.context.scene.game_settings.material_mode = 'GLSL'

        # Set CURSOR Camera as the rendering camera
        bpy.context.scene.camera = bpy.data.objects[preferences.projection.cursor_camera]
        # Make sure the 3d view is rendered with the current bpy.context.scene.camera
        self.set_camera_view()

        # endregion

        # Reset the matrix world to the saved value if there is one
        saved_pod_matrix = bpy.context.scene.live_editing.saved_pod_matrix
        if saved_pod_matrix:
            pod.matrix_world = saved_pod_matrix

        # Flag as setup so that we can use it
        bpy.context.scene.live_editing.setup = True

        return {'FINISHED'}
