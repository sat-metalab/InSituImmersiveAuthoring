import bpy
from bpy.types import Operator
from bpy.props import StringProperty
from ..nodes.vrpn.VRPNNode import VRPNNode


class VRPNNodeDisconnectOperator(Operator):
    """Connect VRPN Node"""
    bl_idname = "vrpn.disconnect_nodes"
    bl_label = "Disconnect"

    nodename = StringProperty(name='nodename')
    treename = StringProperty(name='treename')

    def execute(self, context):
        trees = bpy.data.node_groups
        if self.treename != "":
            trees = [bpy.data.node_groups[self.treename]]

        for tree in trees:
            nodes = tree.nodes
            if self.nodename != "":
                nodes = [tree.nodes[self.nodename]]

            for node in nodes:
                if isinstance(node, VRPNNode):
                    node.disconnect()

        # Reset values before next call
        self.nodename = ""
        self.treename = ""

        return {'FINISHED'}
