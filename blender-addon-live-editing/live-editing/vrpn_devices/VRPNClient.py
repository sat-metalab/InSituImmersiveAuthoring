from ..Constants import DEBUG_VRPN_ANY, DEBUG_VRPN
from ..utils import Logger


class VRPNClient:

    main_loop_called = False

    def __init__(self):
        if DEBUG_VRPN_ANY:
            self._logger = Logger(self)

        self.host = "localhost"
        self.device_name = ""
        self.client = None

    @property
    def uri(self):
        return self.device_name + "@" + self.host

    def connect(self, host, device_name):
        if DEBUG_VRPN:
            self._logger.open("VRPNClient - Connect", host, device_name)

        self.disconnect()
        self.host = host
        self.device_name = device_name

        if DEBUG_VRPN:
            self._logger.close()

    def disconnect(self):
        self.client = None

    def before_update(self):
        # We only need one mainloop call since VRPN seems to share the connection between devices
        VRPNClient.main_loop_called = False

    def update(self):
        if DEBUG_VRPN:
            self._logger.open("VRPNClient - Update")

        if self.client is not None and not VRPNClient.main_loop_called:
            self.client.mainloop()
            VRPNClient.main_loop_called = True

        if DEBUG_VRPN:
            self._logger.close()
