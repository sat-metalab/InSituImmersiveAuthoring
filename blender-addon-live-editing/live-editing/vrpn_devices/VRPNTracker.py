import vrpn
from mathutils import Vector, Quaternion
from . import VRPNClient


class VRPNTracker(VRPNClient):

    def __init__(self):
        super().__init__()
        self.location = {}
        self.rotation = {}

    def connect(self, host, device_name):
        super().connect(host, device_name)
        print("Connecting VRPNTracker to host", self.uri)
        self.client = vrpn.receiver.Tracker(self.uri)
        self.client.register_change_handler(self, self.on_tracker_position, "position")

    def disconnect(self):
        if self.client is not None:
            try:
                self.client.unregister_change_handler(self, self.on_tracker_position, "position")
            except:
                pass
        super().disconnect()

    def on_tracker_position(self, userdata, data):
        # Sensor
        sensor = data["sensor"]

        # Position
        rawPosition = data["position"]
        self.location[sensor] = Vector((rawPosition[0], -rawPosition[2], rawPosition[1]))

        # Quaternion
        rawQuaternion = data["quaternion"]
        self.rotation[sensor] = Quaternion((rawQuaternion[3], rawQuaternion[0], -rawQuaternion[2], rawQuaternion[1]))
