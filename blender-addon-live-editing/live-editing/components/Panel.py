from ..flex.components import Component, Group
from ..engine import Object3D
from ..engine.geometry import Plane
from ..engine.geometry.shapes import Rectangle
from ..engine.materials import LineBasicMaterial, MeshBasicMaterial


class Panel(Group):

    def __init__(self):
        super().__init__()

        background = Component()
        self.add_element(background)

        self._background_geometry = Plane()
        self._background = Object3D(geometry=self._background_geometry, material=MeshBasicMaterial())
        self._background.material.color = (0.00, 0.00, 0.00)
        self._background.material.opacity = 0.50
        background.add_child(self._background)

        self._stroke = Object3D(
            geometry=Rectangle(),
            material=LineBasicMaterial(color=(1.00, 1.00, 1.00), opacity=0.25)
        )
        background.add_child(self._stroke)

    def update_display_list(self, width, height, depth):
        super().update_display_list(width, height, depth)

        self._stroke.x = self._background.x = width / 2
        self._stroke.y = self._background.y = height / 2
        self._stroke.geometry.width = self._background.geometry.width = width
        self._stroke.geometry.height = self._background.geometry.height = height
