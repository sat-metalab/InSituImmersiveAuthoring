from . import Menu
from ...flex.components import Root, HGroup


class ContextualMenu(Root, Menu):
    def __init__(self):
        super().__init__()

        self.ignore_depth = True

        self._container = HGroup(gap=0.05, vertical_align='JUSTIFY')
        self._container.horizontal_center = 0.00
        self._container.vertical_center = 0.00
        self.add_element(self._container)

    @property
    def container(self):
        return self._container