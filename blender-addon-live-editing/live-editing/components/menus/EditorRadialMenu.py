from . import RadialMenu
from . import HelpMenu
from ...engine import Object3D, TextField
from ...engine.geometry import Circle
from ...engine.geometry.shapes import Circle as CircleShape
from ...engine.materials import LineBasicMaterial, MeshBasicMaterial
from ...utils import Colors


class HelpButton(Object3D):

    def __init__(self, on_click=None):
        super().__init__()

        self._interactive = True
        self._on_click = on_click

        self._background = Object3D(geometry=Circle(radius=1.0), material=MeshBasicMaterial())
        self._background.material.opacity = 0.85
        self.add_child(self._background)

        self._stroke = Object3D(geometry=CircleShape(radius=1.0, line_width=2),
                                material=LineBasicMaterial(color=(1.00, 1.00, 1.00), opacity=0.125))
        self.add_child(self._stroke)

        self._text_field = TextField(text="Help", font="SourceSansPro-Bold.ttf")
        self.add_child(self._text_field)

    def on_cursor_entered(self):
        super().on_cursor_entered()

    def on_cursor_released(self):
        super().on_cursor_released()
        if self._on_click is not None and callable(self._on_click):
            self._on_click(self)

    def update_attributes(self):
        super().update_attributes()

        if self._cursor_over:
            self._background.material.opacity = 1.00
            self._background.material.color = Colors.blue_accent
            self._text_field.material.opacity = 1.0
            self._text_field.color = (0.125, 0.125, 0.125)
        else:
            self._background.material.opacity = 0.85
            self._background.material.color = (0.125, 0.125, 0.125)
            self._text_field.material.opacity = 1.0
            self._text_field.color = Colors.white


class EditorRadialMenu(RadialMenu):
    def __init__(self):
        super().__init__()
        self._did_quick_action = False

        self._help_button = HelpButton(on_click=self._show_help)
        self._help_button.x = -self._outer_radius
        self._help_button.y = self._outer_radius
        self.add_child(self._help_button)

    @property
    def editor(self):
        return self._manager.editor if self._manager is not None else None

    def _show_help(self, target):
        self._manager.open_menu(HelpMenu(self.editor.state_object.help_data, self.editor.pod.matrix_world.inverted() * self.editor.picker.cursor_matrix))

    def on_opening(self):
        super().on_opening()
        self._did_quick_action = False

    def on_closed(self):
        super().on_closed()

        def _reset_hover(child):
            child.reset_hover()
        self.traverse(_reset_hover)

        self.reset_selection()
        self._last_hovered = None

    def update(self, time, delta):
        # XXX: Testing UI scaling
        self.scale.x = self.editor.ui_scale
        self.scale_y = self.editor.ui_scale
        self.scale_z = self.editor.ui_scale

        super().update(time, delta)

    def update_menu(self):
        super().update_menu()

        # Quick action, keep menu down and release over a menu option to trigger it
        if not self._did_quick_action and self.editor.controller1.menu_button.released:
            if self._last_hovered is not None:
                # self._item_action(self._last_hovered)
                self._last_hovered.do_action()
            self._did_quick_action = True
