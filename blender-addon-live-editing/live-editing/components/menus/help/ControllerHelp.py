import bpy
from collections import OrderedDict
from mathutils import Matrix, Vector
from OpenGL.GL import GL_LINES
from ....components import Panel
from ....engine import Object3D
from ....engine.geometry import Shape, Circle
from ....engine.materials import MeshBasicMaterial, LineBasicMaterial
from ....flex.components import Component, HGroup, VGroup, Label
from ....utils.ColorUtils import Colors


class ControllerHelp(Panel):

    def __init__(self, name, horizontal_align, editor, help_data, controller, index, controller_matrix, canvas):
        super().__init__()
        self._name = name
        self._horizontal_align = horizontal_align
        self._editor = editor
        self._help_data = help_data
        self._controller = controller
        self._index = index
        self._controller_matrix = controller_matrix
        self._canvas_component = canvas

        self._is_left = self._index % 2 != 0
        self._entries = OrderedDict([
            ('menu', (["Vive Menu Button " + str(index)], self._controller.menu_button)),
            ('trigger', (["Vive Trigger " + str(index)], self._controller.trigger_button)),
            ('trackpad', (["Vive Trackpad " + str(index)], self._controller.trackpad_button)),
            ('grab', (["Vive Left Gripper " + str(index), "Vive Right Gripper " + str(index)], self._controller.grab_button)),
        ])

        self._container = None
        self._lines_canvas = None
        self._dots = {}
        
    def create_children(self):
        super().create_children()

        self._container = VGroup(horizontal_align='JUSTIFY', gap=0.25)
        self._container.top = 0.25
        self._container.bottom = 0.25
        self._container.left = 0.125
        self._container.right = 0.125
        self.add_element(self._container)

        title_label = Label(self._name, font="SourceSansPro-Black.ttf", align=self._horizontal_align)
        title_label.percent_width = 1.0
        self._container.add_element(title_label)

        self._lines_canvas = Object3D(
            geometry=Shape(line_width=2, mode=GL_LINES),
            material=LineBasicMaterial(color=Colors.foreground)
        )
        self._canvas_component.add_child(self._lines_canvas)

        self._dots = {}
        for key in self._entries.keys():
            self._make_label(key)

    def update(self, time, delta):
        super().update(time, delta)

        index = self._index
        controller = self._controller

        vec = Vector((0.00, 1.00, 0.00))
        vec.rotate(controller.rotation)
        track_quat = vec.to_track_quat('Y', 'Z')
        diff = track_quat.rotation_difference(controller.rotation).to_euler()

        model = bpy.data.objects.get("Vive Controller " + str(index))
        model.matrix_world = \
            self._editor.pod.matrix_world * \
            self._controller_matrix * \
            Matrix.Translation(Vector((-0.75 if self._is_left else 0.75, 0.75, 0.25))) * \
            diff.to_matrix().to_4x4() * \
            Matrix.Scale(10 * self._editor.ui_scale, 4)

        prim_inv_mat = self._lines_canvas.matrix_world.inverted()
        self._lines_canvas.geometry.vertices = []

        for key, value in self._entries.items():
            self._highlight(key, value[0], value[1].down)
            self._make_line(prim_inv_mat, key, value[0][0])

    def _make_label(self, name):
        if self._help_data is None or self._help_data.get(name) is None:
            return

        help = self._help_data[name].split('|')

        dot_container = Component()
        dot = Object3D(
            geometry=Circle(radius=0.0625),
            material=MeshBasicMaterial(color=Colors.foreground)
        )
        dot_container.add_child(dot)

        entry = HGroup(vertical_align='MIDDLE', gap=0.125)
        self._container.add_element(entry)

        if not self._is_left:
            entry.add_element(dot_container)

        lines = VGroup(horizontal_align=self._horizontal_align, gap=0.0625)
        lines.percent_width = 1.0
        entry.add_element(lines)

        default_label = Label(text=help[0], color=Colors.white, font="SourceSansPro-Light.ttf", size=0.3)
        default_label.align = self._horizontal_align
        lines.add_element(default_label)

        mod_label = Label(text=help[1] if len(help) > 1 else "", color=Colors.medium, font="SourceSansPro-LightItalic.ttf", size=0.2)
        mod_label.align = self._horizontal_align
        lines.add_element(mod_label)

        if self._is_left:
            entry.add_element(dot_container)

        self._dots[name] = dot

    def _make_line(self, mx, name, ctrl):
        if self._help_data is None or self._help_data.get(name) is None:
            return
        self._lines_canvas.geometry.vertices.extend(
            self._get_line(
                mx,
                self._dots[name],
                ctrl
            )
        )

    def _get_line(self, mx, from_obj, to_obj_name):
        return [
            *(mx * from_obj.matrix_world).to_translation(),
            *(mx * bpy.data.objects[to_obj_name].matrix_world.translation)
        ]

    def _highlight(self, name, obj_names, highlight):
        if self._help_data is not None and self._help_data.get(name) is not None:
            self._dots[name].material.color = Colors.blue_accent if highlight else Colors.foreground
        for obj_name in obj_names:
            bpy.data.objects[obj_name].material_slots[0].material = bpy.data.materials['Vive Selected'] if highlight else bpy.data.materials['Vive Controller Material']
