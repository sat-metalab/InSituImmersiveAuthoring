import bpy
from . import Menu
from .help import ControllerHelp
from ...flex.components import Component, Root, Group, HGroup, VGroup, Label, Spacer


class HelpMenu(Root, Menu):

    def __init__(self, help_data, original_matrix):
        super().__init__()
        self._help_data = help_data
        self._original_matrix = original_matrix  # Root has y_scale=-1, so we "need" a non-flipped matrix
        self._manager = None
        self._container = None
        self._panels = None
        self._canvas = None
        self._primary_panel = None
        self._secondary_panel = None

    def create_children(self):
        super().create_children()

        self._container = Group()
        self._container.ignore_depth = True  # Ignore here since we want the canvas not to ignore it
        self._container.horizontal_center = 0.00
        self._container.vertical_center = 0.00
        self.add_element(self._container)

        self._canvas = Component()
        self.add_element(self._canvas)

        self._panels = HGroup()
        self._container.add_element(self._panels)

        # PRIMARY PANEL
        self._primary_panel = ControllerHelp(
            name="Primary",
            horizontal_align='RIGHT',
            editor=self._manager.editor,
            help_data=self._help_data.get('primary'),
            controller=self._manager.editor.controller1,
            index=1,
            controller_matrix=self._original_matrix,
            canvas=self._canvas
        )
        self._primary_panel.width = 5.00
        self._panels.add_element(self._primary_panel)

        # SPACER
        self._panels.add_element(Spacer(width=4.00))

        # SECONDARY PANEL
        self._secondary_panel = ControllerHelp(
            name="Secondary",
            horizontal_align='LEFT',
            editor=self._manager.editor,
            help_data=self._help_data.get('secondary'),
            controller=self._manager.editor.controller2,
            index=2,
            controller_matrix=self._original_matrix,
            canvas=self._canvas
        )
        self._secondary_panel.width = 5.00
        self._panels.add_element(self._secondary_panel)

    def on_opened(self):
        # Controllers, (Blender Objects)

        controller3d1 = bpy.data.objects.get("Vive Controller 1")
        controller3d1_children = [ob for ob in bpy.data.objects if ob.parent == controller3d1]
        for child in controller3d1_children:
            child.hide = False

        controller3d2 = bpy.data.objects.get("Vive Controller 2")
        controller3d2_children = [ob for ob in bpy.data.objects if ob.parent == controller3d2]
        for child in controller3d2_children:
            child.hide = False

    def on_closing(self, callback):
        # Controllers, (Blender Objects)

        controller3d1 = bpy.data.objects.get("Vive Controller 1")
        controller3d1_children = [ob for ob in bpy.data.objects if ob.parent == controller3d1]
        for child in controller3d1_children:
            child.hide = True

        controller3d2 = bpy.data.objects.get("Vive Controller 2")
        controller3d2_children = [ob for ob in bpy.data.objects if ob.parent == controller3d2]
        for child in controller3d2_children:
            child.hide = True

        callback()

    @property
    def container(self):
        return self._container

    def update(self, time, delta):
        # XXX: Testing UI scaling
        self.scale.x = self._manager.editor.ui_scale
        self.scale_y = self._manager.editor.ui_scale
        self.scale_z = self._manager.editor.ui_scale

        super().update(time, delta)


# class HelpManager:
#
#     def __init__(self, editor):
#         self._editor = editor
#         self._view = None
#
#     @property
#     def editor(self):
#         return self._editor
#
#     def show(self, help_data=None):
#         if help_data is None:
#             help_data=self.editor.state_object.help_data
#
#         mx = self.editor.pod.matrix_world.inverted() * self._editor.picker.cursor_matrix.copy()
#         self._view = HelpView(self, help_data, mx)
#         self._view.matrix_world = mx
#         self._editor.hud.add_child(self._view)
#
#         controller3d1 = bpy.data.objects.get("Vive Controller 1")
#         controller3d1_children = [ob for ob in bpy.data.objects if ob.parent == controller3d1]
#         for child in controller3d1_children:
#             child.hide = False
#
#         controller3d2 = bpy.data.objects.get("Vive Controller 2")
#         controller3d2_children = [ob for ob in bpy.data.objects if ob.parent == controller3d2]
#         for child in controller3d2_children:
#             child.hide = False
#
#     def hide(self):
#         self._editor.hud.remove_child(self._view)
#         self._view.dispose()
#         self._view = None
#
#         controller3d1 = bpy.data.objects.get("Vive Controller 1")
#         controller3d1_children = [ob for ob in bpy.data.objects if ob.parent == controller3d1]
#         for child in controller3d1_children:
#             child.hide = True
#
#         controller3d2 = bpy.data.objects.get("Vive Controller 2")
#         controller3d2_children = [ob for ob in bpy.data.objects if ob.parent == controller3d2]
#         for child in controller3d2_children:
#             child.hide = True

