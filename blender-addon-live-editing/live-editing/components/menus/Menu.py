class Menu:

    def __init__(self):
        self._manager = None
        self._state = None

    def on_opening(self):
        pass

    def on_opened(self):
        pass

    def on_closing(self, callback):
        callback()

    def on_closed(self):
        pass

    def update_menu(self):
        pass

    def close(self):
        if self._manager is not None:
            self._manager.close_menu()
