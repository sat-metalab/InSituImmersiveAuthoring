import bpy
from bpy.types import Panel


class LiveEditingPanel:
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "Live Editing"

    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'


class SceneSetupViewportToolbar(Panel, LiveEditingPanel):
    bl_label = "Scene Setup"

    def draw(self, context):
        layout = self.layout

        col = layout.column(align=True)
        row = col.row(align=True)
        row.scale_y = 2
        row.operator("live_editing.setup_scene", icon='SCENE_DATA')


class VRPNViewportToolbar(Panel, LiveEditingPanel):
    bl_label = "VRPN"

    def draw(self, context):
        preferences = context.user_preferences.addons[__package__].preferences
        layout = self.layout

        col = layout.column(align=True)
        box = col.box()
        col = box.column(align=True)
        col.prop(preferences.vrpn, "host")
        col.prop(preferences.vrpn, "controller_1")
        col.prop(preferences.vrpn, "controller_2")
        row = col.row(align=True)
        row.operator("vrpn.connect")
        row.operator("vrpn.disconnect")
        row = col.row(align=True)
        row.operator("vrpn.connect_nodes", text="Connect Nodes")
        row.operator("vrpn.disconnect_nodes", text="Disconnect Nodes")


class RenderPanel(Panel):
    bl_idname = "RENDER_PT_live_editing"
    bl_label = "Live Editing"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        preferences = context.user_preferences.addons[__package__].preferences
        layout = self.layout

        col = layout.column(align=True)
        box = col.box()
        box.label(text="VRPN")
        col = box.column(align=True)
        col.prop(preferences.vrpn, "host")
        col.prop(preferences.vrpn, "controller_1")
        col.prop(preferences.vrpn, "controller_2")

        col = layout.column(align=True)
        box = col.box()
        box.label(text="Tracking")
        box.prop(preferences.tracking, "tracking_space_position")
        box.prop(preferences.tracking, "tracking_space_rotation")
        box.prop(preferences.tracking, "tracker_rotation")

        col = layout.column(align=True)
        box = col.box()
        box.label(text="UI")
        box.prop(preferences.ui, "ui_scale")

        col = layout.column(align=True)
        box = col.box()
        box.label(text="Gallery")
        box.prop(preferences.gallery, "assets_path")


class ObjectPanel(Panel):
    bl_idname = "OBJECT_PT_live_editing"
    bl_label = "Live Editing"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout

        obj = context.object
        props = obj.live_editing

        row = layout.row()
        row.prop(props, "lock")
        row.prop(props, "non_editable")
