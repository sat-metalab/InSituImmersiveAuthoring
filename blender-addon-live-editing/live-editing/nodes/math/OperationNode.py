import math
from bpy.props import EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class OperationNode(LiveEditingBaseNode):
    """Operation Node"""
    bl_idname = 'OperationNode'
    bl_label = 'Operation'
    bl_icon = 'PLUS'

    op = EnumProperty(
        name="Operation",
        description="Math Operation",
        default="ADD",
        items=[
            ("ADD", "A + B", "A plus B", 1),
            ("SUB", "A - B", "A minus B", 2),
            ("MUL", "A * B", " A multiplied by B", 3),
            ("DIV", "A / B", "A divided by B", 4),
            ("MIN", "min(A, B)", "Lowest between A and B", 5),
            ("MAX", "max(A, B)", "Highest between A and B", 6),
            ("MOD", "A % B", "A modulo B", 7),
            ("POW", "A ^ B", "A to the power of B", 8),
            ("ABS", "abs(A)", "Absolute value of A", 9),
            ("CEIL", "ceil(A)", "Ceil A", 10),
            ("FLOOR", "floor(A)", "Floor A", 11),
            ("ROUND", "round(A)", "Round A", 12),
            ("SQRT", "sqrt(A)", "Square Root of A", 13),
            ("LOG", "log(A)", "Natural logarithm (base e) of A", 14),
            ("LOG2", "log2(A)", "Logarithm (base 2) of A", 15),
            ("LOG10", "log10(A)", "Logarithm (base 10) of A", 16),
            ("LOGB", "log(A,B)", "Logarithm (base B) of A", 17),
            ("EXP", "e ^ A", "Exp A", 18),
            ("SIN", "sin(A)", "Sine of A", 19),
            ("SINH", "sinh(A)", "Hyperbolic sine of A", 20),
            ("COS", "cos(A)", "Cosine of A", 21),
            ("COSH", "cosh(A)", "Hyperbolic cosine of A", 22),
            ("TAN", "tan(A)", "Tangent of A", 23),
            ("TANH", "tanh(A)", "Hyperbolic tangent of A", 24),
            ("ASIN", "asin(A)", "Arc sine of A", 25),
            ("ASINH", "asinh(A)", "Inverse hyperbolic sine of A", 26),
            ("ACOS", "acos(A)", "Arc cosine of A", 27),
            ("ACOSH", "acosh(A)", "Inverse hyperbolic cosine of A", 28),
            ("ATAN", "atan(A)", "Arc tangent of A", 29),
            ("ATAN2", "atan2(A,B)", "Arc tangent of A/B", 30),
            ("ATANH", "atanh(A)", "Inverse hyperbolic tangent of A", 31),
            ("HYPOT", "hypot(A,B)", "Hypotenuse", 31)
        ]
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketFloat', "Input A")
        self.inputs.new('NodeSocketFloat', "Input B")

        self.outputs.new('NodeSocketFloat', "Output")

    def draw_buttons(self, context, layout):
        layout.prop(self, "op")

    def run(self):
        out = 0.00

        # A-Only Operations
        a = self.getInputValue("Input A")

        if self.op == "ABS":
            out = math.fabs(a)        
        elif self.op == "CEIL":
            out = math.ceil(a)
        elif self.op == "FLOOR":
            out =  math.floor(a)
        elif self.op == "ROUND":
            out = round(a)
        elif self.op == "SQRT":
            out = math.sqrt(a)
        elif self.op == "LOG":
            out = math.log(a)
        elif self.op == "LOG2":
            out = math.log2(a)
        elif self.op == "LOG10":
            out = math.log10(a)
        elif self.op == "EXP":
            out = math.exp(a)
        elif self.op == "SIN":
            out = math.sin(a)
        elif self.op == "COS":
            out = math.cos(a)
        elif self.op == "TAN":
            out = math.tan(a)
        elif self.op == "ASIN":
            out = math.asin(a)
        elif self.op == "ACOS":
            out = math.acos(a)
        elif self.op == "ATAN":
            out = math.atan(a)
        else:
            # A - B Operations
            b = self.getInputValue("Input B")

            if self.op == "ADD":
                out = a + b
            elif self.op == "SUB":
                out = a - b
            elif self.op == "MUL":
                out = a * b
            elif self.op == "DIV":
                out = a / b if b != 0 else 0
            elif self.op == "MIN":
                out = min(a, b)
            elif self.op == "MAX":
                out = max(a, b)
            elif self.op == "MOD":
                out = a % b if b != 0 else 0
            elif self.op == "POW":
                out = math.pow(a, b)
            elif self.op == "LOGB":
                out = math.log(a, b)
            elif self.op == "ATAN2":
                out = math.atan2(a, b)
            elif self.op == "HYPOT":
                out = math.hypot(a, b)

        self.outputs["Output"].default_value = out
