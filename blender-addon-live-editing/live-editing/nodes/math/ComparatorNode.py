from bpy.props import EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode

class ComparatorNode(LiveEditingBaseNode):
    """Comparator Node"""
    bl_idname = 'ComparatorNode'
    bl_label = 'Comparator'
    bl_icon = 'LAMP_DATA'

    op = EnumProperty(
        name="Operation",
        description="Comparator Operation",
        default="EQ",
        items=[
            ("EQ", "A == B", "A equals B", 1),
            ("NEQ",  "A != B",  "A does not equal B",  2),
            ("GT", "A > B", " A is greater than B", 3),
            ("GTEQ", "A >= B", "A is greater than or equals B", 4),
            ("LT", "A < B", "A is lower than B", 5),
            ("LTEQ", "A <= B", "A is lower than or equals B", 6)
        ]
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketFloat', "Input A")
        self.inputs.new('NodeSocketFloat', "Input B")

        self.outputs.new('NodeSocketBool', "Output")

    def draw_buttons(self, context, layout):
        layout.prop(self, "op")

    def run(self):
        a = self.getInputValue("Input A")
        b = self.getInputValue("Input B")

        out = False

        if self.op == "EQ":
            out = a == b
        elif self.op == "NEQ":
            out = a != b
        elif self.op == "GT":
            out = a > b
        elif self.op == "GTEQ":
            out = a >= b
        elif self.op == "LT":
            out = a < b
        elif self.op == "LTEQ":
            out = a <= b

        self.outputs["Output"].default_value = out
