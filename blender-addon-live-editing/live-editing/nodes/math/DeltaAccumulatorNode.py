from bpy.props import FloatProperty, BoolProperty, EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class DeltaAccumulatorNode(LiveEditingBaseNode):
    """Delta Accumulator Node"""
    bl_idname = 'DeltaAccumulatorNode'
    bl_label = 'Delta Accumulator'

    accumulate = BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    lastValue = FloatProperty(
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Reset")
        self.inputs.new('NodeSocketBool', "Accumulate")
        self.inputs.new('NodeSocketFloat', "Value")

        self.outputs.new('NodeSocketBool', "Accumulating")
        self.outputs.new('NodeSocketFloat', "Value")

    def run(self):
        reset = self.getInputValue("Reset")
        if reset:
            self.lastValue = 0.00
            self.outputs["Value"].default_value = 0.00

        accumulate = self.getInputValue("Accumulate")
        if accumulate and not self.accumulate:
            self.lastValue = self.getInputValue("Value")
            self.outputs["Accumulating"].default_value = True
            self.accumulate = True

        elif not accumulate and self.accumulate:
            self.outputs["Accumulating"].default_value = False
            self.accumulate = False

        elif accumulate:
            value = self.getInputValue("Value")
            self.outputs["Value"].default_value += value - self.lastValue
            self.lastValue = value