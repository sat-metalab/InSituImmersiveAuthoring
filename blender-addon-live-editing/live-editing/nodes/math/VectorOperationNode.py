from mathutils import Vector, Quaternion
from bpy.props import EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class VectorOperationNode(LiveEditingBaseNode):
    """Vector Operation Node"""
    bl_idname = 'VectorOperationNode'
    bl_label = 'Vector Operation'
    bl_icon = 'PLUS'

    op = EnumProperty(
        name="Operation",
        description="Vector Operation",
        default="ADD",
        items=[
            ("ADD", "A + B", "A plus B", 1),
            ("SUB", "A - B", "A minus B", 2),
            ("MUL", "A * B", " A multiplied by B", 3),
            ("ANGLE", "Angle (Scalat)", "Angle between A and B", 4),
            ("ANGLE_SIGNED", "Angle (Signed, Scalar)", "Signed angle between A and B", 5),
            ("CROSS", "Cross", "Cross product of A and B", 6),
            ("DOT", "Dot", "Dot product of A and B", 7),
            ("NORM", "Normalize", "Normalize A", 8),
            ("ORTHO", "Orthogonal", "Vector perpendicular to A", 9),
            ("PROJECT", "Project", "Projection of A onto B", 10),
            ("REFLECT", "Reflect", "Reflection of A from mirror B", 11),
            ("ROTATION_DIFFERENCE", "Rotation Difference (Quat)", "Difference of rotation between A and B", 12)
        ]
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Vector A")
        self.inputs.new('NodeSocketVectorXYZ', "Vector B")

        self.outputs.new('NodeSocketVectorXYZ', "Vector")
        self.outputs.new('NodeSocketFloat', "Scalar")
        self.outputs.new('NodeSocketQuaternion', "Quaternion")

    def draw_buttons(self, context, layout):
        layout.prop(self, "op")

    def run(self):
        out_v = None
        out_s = None
        out_q = None

        # A-Only Operations
        a = self.getInputValue("Vector A")

        if self.op == "NORM":
            out_v = a.normalized()
        elif self.op == "ORTHO":
            out_v = a.orthogonal()
        else:
            # A - B Operations
            b = self.getInputValue("Vector B")

            if self.op == "ADD":
                out_v = a + b
            elif self.op == "SUB":
                out_v = a - b
            elif self.op == "MUL":
                out_v = a * b
            elif self.op == "ANGLE":
                out_s = a.angle(b, 0.00)
            elif self.op == "ANGLE_SIGNED":
                out_s = a.angle_signed(b, 0.00)
            elif self.op == "CROSS":
                out_v = a.cross(b)
            elif self.op == "DOT":
                out_v = a.dot(b)
            elif self.op == "PROJECT":
                out_v = a.project(b)
            elif self.op == "REFLECT":
                out_v = a.reflect(b)
            elif self.op == "ROTATION_DIFFERENCE":
                out_q = a.rotation_difference(b)

        self.outputs["Vector"].default_value = out_v if out_v is not None else Vector()
        self.outputs["Scalar"].default_value = out_s if out_s is not None else 0.00
        self.outputs["Quaternion"].default_value = out_q if out_q is not None else Quaternion((1.00,0.00,0.00,0.00))
