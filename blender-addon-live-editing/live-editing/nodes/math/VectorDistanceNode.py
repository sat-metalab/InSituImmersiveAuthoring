from ..LiveEditingBaseNode import LiveEditingBaseNode


class VectorDistanceNode(LiveEditingBaseNode):
    """Vector Distance Node"""
    bl_idname = 'VectorDistanceNode'
    bl_label = 'Vector Distance'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Vector A")
        self.inputs.new('NodeSocketVectorXYZ', "Vector B")

        self.outputs.new('NodeSocketFloat', "Distance")

    def run(self):
        self.outputs["Distance"].default_value = (self.getInputValue("Vector B") - self.getInputValue("Vector A")).length
