from mathutils import Euler
from ..LiveEditingBaseNode import LiveEditingBaseNode


class EulerXYZToQuaternionNode(LiveEditingBaseNode):
    """Euler XYZ To Quaternion Node"""
    bl_idname = 'EulerXYZToQuaternionNode'
    bl_label = 'Euler XYZ To Quaternion'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorEuler', "Euler XYZ")

        self.outputs.new('NodeSocketQuaternion', "Quaternion")

    def run(self):
        self.outputs["Quaternion"].default_value = Euler(self.getInputValue("Euler XYZ")).to_quaternion()
