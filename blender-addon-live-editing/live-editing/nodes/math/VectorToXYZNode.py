from ..LiveEditingBaseNode import LiveEditingBaseNode


class VectorToXYZNode(LiveEditingBaseNode):
    """Vector to XYZ Node"""
    bl_idname = 'VectorToXYZNode'
    bl_label = 'Vector to XYZ'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Vector")

        self.outputs.new('NodeSocketFloat', "X")
        self.outputs.new('NodeSocketFloat', "Y")
        self.outputs.new('NodeSocketFloat', "Z")

    def run(self):
        vector = self.getInputValue("Vector")
        self.outputs["X"].default_value = vector.x
        self.outputs["Y"].default_value = vector.y
        self.outputs["Z"].default_value = vector.z
