from ..LiveEditingBaseNode import LiveEditingBaseNode


class RotateVectorNode(LiveEditingBaseNode):
    """Rotate Vector Node"""
    bl_idname = 'RotateVectorNode'
    bl_label = 'Rotate Vector'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Vector")
        self.inputs.new('NodeSocketQuaternion', "Rotation")

        self.outputs.new('NodeSocketVectorXYZ', "Vector")

    def run(self):
        vector = self.getInputValue("Vector").copy()
        vector.rotate(self.getInputValue("Rotation"))
        self.outputs["Vector"].default_value = vector
