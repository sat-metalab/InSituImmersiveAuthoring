from bpy.props import FloatVectorProperty, BoolProperty
from mathutils import Quaternion
from ..LiveEditingBaseNode import LiveEditingBaseNode


class QuaternionDeltaAccumulatorNode(LiveEditingBaseNode):
    """Quaternion Delta Accumulator Node"""
    bl_idname = 'QuaternionDeltaAccumulatorNode'
    bl_label = 'Quaternion Delta Accumulator'

    accumulate = BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    lastValue = FloatVectorProperty(
        size=4,
        subtype='QUATERNION',
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Reset")
        self.inputs.new('NodeSocketBool', "Accumulate")
        self.inputs.new('NodeSocketQuaternion', "Quaternion")

        self.outputs.new('NodeSocketBool', "Accumulating")
        self.outputs.new('NodeSocketQuaternion', "Quaternion")

    def run(self):
        reset = self.getInputValue("Reset")
        if reset:
            self.lastValue = Quaternion((1.0, 0.0, 0.0, 0.0))
            self.outputs["Quaternion"].default_value = Quaternion((1.0, 0.0, 0.0, 0.0))

        accumulate = self.getInputValue("Accumulate")
        if accumulate and not self.accumulate:
            self.lastValue = self.getInputValue("Quaternion").inverted()
            self.outputs["Accumulating"].default_value = True
            self.accumulate = True

        elif not accumulate and self.accumulate:
            self.outputs["Accumulating"].default_value = False
            self.accumulate = False

        elif accumulate:
            value = self.getInputValue("Quaternion") # No need to copy we're not using directly

            self.outputs["Quaternion"].default_value *= self.lastValue * value
            self.lastValue = value.inverted()
