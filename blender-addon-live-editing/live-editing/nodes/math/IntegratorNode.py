import time
from bpy.props import FloatProperty, BoolProperty, EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class IntegratorNode(LiveEditingBaseNode):
    """Integrator Node"""
    bl_idname = 'IntegratorNode'
    bl_label = 'Integrator'

    def updateType(self,context):
        """
        TODO: Keep connection when destroying/creating inputs/outputs
        """

        input = self.inputs.get("Input")
        if input:
            self.inputs.remove(input)

        output = self.outputs.get("Output")
        if output:
            self.outputs.remove(output)

        if self.input_type == "FLOAT":
            self.inputs.new('NodeSocketFloat', "Input")
            self.outputs.new('NodeSocketFloat', "Output")

        elif self.input_type == "VECTOR":
            self.inputs.new('NodeSocketVectorXYZ', "Input")
            self.outputs.new('NodeSocketVectorXYZ', "Output")

        elif self.input_type == "QUATERNION":
            self.inputs.new('NodeSocketQuaternion', "Input")
            self.outputs.new('NodeSocketQuaternion', "Output")

    input_type = EnumProperty(
        name="Type",
        description="Input Type",
        default="FLOAT",
        items=[
            ("FLOAT", "Float", "Floating point", 1),
            ("VECTOR", "Vector", "Vector", 2),
            ("QUATERNION", "Quaternion", "Quaternion", 3)
        ],
        update=updateType
    )

    def init(self, context):
        super().init(context)

        self["integrate"] = False
        self["lastTime"] = 0.0

        self.inputs.new('NodeSocketBool', "Reset")
        self.inputs.new('NodeSocketBool', "Integrate")
        self.inputs.new('NodeSocketBool', "Use Time Scale")
        scale = self.inputs.new('NodeSocketFloat', "Scale")
        scale.default_value = 1.00
        self.inputs.new('NodeSocketFloat', "Input")

        self.outputs.new('NodeSocketBool', "Integrating")
        self.outputs.new('NodeSocketFloat', "Output")

    def draw_buttons_ext(self, context, layout):
        layout.prop(self, "input_type")

    def run(self):
        reset = self.getInputValue("Reset")
        if reset:
            self.outputs["Output"].default_value = 0.00

        integrate = self.getInputValue("Integrate")
        if integrate and not self["integrate"]:
            self.outputs["Integrating"].default_value = True
            self["integrate"] = True
            self["lastTime"] = time.time()

        elif not integrate and self["integrate"]:
            self.outputs["Integrating"].default_value = False
            self["integrate"] = False

        elif integrate:
            now = time.time()
            delta = now - self["lastTime"]
            self["lastTime"] = now

            value = self.getInputValue("Input")
            scale = self.getInputValue("Scale")
            use_time_scale = self.getInputValue("Use Time Scale")

            if use_time_scale:
                scale *= delta

            if self.input_type == "FLOAT":
                self.outputs["Output"].default_value += value * scale

            elif self.input_type == "VECTOR":
                self.outputs["Output"].default_value += value * scale

            elif self.input_type == "QUATERNION":
                # TODO: This might not mean what I think
                quat = value.copy()
                quat.w *= scale
                quat.normalize()
                self.outputs["Output"].default_value *= quat