from bpy.props import FloatVectorProperty, BoolProperty
from mathutils import Vector
from ..LiveEditingBaseNode import LiveEditingBaseNode


class VectorDeltaAccumulatorNode(LiveEditingBaseNode):
    """Vector Delta Accumulator Node"""
    bl_idname = 'VectorDeltaAccumulatorNode'
    bl_label = 'Vector Delta Accumulator'

    accumulate = BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    lastValue = FloatVectorProperty(
        size=3,
        subtype='XYZ',
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Reset")
        self.inputs.new('NodeSocketBool', "Accumulate")
        self.inputs.new('NodeSocketVectorXYZ', "Vector")

        self.outputs.new('NodeSocketBool', "Accumulating")
        self.outputs.new('NodeSocketVectorXYZ', "Vector")

    def run(self):
        reset = self.getInputValue("Reset")
        if reset:
            self.lastValue = Vector()
            self.outputs["Vector"].default_value = Vector()

        accumulate = self.getInputValue("Accumulate")
        if accumulate and not self.accumulate:
            self.lastValue = self.getInputValue("Vector").copy()
            self.outputs["Accumulating"].default_value = True
            self.accumulate = True

        elif not accumulate and self.accumulate:
            self.outputs["Accumulating"].default_value = False
            self.accumulate = False

        elif accumulate:
            value = self.getInputValue("Vector").copy()
            self.outputs["Vector"].default_value += value - self.lastValue
            self.lastValue = value
