import bpy
from bpy.types import NodeGroup, NodeCustomGroup


class LiveEditingNodeGroup(NodeGroup):
    """LiveEditingNodeGroup"""
    bl_idname = 'LiveEditingNodeGroup'
    bl_label = 'LiveEditingNodeGroup'

    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname == 'LiveEditingTree'

    def init(self, context):
        pass
        #super().init(context)

    def update(self):
        pass
        #super().update()