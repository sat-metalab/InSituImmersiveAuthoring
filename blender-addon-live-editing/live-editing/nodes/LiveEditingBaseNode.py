import bpy
from bpy.types import Node


class LiveEditingBaseNode(Node):
    """Live Editing Base node"""
    bl_idname = 'LiveEditingBaseNode'
    bl_label = 'Live Editing'
    bl_icon = 'LOGIC'

    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname == 'LiveEditingTree'

    def init(self, context):
        self['dirty'] = True

    def update(self):
        pass

    def free(self):
        pass

    def treeUpdate(self, tree):
        pass

    def preRun(self):
        self['dirty'] = True

    def _run(self):
        if self['dirty']:
            self.run()
            self['dirty'] = False

    def run(self):
        pass

    def isInputConnected(self, inputName):
        input = self.inputs[inputName]  # Let it scream at us if we misspelled
        if input is None or not input.is_linked:
            return False

        link = input.links[0] if len(input.links) > 0 else None
        if link is None:
            return False

        return link.is_valid

    def getInputValue(self, inputName):
        input = self.inputs[inputName]  # Let it scream at us if we misspelled
        link = input.links[0] if input is not None and len(input.links) > 0 else None
        if input is not None and input.is_linked and link is not None and link.is_valid:
            link.from_node._run()
            return link.from_socket.default_value
        else:
            return input.default_value
