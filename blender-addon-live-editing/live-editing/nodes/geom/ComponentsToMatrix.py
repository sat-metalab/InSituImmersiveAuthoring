import bpy
from mathutils import Matrix
from ...utils import MatrixUtils
from ..LiveEditingBaseNode import LiveEditingBaseNode


class ComponentsToMatrixNode(LiveEditingBaseNode):
    """Components to Matrix Node"""
    bl_idname = 'ComponentsToMatrixNode'
    bl_label = 'Components to Matrix'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorTranslation', "Translation")
        self.inputs.new('NodeSocketQuaternion', "Rotation")
        self.inputs.new('NodeSocketVectorXYZ', "Scale")

        self.outputs.new('NodeSocketMatrix', "Matrix")

    def run(self):
        translation = Matrix.Translation(self.getInputValue("Translation"))

        rotation = self.getInputValue("Rotation").to_matrix().to_4x4()

        scaleVector = self.getInputValue("Scale") if self.isInputConnected("Scale") else 1.00, 1.00, 1.00
        scale = Matrix.Identity(4)
        scale[0][0] = scaleVector[0]
        scale[1][1] = scaleVector[1]
        scale[2][2] = scaleVector[2]

        self.outputs["Matrix"].default_value = MatrixUtils.flatten(translation * rotation * scale)
