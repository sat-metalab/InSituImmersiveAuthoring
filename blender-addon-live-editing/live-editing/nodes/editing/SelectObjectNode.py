import bpy
from bpy.props import BoolProperty
from ..LiveEditingOutputNode import LiveEditingOutputNode


class SelectObjectNode(LiveEditingOutputNode):
    """Select Object Node"""
    bl_idname = 'SelectObjectNode'
    bl_label = 'Select Object'
    bl_icon = 'EDIT'

    selected = BoolProperty(
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Momentary")
        self.inputs.new('NodeSocketBool', "Select")
        self.inputs.new('NodeSocketString', "Object")

    def run(self):
        select = self.getInputValue("Select")
        if not select:
            return self.reset() if self.getInputValue("Momentary") else None

        objectName = self.getInputValue("Object")
        if not objectName:
            return self.reset() if self.getInputValue("Momentary") else None

        obj = bpy.data.objects.get(objectName)
        if not obj:
            return self.reset() if self.getInputValue("Momentary") else None

        if obj.select:
            return

        for o in bpy.context.scene.objects:
            o.select = False
        obj.select = True
        self.selected = True

    def reset(self):
        if self.selected:
            for o in bpy.context.scene.objects:
                o.select = False
        self.selected = False
