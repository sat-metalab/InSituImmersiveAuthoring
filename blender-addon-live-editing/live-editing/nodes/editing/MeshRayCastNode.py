import bpy
import math
from mathutils import Vector, Quaternion, Matrix
from ..LiveEditingBaseNode import LiveEditingBaseNode


class MeshRayCastNode(LiveEditingBaseNode):
    """Mesh Ray Cast Node"""
    bl_idname = 'MeshRayCastNode'
    bl_label = 'Mesh Ray Cast'
    bl_icon = 'SNAP_NORMAL'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Mesh Object")
        self.inputs.new('NodeSocketVectorXYZ', "Camera Location")
        self.inputs.new('NodeSocketVectorXYZ', "Location")
        self.inputs.new('NodeSocketQuaternion', "Rotation")

        self.outputs.new('NodeSocketVectorXYZ', "Mesh Intersection")
        self.outputs.new('NodeSocketQuaternion', "Ray Orientation")
        self.outputs.new('NodeSocketBool', "Result")
        self.outputs.new('NodeSocketString', "Object")
        self.outputs.new('NodeSocketVectorXYZ', "Location")
        self.outputs.new('NodeSocketVectorXYZ', "Normal")

    def run(self):

        inputObjectName = self.getInputValue("Mesh Object")
        if not inputObjectName:
            return self.nothing(True)

        roomMeshObj = bpy.data.objects.get(inputObjectName)
        if not roomMeshObj:
            return self.nothing(True)

        inputRayLocation = self.getInputValue("Location")
        inputRayRotation = self.getInputValue("Rotation")

        # ROOM MESH INTERSECTION

        roomMeshInvMatrix = roomMeshObj.matrix_world.inverted()
        roomMeshRayLocation = roomMeshInvMatrix * inputRayLocation

        roomMeshRayDirection = Vector((0.0, 1.0, 0.0))
        roomMeshRayDirection.rotate(inputRayRotation * roomMeshInvMatrix.to_quaternion())

        result, loc, norm, index = roomMeshObj.ray_cast(roomMeshRayLocation, roomMeshRayDirection)
        if not result:
            return self.nothing(True)

        # Hit Location Output
        roomHitPoint = roomMeshObj.matrix_world.inverted() * loc
        self.outputs["Mesh Intersection"].default_value = roomHitPoint

        # CAMERA -> ROOM MESH INTERSECTION RAYCAST

        cameraLocation = self.getInputValue("Camera Location")

        rayDirectionVector = loc - cameraLocation
        rayDirectionVector.normalize()
        rayOrientationVector = Vector((0.0, 1.0, 0.0))
        rayOrientation = Quaternion(rayOrientationVector.cross(rayDirectionVector), math.acos(rayOrientationVector.dot(rayDirectionVector)))

        self.outputs["Ray Orientation"].default_value = rayOrientation

        result = None
        location = None
        object = None
        normal = None

        distance = math.inf

        for obj in bpy.context.scene.objects:
            if obj.live_editing.lock or obj.type != 'MESH' or not obj.is_visible(bpy.context.scene):
                continue

            # Put the ray in obj's space
            objInvMat = obj.matrix_world.inverted()
            objRayLoc = objInvMat * cameraLocation
            objRayVec = ((objInvMat * roomHitPoint) - objRayLoc).normalized()

            res, loc, norm, index = obj.ray_cast(objRayLoc, objRayVec)
            if not res:
                continue

            dist = math.fabs((obj.matrix_world.translation - cameraLocation).length)
            if dist > distance:
                continue

            object = obj
            location = loc
            normal = norm
            distance = dist

        result = object is not None

        if not result:
            return self.nothing(False)

        self.outputs["Result"].default_value = result
        self.outputs["Object"].default_value = object.name
        self.outputs["Location"].default_value = location
        self.outputs["Normal"].default_value = normal

    def nothing(self, resetDomeValues):
        if resetDomeValues:
            self.outputs["Mesh Intersection"].default_value = (0.00, 0.00, 0.00)
            self.outputs["Ray Orientation"].default_value = Quaternion((1.00, 0.00, 0.00, 0.00))

        self.outputs["Result"].default_value = False
        self.outputs["Object"].default_value = ""
        self.outputs["Location"].default_value = (0.00, 0.00, 0.00)
        self.outputs["Normal"].default_value = (0.00, 0.00, 0.00)
