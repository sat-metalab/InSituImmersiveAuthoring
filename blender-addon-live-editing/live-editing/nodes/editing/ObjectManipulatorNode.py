import bpy
from bpy.props import BoolProperty
from mathutils import Matrix, Vector
from ..LiveEditingOutputNode import LiveEditingOutputNode


class ObjectManipulatorNode(LiveEditingOutputNode):
    """Object Manipulator Node"""
    bl_idname = 'ObjectManipulatorNode'
    bl_label = 'Object Manipulator'
    bl_icon = 'EDIT'

    useDefaultLocation = BoolProperty(
        name="Use default location",
        default=False
    )

    useDefaultRotation = BoolProperty(
        name="Use default rotation",
        default=False
    )

    worldAxis = BoolProperty(
        name="World coord. axis",
        default=False
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Object")
        self.inputs.new('NodeSocketVectorXYZ', "Location")
        self.inputs.new('NodeSocketQuaternion', "Rotation")
        self.inputs.new('NodeSocketVectorXYZ', "Axis")

    def draw_buttons(self, context, layout):
        layout.prop(self, "useDefaultLocation")
        layout.prop(self, "useDefaultRotation")
        layout.prop(self, "worldAxis")

    def run(self):
        obj = None

        objectName = self.getInputValue("Object")
        if objectName:
            obj = bpy.data.objects.get(objectName)

        # We get the values in order to activate the graph path event if we end up not
        # using them. It was a quick fix to prevent paths not being updated when releasing
        # the manipulated object
        location = self.getInputValue("Location")
        rotation = self.getInputValue("Rotation")
        axis = self.getInputValue("Axis")

        if obj is None:
            return

        if self.worldAxis and self.isInputConnected("Axis"):
            axis = obj.matrix_world.inverted() * axis

        if self.useDefaultLocation or self.isInputConnected("Location"):
            translation_matrix = Matrix.Translation(location - axis)
        else:
            translation_matrix = Matrix.Translation(Vector())

        if self.useDefaultRotation or self.isInputConnected("Rotation"):
            intermediate_rotation_matrix = rotation.to_matrix().to_4x4()

            rotation_matrix = Matrix.Identity(4)
            rotation_matrix *= Matrix.Translation(axis)
            rotation_matrix *= intermediate_rotation_matrix
            rotation_matrix *= Matrix.Translation(-axis)
        else:
            rotation_matrix = Matrix.Identity(4)

        obj.matrix_world = translation_matrix * rotation_matrix
