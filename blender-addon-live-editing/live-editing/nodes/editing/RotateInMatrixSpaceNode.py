import bpy
from mathutils import Matrix, Quaternion
from ..LiveEditingBaseNode import LiveEditingBaseNode


class RotateInMatrixSpaceNode(LiveEditingBaseNode):
    """Rotate In Matrix Space Node"""
    bl_idname = 'RotateInMatrixSpaceNode'
    bl_label = 'Rotate In Matrix Space'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketMatrix', "Matrix")
        self.inputs.new('NodeSocketQuaternion', "Rotation")

        self.outputs.new('NodeSocketQuaternion', "Rotation")

    def run(self):
        matrix = self.getInputValue("Matrix")
        rotation = self.getInputValue("Rotation")

        mat = matrix.inverted()
        mat *= rotation.to_matrix().to_4x4()
        mat *= matrix

        self.outputs["Rotation"].default_value = mat.to_quaternion()
