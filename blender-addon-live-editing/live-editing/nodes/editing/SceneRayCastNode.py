import bpy
import math
from mathutils import Vector
from ..LiveEditingBaseNode import LiveEditingBaseNode


class SceneRayCastNode(LiveEditingBaseNode):
    """Scene Ray Cast Node"""
    bl_idname = 'SceneRayCastNode'
    bl_label = 'Scene Ray Cast'
    bl_icon = 'SNAP_NORMAL'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Location")
        self.inputs.new('NodeSocketQuaternion', "Rotation")

        self.outputs.new('NodeSocketBool', "Result")
        self.outputs.new('NodeSocketString', "Object")
        self.outputs.new('NodeSocketVectorXYZ', "Location")
        self.outputs.new('NodeSocketVectorXYZ', "Normal")

    def run(self):
        rayLocation = self.getInputValue("Location")
        rayRotation = self.getInputValue("Rotation")

        rayVector = Vector((0.0, 1.0, 0.0))
        rayVector.rotate(rayRotation)

        result, location, normal, index, object, matrix = bpy.context.scene.ray_cast(rayLocation, rayVector)

        self.outputs["Result"].default_value = result

        if result:
            self.outputs["Object"].default_value = object.name
            self.outputs["Location"].default_value = location
            self.outputs["Normal"].default_value = normal
        else:
            self.outputs["Object"].default_value = ""
            self.outputs["Location"].default_value = (0.00, 0.00, 0.00)
            self.outputs["Normal"].default_value = (0.00, 0.00, 0.00)
