import bpy
from bpy.props import BoolProperty
from ..LiveEditingOutputNode import LiveEditingOutputNode


class ObjectDuplicateNode(LiveEditingOutputNode):
    """Object Duplicate Node"""
    bl_idname = 'ObjectDuplicateNode'
    bl_label = 'Object Duplicate'
    bl_icon = 'EDIT'

    duplicated = BoolProperty(
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Object")
        self.inputs.new('NodeSocketBool', "Duplicate")

    def run(self):
        duplicate = self.getInputValue("Duplicate")
        if not duplicate:
            self.duplicated = False
            return

        if self.duplicated:
            return

        objectName = self.getInputValue("Object")
        if not objectName:
            return

        obj = bpy.data.objects.get(objectName)
        if not obj:
            return

        new_obj = obj.copy()
        new_obj.data = obj.data.copy()
        new_obj.animation_data_clear()
        bpy.context.scene.objects.link(new_obj)

        self.duplicated = True
