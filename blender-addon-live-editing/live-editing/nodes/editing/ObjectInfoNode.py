import bpy
from bpy.props import BoolProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class ObjectInfoNode(LiveEditingBaseNode):
    """Object Info Node"""
    bl_idname = 'ObjectInfoNode'
    bl_label = 'Object Info'
    bl_icon = 'EDIT'


    # Report on world coordinates or local coordinates
    useWorldCoordinates = BoolProperty(
        default=True,
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Object")

        self.outputs.new('NodeSocketString', "Object")
        self.outputs.new('NodeSocketVectorXYZ', "Location")
        self.outputs.new('NodeSocketQuaternion', "Rotation")
        self.outputs.new('NodeSocketVectorXYZ', "Scale")

    def draw_buttons(self, context, layout):
        layout.prop(self, "useWorldCoordinates")

    def run(self):
        objectName = self.getInputValue("Object")
        if not objectName:
            self.reset()
            return

        obj = bpy.data.objects.get(objectName)
        if not obj:
            self.reset()
            return

        self.outputs["Object"].default_value = objectName
        self.outputs["Location"].default_value = obj.matrix_world.translation if self.useWorldCoordinates else obj.location.copy()
        self.outputs["Rotation"].default_value = obj.rotation_quaternion.copy() # FIXME: World vs. Local won't work for rotation here
        self.outputs["Scale"].default_value = obj.scale.copy() # FIXME: World vs. Local won't work for scale here

    def reset(self):
        self.outputs["Object"].default_value = ""
        self.outputs["Location"].default_value = (0.0, 0.0, 0.0)
        self.outputs["Rotation"].default_value = (1.0, 0.0, 0.0, 0.0)
        self.outputs["Scale"].default_value = (0.0, 0.0, 0.0)
