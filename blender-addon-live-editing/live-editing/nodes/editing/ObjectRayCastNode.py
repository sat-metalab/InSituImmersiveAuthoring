import bpy
import math
from mathutils import Vector
from ..LiveEditingBaseNode import LiveEditingBaseNode


class ObjectRayCastNode(LiveEditingBaseNode):
    """Object Ray Cast Node"""
    bl_idname = 'ObjectRayCastNode'
    bl_label = 'Object Ray Cast'
    bl_icon = 'SNAP_NORMAL'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Location")
        self.inputs.new('NodeSocketQuaternion', "Rotation")

        self.outputs.new('NodeSocketBool', "Result")
        self.outputs.new('NodeSocketString', "Object")
        self.outputs.new('NodeSocketVectorXYZ', "Location")
        self.outputs.new('NodeSocketVectorXYZ', "Normal")

    def run(self):
        rayLocation = self.getInputValue("Location")
        rayRotation = self.getInputValue("Rotation")

        rayVector = Vector((0.0, 1.0, 0.0))
        rayVector.rotate(rayRotation)

        object = None
        location = None
        normal = None
        distance = math.inf

        for obj in bpy.context.scene.objects:
            if obj.type != 'MESH' or not obj.is_visible(bpy.context.scene):
                continue

            rv = rayVector.copy()
            rv.rotate(obj.rotation_quaternion)
            result, loc, norm, index = obj.ray_cast(rayLocation - obj.location, rayVector)
            if not result:
                continue

            v = obj.location - rayLocation
            len = math.fabs(v.length)
            if len > distance:
                continue

            object = obj
            location = loc
            normal = norm
            distance = len

        result = object is not None

        self.outputs["Result"].default_value = result

        if result:
            self.outputs["Object"].default_value = object.name
            self.outputs["Location"].default_value = location
            self.outputs["Normal"].default_value = normal
        else:
            self.outputs["Object"].default_value = ""
            self.outputs["Location"].default_value = (0.00, 0.00, 0.00)
            self.outputs["Normal"].default_value = (0.00, 0.00, 0.00)
