import vrpn
import bpy
from bpy.props import StringProperty, BoolProperty
from mathutils import Vector, Quaternion
from .VRPNNode import VRPNNode


class VRPNTrackerNode(VRPNNode):
    """VRPN Tracker Node"""
    bl_idname = 'VRPNTrackerNode'
    bl_label = 'VRPN Tracker'
    bl_icon = 'GAME'

    """hostProperty = StringProperty(
        name="Host",
        description="VRPN server address (and port, if necessary)",
        default="localhost"
    )"""

    deviceNameProperty = StringProperty(
        name="Device",
        description="VRPN device name",
        default=""
    )

    connectedProperty = BoolProperty(
        name="VRPN device connected",
        description="VRPN device connected",
        default=False,
        options={'HIDDEN'}
    )

    """
    Static variable used to determine which node is currently being updated in the VRPN callback
    This is because instances are *always* changing in blender so we can't keep node references that
    will eventually be destroyed and segfault blender if we try to access it.
    """
    trackerNodes = {}
    trackers = {}

    def _keepNodeReference(self):
        """ Update node we are calling mainloop for as a static variable,
        required because we can't keep references to nodes as they seem
        to vanish and be replaced anytime blender feels like it """
        VRPNTrackerNode.trackerNodes[self.name] = self

    def treeUpdate(self, tree):
        super().treeUpdate(tree)
        self._keepNodeReference()

    def connect(self):
        super().connect()
        host = bpy.context.user_preferences.addons[__package__.split('.')[0]].preferences.vrpn.host  # self.hostProperty
        print("Connecting VRPNTracker to host", host)
        tracker = vrpn.receiver.Tracker(self.deviceNameProperty + "@" + host)
        tracker.register_change_handler(self.name, VRPNTrackerNode.onTrackerPosition, "position")
        VRPNTrackerNode.trackers[self.name] = tracker
        self._keepNodeReference()
        self.connectedProperty = True

    def disconnect(self):
        tracker = VRPNTrackerNode.trackers.get(self.name)
        if tracker is not None:
            try:
                tracker.unregister_change_handler(self.name, VRPNTrackerNode.onTrackerPosition, "position")
            except:
                pass
            del VRPNTrackerNode.trackers[self.name]
            del tracker
        else:
            print("No tracker to disconnect!")

        self.connectedProperty = False

    def preRun(self):
        super().preRun()
        tracker = VRPNTrackerNode.trackers.get(self.name)
        if tracker is not None:
            tracker.mainloop()
        elif self.connectedProperty:
            self.connect()

    def onTrackerPosition(userdata, data):
        node = VRPNTrackerNode.trackerNodes.get(userdata)
        if node is None:
            print('NoneType Node in VRPNTracker callback')
            return

        sensor = data["sensor"]

        # Position
        name = "Location " + str(sensor)
        socket = node.outputs.get(name)
        if socket is None:
            socket = node.outputs.new('NodeSocketVectorXYZ', name)
        rawPosition = data["position"]
        socket.default_value = Vector((rawPosition[0], -rawPosition[2], rawPosition[1]))

        # Quaternion
        name = "Rotation " + str(sensor)
        socket = node.outputs.get(name)
        if socket is None:
            socket = node.outputs.new('NodeSocketQuaternion', name)
        rawQuaternion = data["quaternion"]
        socket.default_value = Quaternion((rawQuaternion[3], rawQuaternion[0], -rawQuaternion[2], rawQuaternion[1]))
