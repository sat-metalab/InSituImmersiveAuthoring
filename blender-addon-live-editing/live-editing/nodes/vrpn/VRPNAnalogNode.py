import vrpn
import bpy
from bpy.props import StringProperty, BoolProperty
from .VRPNNode import VRPNNode


class VRPNAnalogNode(VRPNNode):
    """VRPN Analog Node"""
    bl_idname = 'VRPNAnalogNode'
    bl_label = 'VRPN Analog'
    bl_icon = 'GAME'

    """hostProperty = StringProperty(
        name="Host",
        description="VRPN server address (and port, if necessary)",
        default="localhost"
    )"""

    deviceNameProperty = StringProperty(
        name="Device",
        description="VRPN device name",
        default=""
    )

    connectedProperty = BoolProperty(
        name="VRPN device connected",
        description="VRPN device connected",
        default=False,
        options={'HIDDEN'}
    )

    """
    Static variable used to determine which node is currently being updated in the VRPN callback
    This is because instances are *always* changing in blender so we can't keep node references that
    will eventually be destroyed and segfault blender if we try to access it.
    """
    analogNodes = {}
    analogs = {}

    def _keepNodeReference(self):
        """ Update node we are calling mainloop for as a static variable,
        required because we can't keep references to nodes as they seem
        to vanish and be replaced anytime blender feels like it """
        VRPNAnalogNode.analogNodes[self.name] = self

    def treeUpdate(self, tree):
        super().treeUpdate(tree)
        self._keepNodeReference()

    def connect(self):
        super().connect()
        host = bpy.context.user_preferences.addons[__package__.split('.')[0]].preferences.vrpn.host  # self.hostProperty
        print("Connecting VRPNAnalog to host", host)
        analog = vrpn.receiver.Analog(self.deviceNameProperty + "@" + host)
        analog.register_change_handler(self.name, VRPNAnalogNode.onAnalogPosition)
        VRPNAnalogNode.analogs[self.name] = analog
        self._keepNodeReference()
        self.connectedProperty = True

    def disconnect(self):
        analog = VRPNAnalogNode.analogs.get(self.name)
        if analog is not None:
            try:
                analog.unregister_change_handler(self.name, VRPNAnalogNode.onAnalogPosition)
            except:
                pass
            del VRPNAnalogNode.analogs[self.name]
            del analog
        else:
            print("No analog to disconnect!")
        self.connectedProperty = False

    def preRun(self):
        super().preRun()
        analog = VRPNAnalogNode.analogs.get(self.name)
        if analog is not None:
            analog.mainloop()
        elif self.connectedProperty:
            self.connect()

    def onAnalogPosition(userdata, data):
        node = VRPNAnalogNode.analogNodes.get(userdata)
        if node is None:
            print('NoneType Node in VRPNAnalog callback')
            return

        # Channels
        channels = data["channel"]
        for channel in range(len(channels)):
            name = "Channel " + str(channel)
            socket = node.outputs.get(name)
            if socket is None:
                socket = node.outputs.new('NodeSocketFloat', name)
            socket.default_value = channels[channel]
