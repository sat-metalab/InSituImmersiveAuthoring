hasVRPN = False
try:
    import vrpn as vrpnlib
    hasVRPN = True
except ImportError:
    print("Unable to find VRPN Python module, VRPN nodes won't be available")

if hasVRPN is True:
    from .VRPNTrackerNode import VRPNTrackerNode
    from .VRPNAnalogNode import VRPNAnalogNode
    from .VRPNButtonNode import VRPNButtonNode
