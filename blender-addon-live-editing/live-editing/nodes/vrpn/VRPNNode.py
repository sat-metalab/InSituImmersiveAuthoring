from ..LiveEditingBaseNode import LiveEditingBaseNode


class VRPNNode(LiveEditingBaseNode):

    def draw_buttons(self, context, layout):
        #layout.prop(self, "hostProperty")
        layout.prop(self, "deviceNameProperty")

        connect = layout.operator("vrpn.connect_nodes")
        connect.nodename = self.name
        connect.treename = self.id_data.name

        disconnect = layout.operator("vrpn.disconnect_nodes")
        disconnect.nodename = self.name
        disconnect.treename = self.id_data.name

    def free(self):
        self.disconnect()
        super().free()

    def connect(self):
        self.disconnect()

    def disconnect(self):
        pass
