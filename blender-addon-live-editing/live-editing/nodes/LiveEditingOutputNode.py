import bpy
from .LiveEditingBaseNode import LiveEditingBaseNode


class LiveEditingOutputNode(LiveEditingBaseNode):
    """Live Editing Output node"""
    bl_idname = 'LiveEditingOutputNode'
    bl_label = 'Live Editing Output'
    bl_icon = 'LOGIC'


    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname == 'LiveEditingTree'
