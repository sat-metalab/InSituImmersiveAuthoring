from ..LiveEditingOutputNode import LiveEditingOutputNode


class TestNode(LiveEditingOutputNode):
    """Test Node"""
    bl_idname = 'TestNode'
    bl_label = 'Test'
    bl_icon = 'GAME'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Input")

    def run(self):
        input = self.inputs["Input"]
        link = input.links[0] if len(input.links) > 0 else None
        if input.is_linked and link is not None and link.is_valid:
            print(self.getInputValue("Input"))
