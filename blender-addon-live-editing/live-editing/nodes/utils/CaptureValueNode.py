from bpy.props import BoolProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class CaptureValueNode(LiveEditingBaseNode):
    """Capture Value Node"""
    bl_idname = 'CaptureValueNode'
    bl_label = 'Capture Value'
    bl_icon = 'EDIT'

    captured = BoolProperty(
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Momentary")
        self.inputs.new('NodeSocketBool', "Capture")
        self.inputs.new('NodeSocketFloat', "Value")

        self.outputs.new('NodeSocketBool', "Captured")
        self.outputs.new('NodeSocketFloat', "Value")

    def run(self):
        capture = self.getInputValue("Capture")

        if capture and not self.captured:
            self.outputs["Value"].default_value = self.getInputValue("Value")
            self.outputs["Captured"].default_value = True
            self.captured = True

        elif not capture and self.captured:
            if self.getInputValue("Momentary"):
                self.outputs["Value"].default_value = 0.00
                self.outputs["Captured"].default_value = False
            self.captured = False

        #print(capture, self.captured, self.getInputValue("Value"), self.outputs["Value"].default_value)