from bpy.props import EnumProperty
from ...utils import MatrixUtils
from ..LiveEditingBaseNode import LiveEditingBaseNode


class InputSplitterNode(LiveEditingBaseNode):
    """Input Splitter Node"""
    bl_idname = 'InputSplitterNode'
    bl_label = 'Input Splitter'
    bl_icon = 'GAME'

    def updateType(self, context):

        input = self.inputs.get("Input")
        if input:
            self.inputs.remove(input)

        output = self.outputs.get("Output")
        if output:
            self.outputs.remove(output)

        if self.inputType == "AUTO":
            self.inputs.new('NodeSocketVirtual', "Input")
            self.outputs.new('NodeSocketVirtual', "Output")

        elif self.inputType == "STRING":
            self.inputs.new('NodeSocketString', "Input")
            self.outputs.new('NodeSocketString', "Output")

        elif self.inputType == "BOOL":
            self.inputs.new('NodeSocketBool', "Input")
            self.outputs.new('NodeSocketBool', "Output")

        elif self.inputType == "FLOAT":
            self.inputs.new('NodeSocketFloat', "Input")
            self.outputs.new('NodeSocketFloat', "Output")

        elif self.inputType == "VECTOR":
            self.inputs.new('NodeSocketVectorXYZ', "Input")
            self.outputs.new('NodeSocketVectorXYZ', "Output")

        elif self.inputType == "QUATERNION":
            self.inputs.new('NodeSocketQuaternion', "Input")
            self.outputs.new('NodeSocketQuaternion', "Output")

        elif self.inputType == "MATRIX":
            self.inputs.new('NodeSocketMatrix', "Input")
            self.outputs.new('NodeSocketMatrix', "Output")

    inputType = EnumProperty(
        name="Type",
        description="Input Type",
        default="AUTO",
        items=[
            ("AUTO", "Auto", "Automatically creates an output matching the input type", 1),
            ("STRING", "String", "String", 2),
            ("BOOL", "Bool", "Boolean", 3),
            ("FLOAT", "Float", "Floating point", 4),
            ("VECTOR", "Vector", "Vector", 5),
            ("QUATERNION", "Quaternion", "Quaternion", 6),
            ("MATRIX", "Matrix", "Matrix", 7)
        ],
        update=updateType
    )

    def init(self, context):
        super().init(context)
        self.updateType(None)

    def draw_buttons_ext(self, context, layout):
        pass
        layout.prop(self, "inputType")

    def update(self):
        if self.inputType == "AUTO":
            input = self.inputs["Input"]
            output = self.outputs.get("Output")

            link = input.links[0] if len(input.links) > 0 else None
            if input.is_linked and link is not None and link.is_valid:
                if not output or link.from_socket.bl_idname != output.bl_idname:
                    if output:
                        self.outputs.remove(output)
                    self.outputs.new(link.from_socket.bl_idname, "Output")

    def run(self):
        output = self.outputs.get("Output")
        if output:
            if self.inputType == "MATRIX":
                output.default_value = MatrixUtils.flatten(self.getInputValue("Input"))
            else:
                output.default_value = self.getInputValue("Input")
