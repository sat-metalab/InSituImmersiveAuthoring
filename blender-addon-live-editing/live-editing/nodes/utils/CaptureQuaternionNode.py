from mathutils import Quaternion
from bpy.props import BoolProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class CaptureQuaternionNode(LiveEditingBaseNode):
    """Capture Quaternion Node"""
    bl_idname = 'CaptureQuaternionNode'
    bl_label = 'Capture Quaternion'
    bl_icon = 'EDIT'

    captured = BoolProperty(
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Momentary")
        self.inputs.new('NodeSocketBool', "Capture")
        self.inputs.new('NodeSocketQuaternion', "Quaternion")

        self.outputs.new('NodeSocketBool', "Captured")
        self.outputs.new('NodeSocketQuaternion', "Quaternion")

    def run(self):
        capture = self.getInputValue("Capture")

        if capture and not self.captured:
            self.outputs["Quaternion"].default_value = self.getInputValue("Quaternion").copy()
            self.outputs["Captured"].default_value = True
            self.captured = True

        elif not capture and self.captured:
            if self.getInputValue("Momentary"):
                self.outputs["Quaternion"].default_value = Quaternion((1.0, 0.0, 0.0, 0.0))
                self.outputs["Captured"].default_value = False
            self.captured = False