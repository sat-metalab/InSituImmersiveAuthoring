from ..LiveEditingBaseNode import LiveEditingBaseNode


class SwitchNode(LiveEditingBaseNode):
    """Switch Node"""
    bl_idname = 'SwitchNode'
    bl_label = 'Switch'
    bl_icon = 'GAME'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Condition")
        self.inputs.new('NodeSocketVirtual', "Input A")
        self.inputs.new('NodeSocketVirtual', "Input B")

    def update(self):
        inputA = self.inputs.get("Input A")
        linkA = inputA.links[0] if inputA and len(inputA.links) > 0 else None

        inputB = self.inputs.get("Input B")
        linkB = inputB.links[0] if inputB and len(inputB.links) > 0 else None

        output = self.outputs.get("Output")

        if (not linkA or not linkB) and output:
            self.outputs.remove(output)

        # Base the output on the type of A
        if inputA and inputA.is_linked and linkA and linkA.is_valid:
            if not output or linkA.from_socket.bl_idname != output.bl_idname:
                if output:
                    self.outputs.remove(output)
                self.outputs.new(linkA.from_socket.bl_idname, "Output")

    def run(self):
        output = self.outputs.get("Output")
        if not output:
            return

        if self.getInputValue("Condition"):
            output.default_value = self.getInputValue("Input A")
        else:
            output.default_value = self.getInputValue("Input B")
