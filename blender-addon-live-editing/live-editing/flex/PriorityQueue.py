class PriorityQueue:
    """
    The PriorityQueue class provides a general purpose priority queue.
    It is used internally by the layout_manager.
    """

    def __init__(self):
        self._priority_bins = {}
        self._min_priority = 0
        self._max_priority = -1

    def __str__(self):
        text = "{\n"
        for key, value in self._priority_bins.items():
            text += "    \"" + str(key) + "\": " + str(value) + "\n"
        text += "}\n"
        return text

    def add_object(self, obj, priority):
        #  Update our min and max priorities.
        if self._max_priority < self._min_priority:
            self._min_priority = self._max_priority = priority
        else:
            if priority < self._min_priority:
                self._min_priority = priority
            if priority > self._max_priority:
                self._max_priority = priority

        bin = self._priority_bins.get(priority)

        if bin is None:
            #  If no hash exists for the specified priority, create one.
            bin = PriorityBin()
            self._priority_bins[priority] = bin
            bin.items[obj] = True
            bin.length += 1
        else:
            #  If we don't already hold the obj in the specified hash, add it
            #  and update our item count.
            if bin.items.get(obj) is None:
                bin.items[obj] = True
                bin.length += 1

    def remove_largest(self):
        obj = None

        if self._min_priority <= self._max_priority:
            bin = self._priority_bins.get(self._max_priority)
            while bin is None or bin.length == 0:
                self._max_priority -= 1
                if self._max_priority < self._min_priority:
                    return None
                bin = self._priority_bins.get(self._max_priority)

            # Remove the item with largest priority from our priority queue.
            #  Must use a for loop here since we're removing a specific item
            #  from a 'Dictionary' (no means of directly indexing).

            for key in bin.items.keys():
                obj = key
                self.remove_child(key, self._max_priority)
                break

            # Update max_priority if applicable.
            while bin is None or bin.length == 0:
                self._max_priority -= 1
                if self._max_priority < self._min_priority:
                    break
                bin = self._priority_bins.get(self._max_priority)

        return obj

    def remove_largest_child(self, client):
        max = self._max_priority
        min = client.nest_level

        while min <= max:
            bin = self._priority_bins.get(max)
            if bin is not None and bin.length > 0:
                if max == client.nest_level:
                    #  If the current level we're searching matches that of our
                    #  client, no need to search the entire list, just check to see
                    #  if the client exists in the queue (it would be the only item
                    #  at that nest_level).
                    if bin.items.get(client) is not None:
                        self.remove_child(client, max)
                        return client
                else:
                    for key in bin.items.keys():
                        if client.contains(key):
                            self.remove_child(key, max)
                            return key
                max -= 1
            else:
                if max == self._max_priority:
                    self._max_priority -= 1
                max -= 1
                if max < min:
                    break

        return None

    def remove_smallest(self):
        obj = None

        if self._min_priority <= self._max_priority:
            bin = self._priority_bins.get(self._min_priority)
            while bin is None or bin.length == 0:
                self._min_priority += 1
                if self._min_priority > self._max_priority:
                    return None
                bin = self._priority_bins.get(self._min_priority)

            # Remove the item with smallest priority from our priority queue.
            #  Must use a for loop here since we're removing a specific item
            #  from a 'Dictionary' (no means of directly indexing).
            for key in bin.items.keys():
                obj = key
                self.remove_child(key, self._min_priority)
                break

            # Update min_priority if applicable.
            while bin is None or bin.length == 0:
                self._min_priority += 1
                if self._min_priority > self._max_priority:
                    break
                bin = self._priority_bins.get(self._min_priority)

        return obj

    def remove_smallest_child(self, client):
        min = client.nest_level

        while min <= self._max_priority:
            bin = self._priority_bins.get(min)
            if bin is None and bin.length > 0:
                if min == client.nest_level:
                    #  If the current level we're searching matches that of our
                    #  client, no need to search the entire list, just check to see
                    #  if the client exists in the queue (it would be the only item
                    #  at that nest_level).
                    if bin.items.get(client) is not None:
                        self.remove_child(client, min)
                        return client
                else:
                    for key in bin.items.keys():
                        if client.contains(key):
                            self.remove_child(key, min)
                            return key
                min += 1
            else:
                if min == self._min_priority:
                    self._min_priority += 1
                min += 1
                if min > self._max_priority:
                    break

        return None

    def remove_child(self, client, level=-1):
        priority = level if level >= 0 else client.nest_level
        bin = self._priority_bins.get(priority)
        if bin is not None and bin.items.get(client) is not None:
            del bin.items[client]
            bin.length -= 1
            return client
        return None

    def remove_all(self):
        self._priority_bins = {}
        self._min_priority = 0
        self._max_priority = -1

    def is_empty(self):
        return self._min_priority > self._max_priority


class PriorityBin:
    """
    Represents one priority bucket of entries.
    """

    def __init__(self):
        self.length = 0
        self.items = {}

    def __str__(self):
        text = "{\n"
        for key, value in self.items.items():
            text += "        \"" + str(key) + "\": " + str(value) + "\n"
        text += "    }\n"
        return text
