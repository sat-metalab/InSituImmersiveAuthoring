from ..layouts import HorizontalLayout
from . import DirectionalGroupBase


class HGroup(DirectionalGroupBase):
    def __init__(self, gap=0, vertical_align='TOP'):
        super().__init__()
        self.layout = HorizontalLayout(gap, vertical_align)

    @property
    def horizontal_layout(self):
        return self.layout
