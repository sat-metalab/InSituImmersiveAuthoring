from ...engine import Object3D
from . import Group
from .. import LayoutManager
from ...Constants import DEBUG_LAYOUT


class Root(Group):

    def __init__(self, layout=None):
        super().__init__(layout)

        self._layout_manager = LayoutManager()
        # self._layout_manager._use_phased_instantiation = True

        self._nest_level = 1
        self._offset_matrix[1][1] = -1

        self.width = 0
        self.height = 0
        self.depth = 0

    @property
    def root(self):
        return self

    @Object3D.scene.setter
    def scene(self, value):

        Object3D.scene.fset(self, value)

        # Boot up the layout root!
        if self.scene is not None and not self._initialized:
            if DEBUG_LAYOUT:
                self._logger.open("added to a scene, initializing...")

            self.initialize()

        self.invalidate_size()
        self.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    @property
    def layout_manager(self):
        return self._layout_manager

    def update(self, time, delta):
        super().update(time, delta)
        self._layout_manager.update()
