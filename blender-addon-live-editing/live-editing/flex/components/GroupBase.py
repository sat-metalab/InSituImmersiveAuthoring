import math

from ...Constants import DEBUG_LAYOUT
from ..layouts import BasicLayout
from . import Component


class GroupBase(Component):

    def __init__(self, layout=None):
        super().__init__()

        self._layout = None
        self._auto_layout = True
        self._layout_invalidate_size_flag = False
        self._layout_invalidate_display_list_flag = False

        self._content_width = 0.00
        self._content_height = 0.00
        self._content_depth = 0.00

        self._cursor_enabled_where_transparent = True

        if layout is not None:
            self.layout = layout

    #  ##          ###    ##    ##  #######  ##     ## ########
    #  ##         ## ##    ##  ##  ##     ## ##     ##    ##
    #  ##        ##   ##    ####   ##     ## ##     ##    ##
    #  ##       ##     ##    ##    ##     ## ##     ##    ##
    #  ##       #########    ##    ##     ## ##     ##    ##
    #  ##       ##     ##    ##    ##     ## ##     ##    ##
    #  ######## ##     ##    ##     #######   #######     ##

    @property
    def layout(self):
        return self._layout

    @layout.setter
    def layout(self, value):
        if DEBUG_LAYOUT:
            self._logger.open("GroupBase: switching layout", value)

        if self._layout != value:
            if self._layout is not None:
                self._layout.target = None
                # XXX: self._layout.removeEventListener( PropertyChangeEvent.PROPERTY_CHANGE, redispatchLayoutEvent )

        self._layout = value

        if self._layout is not None:
            self._layout.target = self
            # XXX: self._layout.addEventListener( PropertyChangeEvent.PROPERTY_CHANGE, redispatchLayoutEvent )

        self.invalidate_size()
        self.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    @property
    def auto_layout(self):
        return self._auto_layout

    @auto_layout.setter
    def auto_layout(self, value):
        if self._auto_layout != value:
            self._auto_layout = value
            if self._auto_layout:
                self.invalidate_size()
                self.invalidate_display_list()
                self.invalidate_parent_size_and_display_list()

    #   ######   #######  ##    ## ######## ######## ##    ## ########
    #  ##    ## ##     ## ###   ##    ##    ##       ###   ##    ##
    #  ##       ##     ## ####  ##    ##    ##       ####  ##    ##
    #  ##       ##     ## ## ## ##    ##    ######   ## ## ##    ##
    #  ##       ##     ## ##  ####    ##    ##       ##  ####    ##
    #  ##    ## ##     ## ##   ###    ##    ##       ##   ###    ##
    #   ######   #######  ##    ##    ##    ######## ##    ##    ##

    def child_xyz_changed(self):
        if self._auto_layout:
            self.invalidate_size()
            self.invalidate_display_list()

    @property
    def content_width(self):
        return self._content_width

    @content_width.setter
    def content_width(self, value):
        if self._content_width != value:
            # old_value = self._content_width
            self._content_width = value
            # dispatchPropertyChangeEvent( "content_width", old_value, _content_width )

    @property
    def content_height(self):
        return self._content_height

    @content_height.setter
    def content_height(self, value):
        if self._content_height != value:
            # old_value = self._content_height
            self._content_height = value
            # dispatchPropertyChangeEvent( "content_height", old_value, _content_height )

    @property
    def content_depth(self):
        return self._content_depth

    @content_depth.setter
    def content_depth(self, value):
        if self._content_depth != value:
            # old_value = self._content_depth
            self._content_depth = value
            # dispatchPropertyChangeEvent( "content_depth", old_value, _content_depth )

    def set_content_size(self, width=None, height=None, depth=None):
        if width is None and height is None and depth is None:
            return
        if self._content_width == width and self._content_height == height and self._content_depth == depth:
            return
        if width is not None:
            self.content_width = width
        if height is not None:
            self.content_height = height
        if depth is not None:
            self.content_depth = depth

    #  ######## ##       ######## ##     ## ######## ##    ## ########  ######
    #  ##       ##       ##       ###   ### ##       ###   ##    ##    ##    ##
    #  ##       ##       ##       #### #### ##       ####  ##    ##    ##
    #  ######   ##       ######   ## ### ## ######   ## ## ##    ##     ######
    #  ##       ##       ##       ##     ## ##       ##  ####    ##          ##
    #  ##       ##       ##       ##     ## ##       ##   ###    ##    ##    ##
    #  ######## ######## ######## ##     ## ######## ##    ##    ##     ######

    @property
    def num_elements(self):
        return -1

    def get_element_at(self, index):
        return None

    def get_element_index(self, element):
        return -1

    #  #### ##    ## ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #   ##  ###   ## ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #   ##  ####  ## ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #   ##  ## ## ## ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##  ##  ####  ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #   ##  ##   ###   ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #  #### ##    ##    ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def create_children(self):
        if DEBUG_LAYOUT:
            self._logger.open("GroupBase: create_children")

        super().create_children()
        if self.layout is None:
            if DEBUG_LAYOUT:
                self._logger.log("setting default layout")

            self.layout = BasicLayout()

        if DEBUG_LAYOUT:
            self._logger.close()

    def invalidate_size(self):
        super().invalidate_size()
        self._layout_invalidate_size_flag = True

    def invalidate_display_list(self):
        super().invalidate_display_list()
        self._layout_invalidate_display_list_flag = True

    def _invalidate_size(self):
        super().invalidate_size()

    def _invalidate_display_list(self):
        super().invalidate_display_list()

    #  ##     ## ########    ###     ######  ##     ## ########  ########
    #  ###   ### ##         ## ##   ##    ## ##     ## ##     ## ##
    #  #### #### ##        ##   ##  ##       ##     ## ##     ## ##
    #  ## ### ## ######   ##     ##  ######  ##     ## ########  ######
    #  ##     ## ##       #########       ## ##     ## ##   ##   ##
    #  ##     ## ##       ##     ## ##    ## ##     ## ##    ##  ##
    #  ##     ## ######## ##     ##  ######   #######  ##     ## ########

    def measure(self):
        if DEBUG_LAYOUT:
            self._logger.open("GroupBase: measure", "layout:", self.layout, "invalid:", self._layout_invalidate_size_flag)

        if self._layout is not None and self._layout_invalidate_size_flag:
            if DEBUG_LAYOUT:
                self._logger.log("with an invalid layout")

            old_measured_width = self.measured_width
            old_measured_height = self.measured_height
            old_measured_depth = self.measured_depth

            super().measure()

            self._layout_invalidate_size_flag = False
            self._layout.measure()

            if self.measured_width != old_measured_width or self.measured_height != old_measured_height or self.measured_depth != old_measured_depth:
                self.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    #  ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #  ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #  ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #  ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #    ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #     ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def update_display_list(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("GroupBase: update_display_list", width, height, depth)

        super().update_display_list(width, height, depth)

        if self._layout_invalidate_display_list_flag:
            if DEBUG_LAYOUT:
                self._logger.log("layout display list is invalid")

            self._layout_invalidate_display_list_flag = False
            if self.auto_layout and self._layout is not None:
                if DEBUG_LAYOUT:
                    self._logger.log("auto-layout")

                self._layout.update_display_list(width, height, depth)

            elif DEBUG_LAYOUT:
                if not self._auto_layout:
                    self._logger.log("auto-layout disabled")
                if self._layout is None:
                    self._logger.log("no layout")

        if DEBUG_LAYOUT:
            self._logger.close()

    #  ########  ####  ######  ########  ##          ###    ##    ##
    #  ##     ##  ##  ##    ## ##     ## ##         ## ##    ##  ##
    #  ##     ##  ##  ##       ##     ## ##        ##   ##    ####
    #  ##     ##  ##   ######  ########  ##       ##     ##    ##
    #  ##     ##  ##        ## ##        ##       #########    ##
    #  ##     ##  ##  ##    ## ##        ##       ##     ##    ##
    #  ########  ####  ######  ##        ######## ##     ##    ##

    @property
    def cursor_enabled_where_transparent(self):
        return self._cursor_enabled_where_transparent

    @cursor_enabled_where_transparent.setter
    def cursor_enabled_where_transparent(self, value):
        if self._cursor_enabled_where_transparent != value:
            self._cursor_enabled_where_transparent = value
            self.invalidate_display_list()

    def draw_background(self):
        if not self._cursor_enabled_where_transparent:
            return

        try:
            w = max(self.width, self.content_width)
            h = max(self.height, self.content_height)
            d = max(self.depth, self.content_depth)
        except:
            return

        if math.isnan(w) or math.isnan(h) or math.isnan(d):
            return

        # graphics.clear( )
        # graphics.beginFill( hasStyle( "backgroundColor" ) ? getStyle( "backgroundColor" ) : getStyle( "chromeBackgroundColor" ), hasStyle( "backgroundAlpha" ) ? getStyle( "backgroundAlpha" ) : 0.00 )
        # graphics.drawRect( 0, 0, w, h )
        # graphics.endFill( )
