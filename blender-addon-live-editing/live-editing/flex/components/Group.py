from . import GroupBase
from ...Constants import DEBUG_LAYOUT


class Group(GroupBase):

    def __init__(self, layout=None):
        super().__init__(layout)
        self._elements = []
        self._redraw_requested = False
        self._create_children_called = False
        self._elements_changed = False

    @GroupBase.width.setter
    def width(self, value):
        if self._width != value and self._cursor_enabled_where_transparent:
            self.redraw_requested = True
            super()._invalidate_display_list()
        GroupBase.width.fset(self, value)

    @GroupBase.height.setter
    def height(self, value):
        if self._height != value and self._cursor_enabled_where_transparent:
            self.redraw_requested = True
            super()._invalidate_display_list()
        GroupBase.height.fset(self, value)

    @GroupBase.depth.setter
    def depth(self, value):
        if self._depth != value and self._cursor_enabled_where_transparent:
            self.redraw_requested = True
            super()._invalidate_display_list()
        GroupBase.depth.fset(self, value)

    def set_actual_size(self, w, h, d):
        if self._width != w or self._height != h or self._depth != d:
            if self._cursor_enabled_where_transparent:
                self.redraw_requested = True
                super()._invalidate_display_list()
        super().set_actual_size(w, h, d)

    @GroupBase.cursor_enabled_where_transparent.setter
    def cursor_enabled_where_transparent(self, value):
        if self._cursor_enabled_where_transparent != value:
            GroupBase.cursor_enabled_where_transparent.fset(self, value)
            self.redraw_requested = True

    @property
    def redraw_requested(self):
        return self._redraw_requested

    @redraw_requested.setter
    def redraw_requested(self, value):
        if self._redraw_requested != value:
            self._redraw_requested = value

    #  ##       #### ######## ########  ######  ##    ##  ######  ##       ########
    #  ##        ##  ##       ##       ##    ##  ##  ##  ##    ## ##       ##
    #  ##        ##  ##       ##       ##         ####   ##       ##       ##
    #  ##        ##  ######   ######   ##          ##    ##       ##       ######
    #  ##        ##  ##       ##       ##          ##    ##       ##       ##
    #  ##        ##  ##       ##       ##    ##    ##    ##    ## ##       ##
    #  ######## #### ##       ########  ######     ##     ######  ######## ########

    def create_children(self):
        super().create_children()
        self._create_children_called = True
        if self._elements_changed:
            self._elements_changed = False
            self.set_elements(self._elements)

    #  ######## ##       ######## ##     ## ######## ##    ## ########  ######
    #  ##       ##       ##       ###   ### ##       ###   ##    ##    ##    ##
    #  ##       ##       ##       #### #### ##       ####  ##    ##    ##
    #  ######   ##       ######   ## ### ## ######   ## ## ##    ##     ######
    #  ##       ##       ##       ##     ## ##       ##  ####    ##          ##
    #  ##       ##       ##       ##     ## ##       ##   ###    ##    ##    ##
    #  ######## ######## ######## ##     ## ######## ##    ##    ##     ######

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, value):
        if self._create_children_called:
            self.set_elements(value)
        else:
            self._elements_changed = True
            self._elements = value

    def set_elements(self, value):
        # if there's old content and it's different than what
        # we're trying to set it to, then let's remove all the old
        # elements first.
        if self._elements is not None and self._elements != value:
            for i in range(0, len(self._elements)):
                self.element_removed(self._elements[i], i)

        self._elements = value

        if self._elements is not None:
            for i in range(0, len(self._elements)):
                elt = self._elements[i]
                if elt.parent is not None and elt.parent != self:
                    raise Exception("Element cannot have multiple parents (" + elt + ")")
                self.element_added(elt, i)

    @property
    def num_elements(self):
        if self.elements is None:
            return 0
        return len(self.elements)

    def contains_element(self, element):
        for e in self.elements:
            if e == element:
                return True

        return False

    def get_element_at(self, index):
        self.check_for_range_error(index)
        return self.elements[index]

    def check_for_range_error(self, index, adding_element=False):
        # figure out the maximum allowable index
        max_index = -1 if self._elements is None else len(self._elements) - 1

        # if adding an element, we allow an extra index at the end
        if adding_element:
            max_index += 1

        if index < 0 or index > max_index:
            raise Exception("Index out of range (" + index + ")")

    def add_element(self, element):
        if DEBUG_LAYOUT:
            self._logger.open('Group: add_element', element)

        index = self.num_elements

        # This handles the case where we call add_element on something
        # that already is in the list.  Let's just handle it silently
        # and not throw up any errors.
        if element.parent == self:
            index = self.num_elements - 1

        self.add_element_at(element, index)

        if DEBUG_LAYOUT:
            self._logger.close()

    def add_element_at(self, element, index):
        if DEBUG_LAYOUT:
            self._logger.open('Group: add_element_at', element, index)

        if element == self:
            raise Exception("Cannot add yourself as your child")

        # check for RangeError:
        self.check_for_range_error(index, True)

        host = element.parent

        # This handles the case where we call add_element on something
        # that already is in the list.  Let's just handle it silently
        # and not throw up any errors.
        if host == self:
            self.set_element_index(element, index)
            if DEBUG_LAYOUT:
                self._logger.close("already added")
        elif isinstance(host, Group):
            # Remove the item from the group if that group isn't this group
            host.remove_element(element)

        if self._elements is None:
            self._elements = []

        self._elements.insert(index, element)

        if not self._elements_changed:
            self.element_added(element, index)

        if DEBUG_LAYOUT:
            self._logger.close()

    def remove_element(self, element):
        if DEBUG_LAYOUT:
            self._logger.close("Group: remove_element", element)

        self.remove_element_at(self.get_element_index(element))

        if DEBUG_LAYOUT:
            self._logger.close()

    def remove_element_at(self, index):
        if DEBUG_LAYOUT:
            self._logger.open("Group: remove_element_at", index)

        # check RangeError
        self.check_for_range_error(index)

        element = self._elements[index]

        # Need to call element_removed before removing the item so anyone listening
        # for the event can access the item.

        if not self._elements_changed:
            self.element_removed(element, index)

        # self._elements.splice( index, 1 )
        self._elements.pop(index)

        if DEBUG_LAYOUT:
            self._logger.close()

    def remove_all_elements(self):
        if DEBUG_LAYOUT:
            self._logger.open("Group: remove_all_elements")

        count = self.num_elements
        for i in range(0, count):
            self.remove_element_at((count - 1) - i)

        if DEBUG_LAYOUT:
            self._logger.close()

    def get_element_index(self, element):
        index = -1
        for i in range(0, len(self._elements)):
            if self._elements[i] == element:
                index = i
                break
        if index == -1:
            raise Exception("Element not found in group (" + element + ")")
        else:
            return index

    def set_element_index(self, element, index):
        # check for RangeError...this is done in addItemAt
        # but we want to do it before removing the element
        self.check_for_range_error(index)

        if self.get_element_index(element) == index:
            return

        self.remove_element(element)
        self.add_element_at(element, index)

    def swap_elements(self, element1, element2):
        self.swap_elements_at(self.get_element_index(element1), self.get_element_index(element2))

    def swap_elements_at(self, index1, index2):
        self.check_for_range_error(index1)
        self.check_for_range_error(index2)

        # Make sure that index1 is the smaller index so that add_element_at
        # doesn't RTE
        if index1 > index2:
            temp = index2
            index2 = index1
            index1 = temp
        elif index1 == index2:
            return

        element1 = self._elements[index1]
        element2 = self._elements[index2]

        # Make sure we do the proper invalidations, but don't dispatch events
        if not self._elements_changed:
            self.element_removed(element1, index1)
            self.element_removed(element2, index2)

        # Step 1: remove
        # Make sure we remove the bigger index first
        self._elements.pop(index2)
        self._elements.pop(index1)

        # Step 2: swap
        # Add them in reverse order
        self._elements.insert(index1, element2)
        self._elements.insert(index2, element1)

        # Make sure we do the proper invalidations, but don't dispatch events
        if not self._elements_changed:
            self.element_added(element2, index1)
            self.element_added(element1, index2)

    #   ######   #######  ##    ## ######## ######## ##    ## ########
    #  ##    ## ##     ## ###   ##    ##    ##       ###   ##    ##
    #  ##       ##     ## ####  ##    ##    ##       ####  ##    ##
    #  ##       ##     ## ## ## ##    ##    ######   ## ## ##    ##
    #  ##       ##     ## ##  ####    ##    ##       ##  ####    ##
    #  ##    ## ##     ## ##   ###    ##    ##       ##   ###    ##
    #   ######   #######  ##    ##    ##    ######## ##    ##    ##

    def element_added(self, element, index):
        if DEBUG_LAYOUT:
            self._logger.open("Group: element_added", element, index)

        if self.layout is not None:
            self.layout.element_added(index)

        self.add_display_object_to_display_list(element, index)

        self.invalidate_size()
        self.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    def element_removed(self, element, index):
        if DEBUG_LAYOUT:
            self._logger.open("Group: element_removed", element, index)

        if element is not None and element.parent == self:
            super().remove_child(element)

        self.invalidate_size()
        self.invalidate_display_list()

        if self.layout is not None:
            self.layout.element_removed(index)

        if DEBUG_LAYOUT:
            self._logger.close()

    def add_display_object_to_display_list(self, child, index=-1):
        if DEBUG_LAYOUT:
            self._logger.open("Group: add_display_object_to_display_list", child, index)

        if child.parent == self:
            super().set_child_index(child, index if index != -1 else super().num_children - 1)
        else:
            super().add_child_at(child, index if index != -1 else super().num_children)

        if DEBUG_LAYOUT:
            self._logger.close()

    #  ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #  ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #  ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #  ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #    ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #     ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def update_display_list(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("Group: update_display_list", width, height, depth)

        super().update_display_list(width, height, depth)

        if self._redraw_requested:
            # graphics.clear( )
            self.draw_background()

        if DEBUG_LAYOUT:
            self._logger.close()
