from . import Group


class DirectionalGroupBase(Group):

    @property
    def gap(self):
        return self.layout.gap

    @gap.setter
    def gap(self, value):
        self.layout.gap = value

    @property
    def padding_top(self):
        return self.layout.padding_top

    @padding_top.setter
    def padding_top(self, value):
        self.layout.padding_top = value

    @property
    def padding_bottom(self):
        return self.layout.padding_bottom

    @padding_bottom.setter
    def padding_bottom(self, value):
        self.layout.padding_bottom = value

    @property
    def padding_left(self):
        return self.layout.padding_left

    @padding_left.setter
    def padding_left(self, value):
        self.layout.padding_left = value

    @property
    def padding_right(self):
        return self.layout.padding_right

    @padding_right.setter
    def padding_right(self, value):
        self.layout.padding_right = value

    @property
    def variable_row_height(self):
        return self.layout.variable_row_height

    @variable_row_height.setter
    def variable_row_height(self, value):
        self.layout.variable_row_height = value

    @property
    def horizontal_align(self):
        return self.layout.horizontal_align

    @horizontal_align.setter
    def horizontal_align(self, value):
        self.layout.horizontal_align = value

    @property
    def vertical_align(self):
        return self.layout.vertical_align

    @vertical_align.setter
    def vertical_align(self, value):
        self.layout.vertical_align = value
