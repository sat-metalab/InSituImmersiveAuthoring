from . import Component


class Spacer(Component):

    def __init__(self, width=None, percent_width=None, height=None, percent_height=None, depth=None, percent_depth=None):
        super().__init__()
        self.width = width
        self.percent_width = percent_width
        self.height = height
        self.percent_height = percent_height
        self.depth = depth
        self.percent_depth = percent_depth
