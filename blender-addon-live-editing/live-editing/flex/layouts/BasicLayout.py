import math
from . import LayoutBase
from ...Constants import DEBUG_LAYOUT


class BasicLayout(LayoutBase):
    def __init__(self):
        super().__init__()

    def measure(self):
        super().measure()

        width = 0.00
        height = 0.00
        depth = 0.00

        min_width = 0.00
        min_height = 0.00
        min_depth = 0.00

        count = self.target.num_elements
        for i in range(0, count):
            layout_element = self.target.get_element_at(i)

            if not layout_element.include_in_layout:
                continue

            h_center = layout_element.horizontal_center
            v_center = layout_element.vertical_center
            d_center = layout_element.depth_center

            left = layout_element.left
            right = layout_element.right
            top = layout_element.top
            bottom = layout_element.bottom
            front = layout_element.front
            back = layout_element.back

            # Extents of the element - how much additional space (besides its own width/height/depth)
            # the element needs based on its constraints.
            ext_x = 0.00
            ext_y = 0.00
            ext_z = 0.00

            if left is not None and right is not None:
                # If both left & right are set, then the extents is always
                # left + right so that the element is resized to its preferred
                # size (if it's the one that pushes out the default size of the container).
                ext_x = left + right
            elif h_center is not None:
                # If we have horizontal_center, then we want to have at least enough space
                # so that the element is within the parent container.
                # If the element is aligned to the left/right edge of the container and the
                # distance between the centers is hCenter, then the container width will be
                # parentWidth = 2 * (abs(hCenter) + elementWidth / 2)
                # <=> parentWidth = 2 * abs(hCenter) + elementWidth
                # Since the extents is the additional space that the element needs
                # extX = parentWidth - elementWidth = 2 * abs(hCenter)
                ext_x = math.fabs(h_center) * 2
            elif left is not None or right is not None:
                ext_x = 0 if left is None else left
                ext_x += 0 if right is None else right
            else:
                ext_x = layout_element.x

            if top is not None and bottom is not None:
                # If both top & bottom are set, then the extents is always
                # top + bottom so that the element is resized to its preferred
                # size (if it's the one that pushes out the default size of the container).
                ext_y = top + bottom
            elif v_center is not None:
                # If we have vertical_center, then we want to have at least enough space
                # so that the element is within the parent container.
                # If the element is aligned to the top/bottom edge of the container and the
                # distance between the centers is vCenter, then the container height will be
                # parentHeight = 2 * (abs(vCenter) + elementHeight / 2)
                # <=> parentHeight = 2 * abs(vCenter) + elementHeight
                # Since the extents is the additional space that the element needs
                # extY = parentHeight - elementHeight = 2 * abs(vCenter)
                ext_y = math.fabs(v_center) * 2
            elif top is not None or bottom is not None:
                ext_y = 0 if top is None else top
                ext_y += 0 if bottom is None else bottom
            else:
                ext_y = layout_element.y

            if front is not None and back is not None:
                # If both front & back are set, then the extents is always
                # front + back so that the element is resized to its preferred
                # size (if it's the one that pushes out the default size of the container).
                ext_z = front + back
            elif d_center is not None:
                # If we have depth_center, then we want to have at least enough space
                # so that the element is within the parent container.
                # If the element is aligned to the front/back edge of the container and the
                # distance between the centers is dCenter, then the container depth will be
                # parentDepth = 2 * (abs(dCenter) + elementDepth / 2)
                # <=> parentDepth = 2 * abs(dCenter) + elementDepth
                # Since the extents is the additional space that the element needs
                # extZ = parentDepth - elementDepth = 2 * abs(dCenter)
                ext_z = math.fabs(d_center) * 2
            elif front is not None or back is not None:
                ext_z = 0 if front is None else front
                ext_z += 0 if back is None else back
            else:
                ext_z = layout_element.z

            preferred_width = layout_element.explicit_or_measured_width
            # Added for the Blender version
            if preferred_width is None:
                preferred_width = 0

            preferred_height = layout_element.explicit_or_measured_height
            # Added for the Blender version
            if preferred_height is None:
                preferred_height = 0

            preferred_depth = layout_element.explicit_or_measured_depth
            # Added for the Blender version
            if preferred_depth is None:
                preferred_depth = 0

            width = max(width, ext_x + preferred_width)
            height = max(height, ext_y + preferred_height)
            depth = max(depth, ext_z + preferred_depth)

            # Find the minimum default extents, we take the minimum width/height only
            # when the element size is determined by the parent size
            element_min_width = layout_element.get_min_bounds_width() if self.constraints_determine_width(layout_element) else preferred_width
            element_min_height = layout_element.get_min_bounds_height() if self.constraints_determine_height(layout_element) else preferred_height
            element_min_depth = layout_element.get_min_bounds_depth() if self.constraints_determine_depth(layout_element) else preferred_depth

            min_width = max(min_width, ext_x + element_min_width)
            min_height = max(min_height, ext_y + element_min_height)
            min_depth = max(min_depth, ext_z + element_min_depth)

        # Use Math.ceil() to make sure that if the content partially occupies
        # the last pixel, we'll count it as if the whole pixel is occupied.
        # self.target.measured_width = math.ceil(max(width, min_width))
        # self.target.measured_height = math.ceil(max(height, min_height))
        # self.target.measured_depth = math.ceil(max(depth, min_depth))
        # self.target.measured_min_width = math.ceil(min_width)
        # self.target.measured_min_height = math.ceil(min_height)
        # self.target.measured_min_depth = math.ceil(min_depth)
        self.target.measured_width = max(width, min_width)
        self.target.measured_height = max(height, min_height)
        self.target.measured_depth = max(depth, min_depth)
        self.target._measured_min_width = min_width
        self.target._measured_min_height = min_height
        self.target._measured_min_depth = min_depth

    #  ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #  ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #  ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #  ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #    ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #     ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def update_display_list(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("BasicLayout: update_display_list", width, height, depth)

        super().update_display_list(width, height, depth)

        max_x = 0.00
        max_y = 0.00
        max_z = 0.00

        count = self.target.num_elements
        for i in range(0, count):
            layout_element = self.target.get_element_at(i)
            if DEBUG_LAYOUT:
                self._logger.open(layout_element)

            if not layout_element.include_in_layout:
                if DEBUG_LAYOUT:
                    self._logger.close("not included in layout")
                continue

            h_center = layout_element.horizontal_center
            v_center = layout_element.vertical_center
            d_center = layout_element.depth_center

            left = layout_element.left
            right = layout_element.right
            top = layout_element.top
            bottom = layout_element.bottom
            front = layout_element.front
            back = layout_element.back

            percent_width = layout_element.percent_width
            percent_height = layout_element.percent_height
            percent_depth = layout_element.percent_depth

            element_max_width = None
            element_max_height = None
            element_max_depth = None
            child_width = None
            child_height = None
            child_depth = None

            if percent_width is not None:
                available_width = width
                if left is not None:
                    available_width -= left
                if right is not None:
                    available_width -= right
                # child_width = round(available_width * min(percent_width, 1))
                child_width = available_width * min(percent_width, 1)
                element_max_width = min(layout_element.get_max_bounds_width(), self.max_size_to_fit_in(width, h_center, left, right, layout_element.x))
            elif left is not None and right is not None:
                child_width = width - right - left

            if percent_height is not None:
                available_height = height
                if top is not None:
                    available_height -= top
                if bottom is not None:
                    available_height -= bottom
                # child_height = round(available_height * min(percent_height, 1))
                child_height = available_height * min(percent_height, 1)
                element_max_height = min(layout_element.get_max_bounds_height(), self.max_size_to_fit_in(height, v_center, top, bottom, layout_element.y))
            elif top is not None and bottom is not None:
                child_height = height - bottom - top

            if percent_depth is not None:
                available_depth = depth
                if front is not None:
                    available_depth -= front
                if back is not None:
                    available_depth -= back
                # child_depth = round(available_depth * min(percent_depth, 1))
                child_depth = available_depth * min(percent_depth, 1)
                element_max_depth = min(layout_element.get_max_bounds_depth(), self.max_size_to_fit_in(depth, d_center, front, back, layout_element.z))
            elif front is not None and back is not None:
                child_depth = depth - back - front

            # Apply min and max constraints, make sure min is applied last. In the cases
            # where childWidth and childHeight are NaN, set_layout_bounds_size will use preferred_size
            # which is already constrained between min and max.
            if child_width is not None:
                if element_max_width is None:
                    element_max_width = layout_element.get_max_bounds_width()
                child_width = max(layout_element.get_min_bounds_width(), min(element_max_width, child_width))
            if child_height is not None:
                if element_max_height is None:
                    element_max_height = layout_element.get_max_bounds_height()
                child_height = max(layout_element.get_min_bounds_height(), min(element_max_height, child_height))
            if child_depth is not None:
                if element_max_depth is None:
                    element_max_depth = layout_element.get_max_bounds_depth()
                child_depth = max(layout_element.get_min_bounds_depth(), min(element_max_depth, child_depth))

            # Set the size.
            layout_element.set_layout_bounds_size(child_width, child_height, child_depth)

            element_width = layout_element.width
            # Added for Blender
            if element_width is None:
                element_width = 0

            element_height = layout_element.height
            # Added for Blender
            if element_height is None:
                element_height = 0

            element_depth = layout_element.depth
            # Added for Blender
            if element_depth is None:
                element_depth = 0

            child_x = None
            child_y = None
            child_z = None

            # Horizontal position
            if h_center is not None:
                # child_x = round((width - element_width) / 2 + h_center)
                child_x = (width - element_width) / 2 + h_center
            elif left is not None:
                child_x = left
            elif right is not None:
                child_x = width - element_width - right
            else:
                child_x = layout_element.x

            # Vertical position
            if v_center is not None:
                # child_y = round((height - element_height) / 2 + v_center)
                child_y = (height - element_height) / 2 + v_center
            elif top is not None:
                child_y = top
            elif bottom is not None:
                child_y = height - element_height - bottom
            else:
                child_y = layout_element.y

            # Depth position
            if d_center is not None:
                # child_z = round((depth - element_depth) / 2 + d_center)
                child_z = (depth - element_depth) / 2 + d_center
            elif front is not None:
                child_z = front
            elif back is not None:
                child_z = depth - element_depth - back
            else:
                child_z = layout_element.z

            # Set position
            layout_element.move(child_x, child_y, child_z)

            # update content limits
            max_x = max(max_x, child_x + element_width)
            max_y = max(max_y, child_y + element_height)
            max_z = max(max_z, child_z + element_depth)

            if DEBUG_LAYOUT:
                self._logger.close()

        # self.target.set_content_size(math.ceil(max_x), math.ceil(max_y), math.ceil(max_z))
        self.target.set_content_size(max_x, max_y, max_z)

        if DEBUG_LAYOUT:
            self._logger.close()

    #  ##     ## ######## ##       ########  ######## ########   ######
    #  ##     ## ##       ##       ##     ## ##       ##     ## ##    ##
    #  ##     ## ##       ##       ##     ## ##       ##     ## ##
    #  ######### ######   ##       ########  ######   ########   ######
    #  ##     ## ##       ##       ##        ##       ##   ##         ##
    #  ##     ## ##       ##       ##        ##       ##    ##  ##    ##
    #  ##     ## ######## ######## ##        ######## ##     ##  ######

    def constraints_determine_width(self, layout_element):
        return layout_element.percent_width is not None or (
            layout_element.left is not None and layout_element.right is not None)

    def constraints_determine_height(self, layout_element):
        return layout_element.percent_height is not None or (
            layout_element.top is not None and layout_element.bottom is not None)

    def constraints_determine_depth(self, layout_element):
        return layout_element.percent_depth is not None or (
            layout_element.front is not None and layout_element.back is not None)

    def max_size_to_fit_in(self, total_size, center, low_constraint, high_constraint, position):
        if center is not None:
            # (1) x == (totalSize - childWidth) / 2 + hCenter
            # (2) x + childWidth <= totalSize
            # (3) x >= 0
            #
            # Substitute x in (2):
            # (totalSize - childWidth) / 2 + hCenter + childWidth <= totalSize
            # totalSize - childWidth + 2 * hCenter + 2 * childWidth <= 2 * totalSize
            # 2 * hCenter + childWidth <= totalSize se we get:
            # (3) childWidth <= totalSize - 2 * hCenter
            #
            # Substitute x in (3):
            # (4) childWidth <= totalSize + 2 * hCenter
            #
            # From (3) & (4) above we get:
            # childWidth <= totalSize - 2 * abs(hCenter)

            return total_size - 2 * abs(center)
        elif low_constraint is not None:
            # childWidth + left <= totalSize
            return total_size - low_constraint
        elif high_constraint is not None:
            # childWidth + right <= totalSize
            return total_size - high_constraint
        else:
            # childWidth + childX <= totalSize
            return total_size - position
