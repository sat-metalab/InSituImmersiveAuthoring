import math
from .. import ChildInfo
from ...Constants import DEBUG_LAYOUT
from . import LayoutBase


class LayoutElementChildInfo(ChildInfo):
    def __init__(self):
        super().__init__()
        self.layout_element = None


class DirectionalLayoutBase(LayoutBase):

    def __init__(self, gap=0, horizontal_align='LEFT', vertical_align='TOP'):
        super().__init__()

        self._gap = gap
        self._horizontal_align = horizontal_align
        self._vertical_align = vertical_align

        self._padding_top = 0.00
        self._padding_bottom = 0.00
        self._padding_left = 0.00
        self._padding_right = 0.00

    @property
    def gap(self):
        return self._gap

    @gap.setter
    def gap(self, value):
        if self._gap != value:
            self._gap = value
            self.invalidate_target_size_and_display_list()

    #     ###    ##       ####  ######   ##    ## ##     ## ######## ##    ## ########
    #    ## ##   ##        ##  ##    ##  ###   ## ###   ### ##       ###   ##    ##
    #   ##   ##  ##        ##  ##        ####  ## #### #### ##       ####  ##    ##
    #  ##     ## ##        ##  ##   #### ## ## ## ## ### ## ######   ## ## ##    ##
    #  ######### ##        ##  ##    ##  ##  #### ##     ## ##       ##  ####    ##
    #  ##     ## ##        ##  ##    ##  ##   ### ##     ## ##       ##   ###    ##
    #  ##     ## ######## ####  ######   ##    ## ##     ## ######## ##    ##    ##

    @property
    def horizontal_align(self):
        return self._horizontal_align

    @horizontal_align.setter
    def horizontal_align(self, value):
        if self._horizontal_align != value:
            self._horizontal_align = value
            if self.target is not None:
                self.target.invalidate_display_list()

    @property
    def vertical_align(self):
        return self._vertical_align

    @vertical_align.setter
    def vertical_align(self, value):
        if self._vertical_align != value:
            self._vertical_align = value
            if self.target is not None:
                self.target.invalidate_display_list()

    #  ########     ###    ########  ########  #### ##    ##  ######
    #  ##     ##   ## ##   ##     ## ##     ##  ##  ###   ## ##    ##
    #  ##     ##  ##   ##  ##     ## ##     ##  ##  ####  ## ##
    #  ########  ##     ## ##     ## ##     ##  ##  ## ## ## ##   ####
    #  ##        ######### ##     ## ##     ##  ##  ##  #### ##    ##
    #  ##        ##     ## ##     ## ##     ##  ##  ##   ### ##    ##
    #  ##        ##     ## ########  ########  #### ##    ##  ######

    @property
    def padding_top(self):
        return self._padding_top

    @padding_top.setter
    def padding_top(self, value):
        if self._padding_top != value:
            self._padding_top = value
            self.invalidate_target_size_and_display_list()

    @property
    def padding_bottom(self):
        return self._padding_bottom

    @padding_bottom.setter
    def padding_bottom(self, value):
        if self._padding_bottom != value:
            self._padding_bottom = value
            self.invalidate_target_size_and_display_list()

    @property
    def padding_left(self):
        return self._padding_left

    @padding_left.setter
    def padding_left(self, value):
        if self._padding_left != value:
            self._padding_left = value
            self.invalidate_target_size_and_display_list()

    @property
    def padding_right(self):
        return self._padding_right

    @padding_right.setter
    def padding_right(self, value):
        if self._padding_right != value:
            self._padding_right = value
            self.invalidate_target_size_and_display_list()

    #  #### ##    ## ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #   ##  ###   ## ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #   ##  ####  ## ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #   ##  ## ## ## ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##  ##  ####  ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #   ##  ##   ###   ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #  #### ##    ##    ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def invalidate_target_size_and_display_list(self):
        if DEBUG_LAYOUT:
            self._logger.open("DirectionalLayoutBase: invalidate_target_size_and_display_list")

        if self.target is None:
            if DEBUG_LAYOUT:
                self._logger.close("no target")
            return

        self.target.invalidate_size()
        self.target.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    #  ##     ## ########    ###     ######  ##     ## ########  ########
    #  ###   ### ##         ## ##   ##    ## ##     ## ##     ## ##
    #  #### #### ##        ##   ##  ##       ##     ## ##     ## ##
    #  ## ### ## ######   ##     ##  ######  ##     ## ########  ######
    #  ##     ## ##       #########       ## ##     ## ##   ##   ##
    #  ##     ## ##       ##     ## ##    ## ##     ## ##    ##  ##
    #  ##     ## ######## ##     ##  ######   #######  ##     ## ########

    def measure_real(self, layout_target):
        raise Exception('measure_real() is not implemented')

    def measure(self):
        if DEBUG_LAYOUT:
            self._logger.open("DirectionalLayoutBase: measure")

        if self.target is None:
            if DEBUG_LAYOUT:
                self._logger.close("no target")
            return

        self.measure_real(self.target)

        # Use Math.ceil() to make sure that if the content partially occupies
        # the last pixel, we'll count it as if the whole pixel is occupied.
        # self.target.measured_width = math.ceil(self.target.measured_width)
        # self.target.measured_height = math.ceil(self.target.measured_height)
        # self.target.measured_min_width = math.ceil(self.target.measured_min_width)
        # self.target.measured_min_height = math.ceil(self.target.measured_min_height)

        if DEBUG_LAYOUT:
            self._logger.close()

    #  ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #  ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #  ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #  ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #    ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #     ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def update_display_list_real(self):
        raise Exception('update_display_list_real() is not implemented')

    def update_display_list(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("DirectionalLayoutBase: update_display_list", width, height, depth)

        super().update_display_list(width, height, 0)

        layout_target = self.target
        if layout_target is None:
            if DEBUG_LAYOUT:
                self._logger.close("no target")
            return

        if layout_target.num_elements == 0 or width == 0 or height == 0:  # or depth == 0: XXX: We don't really care about depth
            if DEBUG_LAYOUT:
                if layout_target.num_elements == 0:
                    self._logger.close("no elements")
                elif width == 0 or height == 0 or depth == 0:
                    self._logger.close("a dimension is zero", width, height, depth)

            # setColumnCount(0)
            # setIndexInView(-1, -1)
            if layout_target.num_elements == 0:
                # layout_target.set_content_size(
                #     math.ceil(self.padding_left + self.padding_right),
                #     math.ceil(self.padding_top + self.padding_bottom)
                # )
                layout_target.set_content_size(
                    self.padding_left + self.padding_right,
                    self.padding_top + self.padding_bottom
                )
            return

        self.update_display_list_real()

        if DEBUG_LAYOUT:
            self._logger.close()
