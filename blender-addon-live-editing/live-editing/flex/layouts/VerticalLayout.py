import math
from ...utils import MathUtils
from .. import Flex, SizesAndLimit
from ...Constants import DEBUG_LAYOUT
from . import DirectionalLayoutBase, LayoutElementChildInfo


class VerticalLayout(DirectionalLayoutBase):

    @staticmethod
    def calculate_percent_width(layout_element, width):
        return MathUtils.clamp(
            min(round(layout_element.percent_width * width), width),
            layout_element.get_min_bounds_width(),
            layout_element.get_max_bounds_width()
        )

    @staticmethod
    def size_layout_element(layout_element, width, horizontal_align, restricted_width, height, variable_row_height, row_height):
        new_width = None

        if horizontal_align == 'JUSTIFY':
            new_width = restricted_width
        else:
            if layout_element.percent_width is not None:
                new_width = VerticalLayout.calculate_percent_width(layout_element, width)

        if variable_row_height:
            layout_element.set_layout_bounds_size(new_width, height, None)
        else:
            layout_element.set_layout_bounds_size(new_width, row_height, None)

    def __init__(self, gap=0, horizontal_align='LEFT'):
        super().__init__(gap, horizontal_align, 'TOP')
        self._row_height = None
        self._variable_row_height = True

    #  ########   #######  ##      ##  ######
    #  ##     ## ##     ## ##  ##  ## ##    ##
    #  ##     ## ##     ## ##  ##  ## ##
    #  ########  ##     ## ##  ##  ##  ######
    #  ##   ##   ##     ## ##  ##  ##       ##
    #  ##    ##  ##     ## ##  ##  ## ##    ##
    #  ##     ##  #######   ###  ###   ######

    @property
    def row_height(self):
        return self._row_height if self._row_height is not None else 0

    @row_height.setter
    def row_height(self, value):
        if self._row_height != value:
            self._row_height = value
        self.invalidate_target_size_and_display_list()

    @property
    def variable_row_height(self):
        return self._variable_row_height

    @variable_row_height.setter
    def variable_row_height(self, value):
        if self._variable_row_height != value:
            self._variable_row_height = value
            self.invalidate_target_size_and_display_list()

    #  ##     ## ########    ###     ######  ##     ## ########  ########
    #  ###   ### ##         ## ##   ##    ## ##     ## ##     ## ##
    #  #### #### ##        ##   ##  ##       ##     ## ##     ## ##
    #  ## ### ## ######   ##     ##  ######  ##     ## ########  ######
    #  ##     ## ##       #########       ## ##     ## ##   ##   ##
    #  ##     ## ##       ##     ## ##    ## ##     ## ##    ##  ##
    #  ##     ## ######## ##     ##  ######   #######  ##     ## ########

    def measure_real(self, layout_target):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: measure_real", layout_target)

        size = SizesAndLimit()
        justify = self._horizontal_align == 'JUSTIFY'
        num_elements = layout_target.num_elements
        num_elements_in_layout = num_elements

        preferred_width = 0.00
        preferred_height = 0.00
        min_width = 0.00
        min_height = 0.00

        fixed_row_height = None if self._variable_row_height else self._row_height

        for i in range(0, num_elements):
            element = layout_target.get_element_at(i)

            if DEBUG_LAYOUT:
                self._logger.open("element", element)

            if element is None or not element.include_in_layout:
                if DEBUG_LAYOUT:
                    if element is None:
                        self._logger.close("no element")
                    elif not element.include_in_layout:
                        self._logger.close("element not included in layout")

                num_elements_in_layout -= 1
                continue

            self.get_element_width(element, justify, size)
            preferred_width = max(preferred_width, size.preferred_size)
            min_width = max(min_width, size.min_size)

            self.get_element_height(element, fixed_row_height, size)
            preferred_height += size.preferred_size
            min_height += size.min_size

            if DEBUG_LAYOUT:
                self._logger.close()

        # Add gaps
        if num_elements_in_layout > 1:
            gap = self._gap * (num_elements_in_layout - 1)
            preferred_height += gap
            min_height += gap

        h_padding = self._padding_left + self._padding_right
        v_padding = self._padding_top + self._padding_bottom

        layout_target.measured_width = preferred_width + h_padding
        layout_target.measured_height = preferred_height + v_padding
        layout_target._measured_min_width = min_width + h_padding
        layout_target._measured_min_height = min_height + v_padding

        if DEBUG_LAYOUT:
            self._logger.close(
                str(layout_target.measured_width) + "/" + str(layout_target._measured_min_width),
                str(layout_target.measured_height) + "/" + str(layout_target._measured_min_height)
            )

    #  ######## ##       ######## ##     ## ######## ##    ## ########  ######
    #  ##       ##       ##       ###   ### ##       ###   ##    ##    ##    ##
    #  ##       ##       ##       #### #### ##       ####  ##    ##    ##
    #  ######   ##       ######   ## ### ## ######   ## ## ##    ##     ######
    #  ##       ##       ##       ##     ## ##       ##  ####    ##          ##
    #  ##       ##       ##       ##     ## ##       ##   ###    ##    ##    ##
    #  ######## ######## ######## ##     ## ######## ##    ##    ##     ######

    def get_element_width(self, element, justify, result):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: get_element_width", element, justify, result)

        # element_preferred_width = math.ceil(element.explicit_or_measured_width)
        element_preferred_width = element.explicit_or_measured_width
        flexible_width = element.percent_width is not None or justify
        # element_min_width = math.ceil(element.get_min_bounds_width()) if flexible_width else element_preferred_width
        element_min_width = element.get_min_bounds_width() if flexible_width else element_preferred_width
        result.preferred_size = element_preferred_width
        result.min_size = element_min_width

        if DEBUG_LAYOUT:
            self._logger.close(result)

    def get_element_height(self, element, fixed_row_height, result):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: get_element_height", element, fixed_row_height, result)

        # element_preferred_height = math.ceil(element.explicit_or_measured_height) if fixed_row_height is None else fixed_row_height
        element_preferred_height = element.explicit_or_measured_height if fixed_row_height is None else fixed_row_height
        flexible_height = element.percent_height is not None
        # element_min_height = math.ceil(element.get_min_bounds_height()) if flexible_height else element_preferred_height
        element_min_height = element.get_min_bounds_height() if flexible_height else element_preferred_height
        result.preferred_size = element_preferred_height
        result.min_size = element_min_height

        if DEBUG_LAYOUT:
            self._logger.close(result)

    #  ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #  ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #  ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #  ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #    ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #     ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def update_display_list_real(self):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: update_display_list_real")

        layout_target = self.target
        target_width = max(0, layout_target.width - self._padding_left - self._padding_right)
        target_height = max(0, layout_target.height - self._padding_top - self._padding_bottom)

        if DEBUG_LAYOUT:
            self._logger.log(
                "layout_target:", layout_target,
                "target_width", target_width,
                "target_height:", target_height
            )

        #layout_element = None
        count = layout_target.num_elements

        container_width = target_width
        # if self._clipAndEnableScrolling and ( self._horizontal_align == 'CENTER' or self._horizontal_align == 'RIGHT' ):
        #     for i in range(0, count):
        #         layout_element = layout_target.get_element_at( i )
        #         if layout_element is None or not layout_element.include_in_layout:
        #             continue
        #
        #         layout_elementWidth = None
        #         if layout_element.percent_width is not None:
        #             layout_elementWidth = self.calculate_percent_width( layout_element, targetWidth )
        #         else:
        #             layout_elementWidth = layout_element.explicit_or_measured_width
        #
        #         containerWidth = max( containerWidth, math.ceil( layout_elementWidth ) )

        excess_height = self.distribute_height(target_width, target_height, container_width)

        if DEBUG_LAYOUT:
            self._logger.log(
                "container_width:", container_width,
                "excess_height:", excess_height
            )

        h_align = 0
        if self.horizontal_align == 'CENTER':
            h_align = .5
        elif self._horizontal_align == 'RIGHT':
            h_align = 1

        # As the layout_elements are positioned, we'll count how many columns
        # fall within the layout_target's scrollRect
        # visible_rows = 0
        # min_visible_y = layout_target.verticalScrollPosition
        # max_visible_y = min_visible_y + target_height

        # Finally, position the layout_elements and find the first/last
        # visible indices, the content size, and the number of
        # visible elements.
        x0 = self._padding_left
        y = self._padding_top
        max_x = self._padding_left
        max_y = self._padding_top
        # first_row_in_view = -1
        # last_row_in_view = -1

        # Take horizontal_align into account
        if excess_height > 0:  # or not self._clipAndEnableScrolling:
            v_align = self._vertical_align
            if v_align == 'MIDDLE':
                # y = self.padding_top + round(excess_height / 2)
                y = self.padding_top + (excess_height / 2)
            elif v_align == 'BOTTOM':
                y = self.padding_top + excess_height

        for index in range(0, count):
            layout_element = layout_target.get_element_at(index)
            if layout_element is None or not layout_element.include_in_layout:
                continue

            # Set the layout element's position
            # dx = math.ceil(layout_element.width)  # Was getPreferredBoundsWidth
            dx = layout_element.width  # Was getPreferredBoundsWidth
            # dy = math.ceil(layout_element.height)  # Was getPreferredBoundsHeight
            dy = layout_element.height  # Was getPreferredBoundsHeight

            x = x0 + (container_width - dx) * h_align
            # In case we have horizontal_align.CENTER we have to round
            # if h_align == 0.5:
            #     x = round(x)
            layout_element.move(x, y)  # Was setLayoutBoundsPosition

            # Update maxX,Y, first,lastVisibleIndex, and y
            max_x = max(max_x, x + dx)
            max_y = max(max_y, y + dy)
            # if not self._clipAndEnableScrolling or ( ( y < max_visible_y ) and ( ( y + dy ) > min_visible_y ) ) or ( ( dy <= 0 ) and ( ( y == max_visible_y ) or (y == min_visible_y ) ) ) :
            #     visible_rows += 1
            #     if first_row_in_view == -1:
            #         first_row_in_view = last_row_in_view = index
            #     else:
            #         last_row_in_view = index
            y += dy + self.gap

        # setRowCount(visible_rows)
        # setIndexInView(first_row_in_view, last_row_in_view)

        # Make sure that if the content spans partially over a pixel to the right/bottom,
        # the content size includes the whole pixel.
        # layout_target.set_content_size(math.ceil(max_x + self.padding_right), math.ceil(max_y + self.padding_bottom))
        layout_target.set_content_size(max_x + self.padding_right, max_y + self.padding_bottom)

        if DEBUG_LAYOUT:
            self._logger.close()

    def distribute_height(self, width, height, restricted_width):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: distribute_height", width, height, restricted_width)

        space_to_distribute = height
        total_percent_height = 0
        child_info_array = []

        # row_height can be expensive to compute
        # rh = 0 if self._variable_row_height else math.ceil(self.row_height)
        rh = 0 if self._variable_row_height else self.row_height
        count = self.target.num_elements
        total_count = count  # number of elements to use in gap calculation

        # If the child is flexible, store information about it in the
        # childInfoArray. For non-flexible children, just set the child's
        # width and height immediately.
        for index in range(0, count):
            layout_element = self.target.get_element_at(index)
            if layout_element is None or not layout_element.include_in_layout:
                total_count -= 1
                continue

            if layout_element.percent_height is not None and self._variable_row_height:
                total_percent_height += layout_element.percent_height
                child_info = LayoutElementChildInfo()
                child_info.layout_element = layout_element
                child_info.percent = layout_element.percent_height
                child_info.min = layout_element.get_min_bounds_height()
                child_info.max = layout_element.get_max_bounds_height()
                child_info_array.append(child_info)
            else:
                self.size_layout_element(layout_element, width, self._horizontal_align, restricted_width, None, self.variable_row_height, rh)
                # space_to_distribute -= math.ceil(layout_element.height)  # Was getLayoutBoundsHeight
                space_to_distribute -= layout_element.height  # Was getLayoutBoundsHeight

        if total_count > 1:
            space_to_distribute -= (total_count - 1) * self.gap

        # Distribute the extra space among the flexible children
        if total_percent_height > 0:
            Flex.flex_children_proportionally(height, space_to_distribute, total_percent_height, child_info_array)
            round_off = 0
            for child_info in child_info_array:
                # Make sure the calculated percentages are rounded to pixel boundaries
                # XXX: Probably not anymore since we don't round
                # child_size = round(child_info.size + round_off)
                child_size = child_info.size + round_off
                round_off += child_info.size - child_size
                self.size_layout_element(child_info.layout_element, width, self._horizontal_align, restricted_width, child_size, self._variable_row_height, rh)
                space_to_distribute -= child_size

        if DEBUG_LAYOUT:
            self._logger.close()

        return space_to_distribute
