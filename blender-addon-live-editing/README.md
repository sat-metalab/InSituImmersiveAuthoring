# Requirements

sudo apt install python3 python3-pip python3-setuptools
sudo pip3 install transitions "pyopengl==3.1.1a1" "pyopengl-accelerate==3.1.1a1" hachoir3

## VRPN
It is expected that VRPN was built with Python 3 support and installed on the system where this will run.

## Transitions
A python state machine. https://github.com/tyarkoni/transitions

    pip3 install transitions

# More Information
https://sat-metalab.atlassian.net/wiki/pages/viewpage.action?spaceKey=RES&title=Blender+Live+Editing

# Cubemap "ideal" resolution for splash
3840 x 2560

# 16-11-25 - Notes
* [OK] Objects inside dome are not aligned with pointer
* [OK] Can only ray cast mesh objects
* [OK] Some mesh objects don't have meshes ready when loading document

* [OK] Two possible solutions for pointing:
    - [OK] Calculate where we are pointing on the dome surface
    - ~~Move cube cam frustrum to match person's position~~
    
* [OK] Separate selection/grabbing and editing so that we don't necessarily need to hold, also easier for duplication
* [OK] How to setup a scene to work in the dome

# 17-01-06
* [OK] Have VRPN Host as ~~an input~~ a global config
* [OK] Something does not consider dome rotation when grabbing
    - [OK] Controller rotation does not rotate its position in space
* Left controller Y rotation + thumb rotation combine in a very bad way
    - Wrist rotation should probably be added AFTER thumb so that we continue the rotation in the right axis
* [OK] Grabbing object resets scale to 1.00
    - [OK] This is due to the matrix transformations in *ObjectManipulatorNode*, I need to talk with Manu for this

# 17-02-27
* [OK] Creating multiple widows requires to create multiple vertex arrays
* [OK] Button contrast is not great when white on blue
* [OK] Text should be a bit bigger
* [OK] Tracking space rotation breaks cursor, it flip up nd down
* [OK] Selection chevron is not relative to moved camera
* [OK] Camera is not moveable in blender

# 17-03-27

* [OK] Center animation panel vertically on 0
* [OK] Transform helpers don't follow object
* [TODO: TEST] Isolate brush sizes for brush and paint
* [??? Manu? Is it when we are touching the pad on init?] Navigate resets position on enter
* [OK] Center pointer before creating mesh otherwise it creates the dome at the wrong place
* [OK] Clear keyframes seems to crash
* [OK] Is the chevron pointing the selection gone?

# 17-??-??
